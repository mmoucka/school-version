<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\commands;
use Yii;

use yii\console\Controller;
use app\models\dialplan\routeEngine;
use app\models\asterisk\DialoutForm;
use app\models\asterisk\DialoutEntry;
use app\models\asterisk\Auth;
use app\models\asterisk\QueueForm;
use phpari\phpari;
use phpari\AGI_AsteriskManager;

/**
 * Controller managing inbound and outbound calls.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class DialoutcmdController extends Controller {
    /**
     *
     * @var phpagi for handling calls.
     */
    private $agi;
    /**
     *
     * @var routeEngine for find route to destination.
     */
    private $engine;
    
    /**
     * Starts searching for records in table `dialouts`, when record is found and a queue has free members it makes call to the stored number.
     * If the call is answered puts it into the queue.
     */
    public function actionStart() {
        $ami = new AGI_AsteriskManager();
        $ari = new phpari();
        $entries = DialoutForm::listAll();
        Yii::$app->cache->set('dialout', 'running');
        if ($ami->connect()) {
            $called = false;
            foreach ($entries as $entry) {
                if (!strcmp('stopping', Yii::$app->cache->get('dialout'))) {
                    Yii::$app->cache->set('dialout', 'stopped');
                    exit();
                }
                $members = QueueForm::getMembers($entry['destqueue']);
                $auth = Auth::findOne(['id' => $entry['trunk']]);
                $free = false;
                foreach ($members as $member) {
                    $endpoint = $ari->endpoints()->show($member['interface']);
                    if (!strcmp('online', $endpoint['state']) && empty($endpoint['channel_ids'])) {
                        $free = true;
                    }
                }
                if ($free) {
                    $dialoutentry = DialoutEntry::findOne(['id' => $entry['id']]);
                    if (strcmp('Success', $dialoutentry->status)) {
                        $dialoutentry->attempts += 1;
                        $result = $ami->Originate('PJSIP/' . $entry['number'] . ($entry['trunk'] ? '@' . $entry['trunk'] : '')
                                , null, null, null, 'Queue', $entry['destqueue'] . ',n', 15000, $entry['callerid'] . ' <' . ($entry['trunk'] && isset($auth->username) ? $auth->username : 'Local') . '>'
                                , 'CDR(userfield)=Dialout:' . $entry['number']);
                        $dialoutentry->status = $result['Response'];
                        if (strcmp('Success', $result['Response'])) {
                            $called = true;
                        }
                        $dialoutentry->update();
                    }
                }
            }
            if (!$called) {
                sleep(60);
            }
            $ami->disconnect();
            if (!strcmp('running', Yii::$app->cache->get('dialout'))) {
                $this->actionStart();
            }            
            Yii::$app->cache->set('dialout', 'stopped');
            exit();
        }
        Yii::$app->cache->set('dialout', 'stopped');
    }
    public function actionStop() {
        echo "Stopping\n";
        Yii::$app->cache->set('dialout', 'stopping');
    }
}

