<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1;dbname=webinterface',
    'username' => 'username',
    'password' => 'password',
    'charset' => 'utf8',
];
