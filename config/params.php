<?php

return [
    'adminEmail' => 'admin@example.com',
    'phpariConfig' => __DIR__.'/phpari.ini',
];
