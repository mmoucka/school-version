<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'webinterface',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '2KzmkGKOLCz00dPBgP/R8r0MvCVQ6lyc',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'authTimeout' => '3600',            
            'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],        
        'db' => require(__DIR__ . '/db.php'),
        'dbAsterisk' => require(__DIR__ . '/dbAsterisk.php'),        
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],        
    ],
     'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
             'downloadAction' => 'gridview/export/download',
             'i18n' => [
                'class' => 'yii\i18n\PhpMessageSource',
                 'basePath' => '@kvgrid/messages',
                 'forceTranslation' => true
                ]
            ]
    ],
    'params' => $params,
    ];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
