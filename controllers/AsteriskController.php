<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use app\models\asterisk\AsteriskSideMenu;
use app\models\asterisk\ExtensionForm;
use app\models\asterisk\TrunkForm;
use phpari\AGI_AsteriskManager;
use phpari\phpari;

/**
 * Reloads Asterisk, renders Dashboard and controll spy on channel.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class AsteriskController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['spy'],
                        'allow' => true,
                        'roles' => ['asterisk_spyChannel'],
                    ],
                    [
                        'actions' => ['hangup'],
                        'allow' => true,
                        'roles' => ['asterisk_hangup'],
                    ],
                    [
                        'actions' => ['reload'],
                        'allow' => true,
                        'roles' => ['asterisk_reload'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $permissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->getId());
                            return preg_match('"asterisk"', implode(',', array_keys($permissions)));
                        }
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Populates Information about running astererisk server.
     * @return ArrayDataProvider
     */
    protected function  populateInformation()
    {
        $ari = new phpari();
        $information = [];
        if(is_array($array = $ari->asterisk()->info()))
        {
            $now = new \DateTime('NOW');
            $startup = new \DateTime($array['status']['startup_time']);
            $information[] = ['key' => 'Version', 'value' => $array['system']['version']];
            $information[] = ['key' => 'OS', 'value' => $array['build']['os']];
            $information[] = ['key' => 'Architecture', 'value' => $array['build']['machine']];
            $information[] = ['key' => 'Default language', 'value' => $array['config']['default_language']];
            $information[] = ['key' => 'Uptime', 'value' => $now->diff($startup)->format('%d Days %H:%i:%s')];
        }
        $list = new ArrayDataProvider([
            'allModels' => $information,
        ]);
        return $list;
    }
    /**
     * Populates statistics.
     * @return ArrayDataProvider
     */
    protected function  populateStatistics()
    {
        $ari = new phpari();
        $trunk = new TrunkForm();
        $extension = new ExtensionForm();
        $query = new Query();
        $information = [];
        $trunks = $trunk->listAll();
        $extensions = $extension->listAll();   
        $number = 0;
        $registered = 0;
        foreach ($extensions as $value)
        {
            $number++;
            $extension->id = $value['id'];
            $extension->getContacts() ? $registered++ : null;
        }
        $information[] = ['key' => 'Extensions', 'value' => $registered.'/'.$number.' registered'];
        $number = 0;
        $registered = 0;
        foreach ($trunks as $value)
        {
            $number++;
            !strcmp($value['status'], 'Registered') ? $registered++ : null;
        }  
        $information[] = ['key' => 'Trunks', 'value' => $registered.'/'.$number.' registered'];
        $number = 0;
        $members = 0;
        if (Yii::$app->dbAsterisk->schema->getTableSchema('queues', true) != null && Yii::$app->dbAsterisk->schema->getTableSchema('queue_members', true) != null) {
            $query->from('queues');
            $queues = $query->all(Yii::$app->dbAsterisk);
            foreach ($queues as $value) {
                $number++;
            }
            $query->from('queue_members');
            $queue_members = $query->all(Yii::$app->dbAsterisk);
            foreach ($queue_members as $value) {
                $members++;
            }
            $information[] = ['key' => 'Queues', 'value' => $number.', members: '.$members];
        }
        $number = 0;
        if (is_array($array = $ari->channels()->show())) {
            foreach ($array as $value) {
                if(strcmp($value['caller']['number'], $value['dialplan']['exten']) && $value['dialplan']['exten'] && $value['caller']['number'])
                    $number++;
            }
        }
        $information[] = ['key' => 'Active calls', 'value' => $number];
        $number = 0;
        if (Yii::$app->dbAsterisk->schema->getTableSchema('applications', true) != null) {
            $query->from('applications')->orderBy('appname');
            $applications = $query->all(Yii::$app->dbAsterisk);
            $name = '';
            foreach ($applications as $value) {
                if(strcmp($name, $value['appname']))
                {
                    $number++;
                    $name = $value['appname'];
                }                
            }
            $information[] = ['key' => 'Applications', 'value' => $number];
        }
        $list = new ArrayDataProvider([
            'allModels' => $information,
        ]);
        return $list;
    }
    /**
     * Populates active calls.
     * @return ArrayDataProvider
     */
    protected function  populateActiveCalls()
    {
        $ari = new phpari();
        $information = [];
        if(is_array($array = $ari->channels()->show()))
        {
            foreach ($array as $key => $value) {
                if(strcmp($value['caller']['number'], $value['dialplan']['exten']) && $value['dialplan']['exten'] && $value['caller']['number'])
                {
                    $now = new \DateTime('NOW');
                    $startup = new \DateTime($array[$key]['creationtime']);
                    $information[$key]['channel_id'] = $value['id'];
                    $information[$key]['from'] = $value['caller']['number'];
                    $information[$key]['to'] = $value['dialplan']['exten'];
                    $information[$key]['state'] = $value['state'];
                    $information[$key]['duration'] = $now->diff($startup)->format('%H:%i:%s');
                }
            }            
        }
        $list = new ArrayDataProvider([
            'allModels' => $information,
            'key' => 'channel_id',
            'sort' => [
                'attributes' => [
                    'channel_id', 'from', 'to', 'state', 'duration',
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $list;
    }
    /**
     * Populates array of extensions.
     * @return array
     */
    protected function populateExtensionsArray()
    {
        $extension = new ExtensionForm();
        $extensions = $extension->listAll();     
        $destinations = [];
        foreach ($extensions as $value)
        {
            $destinations['PJSIP/' . $value['id']] = $value['id'];
        }
        return $destinations;
    }
    /**
     * Renders Index page.
     * @return string
     */
    public function actionIndex()
    {
        $menu = new AsteriskSideMenu();
        return $this->render('index', [
            'menu' => $menu->getMenu(),
            'information' => $this->populateInformation(),
            'statistics' => $this->populateStatistics(),
            'calls' => $this->populateActiveCalls(),
        ]);
    }
    /**
     * Hangups active call.
     * @return string
     */
    public function actionHangup()
    {
        $ari = new phpari();
        $ari->channels()->delete(Yii::$app->request->get('id'));
        return $this->redirect(Url::toRoute('asterisk/index'));
    }
    /**
     * Connects selected extension and active call.
     * @return string
     */
    public function actionSpy()
    {        
        $menu = new AsteriskSideMenu();
        if ($array = Yii::$app->request->post()) {
            $ami = new AGI_AsteriskManager('phpari.ini');
            $ari = new phpari();
            $ami->connect();
            $channel = $ari->channels()->getDetails(Yii::$app->request->get('id'));
            $ami->Originate($array['spy'], explode('/', $channel['name'])[1], 'local', '1', 'ChanSpy', isset($array['whisper']) ? ',wq' : ',q', 3600000,
                    'Spy <'. $channel['caller']['number'] .'>', 'CDR(userfield)='.$channel['dialplan']['exten']. ' Spy: ' .explode('/', $array['spy'])[1]);
            $ami->disconnect();
            return $this->redirect(Url::toRoute('asterisk/index'));
        }             
        return $this->render('spy', [
            'destinations' => $this->populateExtensionsArray(),
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Reload Asterisk's modules pursuant to reload parameters.
     * @return string
     */
    public function actionReload()
    {
        $ami = new AGI_AsteriskManager();
        if($ami->connect() && ($AsteriskReload = Yii::$app->cache->get('AsteriskReload')))
        {
            foreach ($AsteriskReload as $value)
            {
                $ami->Command($value);
            }
            Yii::$app->cache->delete('AsteriskReload');
        }
        return Yii::$app->request->referrer ? $this->redirect(Yii::$app->request->referrer) : $this->redirect(Url::toRoute('asterisk/index'));
    }
}