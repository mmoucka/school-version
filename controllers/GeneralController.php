<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\GeneralSideMenu;
use app\models\UploadForm;

//use Plugins;
//include_once '../vendor/plugins/Plugins.php';



class GeneralController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $permissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->getId());
                            return preg_match('"general"', implode(',', array_keys($permissions)));
                        }
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        //Yii::$classMap['Class1'] = 'vendor/plugins/Plugins.php';

        $menu = new GeneralSideMenu();
        return $this->render('index', [
                    'menu' => $menu->getMenu(),
        ]);
    }

}
