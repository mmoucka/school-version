<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */

namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use app\models\asterisk\ApplicationForm;
use app\models\asterisk\ExtensionForm;
use yii\helpers\Json;
use app\models\dialplan\routeEngine;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over application. Create, Delete and Update.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class ApplicationsController extends Controller
{    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['data'],
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteApplication'],
                    ],
                    [
                        'actions' => ['updatespecial','updateivr'],
                        'allow' => true,
                        'roles' => ['asterisk_updateApplication'],
                    ],
                    [
                        'actions' => ['createspecial','createivr'],
                        'allow' => true,
                        'roles' => ['asterisk_createApplication'],
                    ],
                    [
                        'actions' => ['viewspecial','viewivr'],
                        'allow' => true,
                        'roles' => ['asterisk_viewApplication'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteApplication','asterisk_viewApplication',
                            'asterisk_updateApplication','asterisk_createApplication'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Populates array of available actions in application
     * @return array
     */
    protected function populateActions()
    {
        return [
          'goto' => 'Goto ->',
          'dial' => 'Dial ->',
          'play' => 'Play Stream ->'
        ];
    }
    /**
     * Loads available destination of actions to cache.
     */
    protected function loadDestinationArrays()
    {
        Yii::$app->cache->set('dial', $this->populateDialArray(), 1200);
        Yii::$app->cache->set('goto', $this->populateGotoArray(), 1200);
        Yii::$app->cache->set('records', $this->populatePlayArray(), 1200);
    }
    /**
     * Populates destinations of action Dial
     * @return array
     */
    protected function populateDialArray()
    {
        $extension = new ExtensionForm();
        $extensions = $extension->listAll();     
        $destinations = [];
        foreach ($extensions as $value)
        {
            $destinations[] = ['id' => 'extension,' . $value['id'], 'name' => $value['id']];
        }
        return $destinations;
    }
    /**
     * Populates destinations of action Goto
     * @return array
     */
    protected function populateGotoArray()
    {
        $query = new Query();
        $query->from('applications')->orderBy('appname');
        $destinations = [];
        $applications = $query->all(Yii::$app->dbAsterisk);
        if (isset($applications)) {
            $temp = '';
            foreach ($applications as $value)
            {
                if(strcmp($temp, $value['appname']))
                {
                    if($value['type'] == 1)
                    {
                        $destinations[] = ['id' => 'app,' . $value['appname'], 'name' => $value['appname']];
                    }
                    elseif($value['type'] == 2)
                    {
                        $destinations[] = ['id' => 'execute,' . $value['appname'], 'name' => $value['appname']];
                    }
                    $temp = $value['appname'];
                }                
            }
        }        
        if (Yii::$app->dbAsterisk->schema->getTableSchema('queues', true) != null) {
            $query = new Query();
            $query->from('queues');
            $queues = $query->all(Yii::$app->dbAsterisk);
        }
        if (isset($queues)) {
            foreach ($queues as $value) {
                $destinations[] = ['id' => 'queue,' . $value['name'], 'name' => $value['name']];
            }
        }
        return $destinations;
    }
    /**
     * Populates destinations of action Play
     * @return array
     */
    protected function populatePlayArray()
    {
        $destinations = [];
        $customRecords = '/var/asterisk/records/';
        $asteriskSounds = '/var/lib/asterisk/sounds/en/';
        $files = [];
        if(file_exists($customRecords))
        {
            $files = \yii\helpers\FileHelper::findFiles($customRecords,['only'=>['*.mp3','*.gsm', '*.wav'], 'recursive' => true]);
        }
        foreach ($files as $value) {
            $file = basename($value, '.'.pathinfo($value)['extension']);
            array_push($destinations, ['id' => 'play,' . $file, 'name' => $file]);
        }
        unset($files);
        $files = [];
        if(file_exists($asteriskSounds))
        {
            $files = \yii\helpers\FileHelper::findFiles($asteriskSounds,['only'=>['*.mp3','*.gsm', '*.wav'], 
            'recursive' => true, 'except' => ['digits/*', 'phonetic/*', 'silence/*', 'letters/*']]);
        }        
        foreach ($files as $value) {
            $file = basename($value, '.'.pathinfo($value)['extension']);
            array_push($destinations, ['id' => 'play,' . $file, 'name' => $file]);
        }
        return $destinations;
    }
    /**
     * Populates array of available records on local storage.
     * @return array
     */
    protected function populateRecordsArray()
    {
        $records = [];
        $customRecords = '/var/asterisk/records/';
        $asteriskSounds = '/var/lib/asterisk/sounds/en/';
        $files = [];
        if(file_exists($customRecords))
        {
            $files = \yii\helpers\FileHelper::findFiles('/var/asterisk/records/',['only'=>['*.mp3','*.gsm', '*.wav'], 'recursive' => true]);
        }
        foreach ($files as $value) {
            $file = basename($value, '.'.pathinfo($value)['extension']);
            $records['play,/var/asterisk/records/' . $file] = $file;
        }
        unset($files);
        $files = [];
        if(file_exists($asteriskSounds))
        {
            $files = \yii\helpers\FileHelper::findFiles('/var/lib/asterisk/sounds/en/',['only'=>['*.mp3','*.gsm', '*.wav'], 
            'recursive' => true, 'except' => ['digits/*', 'phonetic/*', 'silence/*', 'letters/*']]);
        }        
        foreach ($files as $value) {
            $file = basename($value, '.'.pathinfo($value)['extension']);
            $records['play,' . $file] = $file;
        }
        return $records;
    }
    /**
     * Populates graphical trace through IVR.
     * @param string $name name of IVR
     * @param array $seen IVR it has been to
     * @return string
     */
    public function populateTree ($name, $seen = [])
    {
        $engine = new routeEngine();
        $ivr = $engine->getIvrMenu($name);
        $tree = '';
        $tree .= '<ul>';
        if(isset($ivr['intro']))
        {
            $tree .= '<li><span class="label label-primary">Intro: Stream - ' . $ivr['intro'] . '</span></li>';
            foreach ($ivr as $key => $value) {
                if (strcmp($key, 'intro') && strcmp($key, 'keys')) {
                    $data = explode(',', $value);
                    switch ($data[0]) {
                        case 'app':
                            $tree .= '<li><span class="label label-danger">' . $key . ': Application - ' . $data[1] . '</span>';
                            if (!in_array($data[1], $seen)) {
                                $seen[] = $name;
                                $tree .= $this->populateTree($data[1], $seen);
                            }
                            $tree .= '</li>';
                            break;
                        case 'queue':
                            $tree .= '<li><span class="label label-info">' . $key . ': Queue - ' . $data[1] . '</span></li>';
                            break;
                        case 'play':
                            $tree .= '<li><span class="label label-success">' . $key . ': Stream - ' . $data[1] . '</span></li>';
                            break;
                        case 'extension':
                            $tree .= '<li><span class="label label-warning">' . $key . ': Dial - ' . $data[1] . '</span></li>';
                            break;
                        case 'execute':
                            $tree .= '<li><span class="label label-danger">' . $key . ': Execute - ' . $data[1] . '</span></li>';
                            break;
                        default:
                            $tree .= '<li><span class="label label-default">' . $key . ': Unknown </span></li>';
                            break;
                    }
                }
            }
        }        
        $tree .= '</ul>';
        return $tree;
    }
    /**
     * Renders Index page.
     * @return string
     */
    public function actionIndex()
    {
        $model = new ApplicationForm();
        $menu = new AsteriskSideMenu();
        $ivrs = new ArrayDataProvider([
            'allModels' => $model->listAll(1),
            'key' => 'appname',
            'sort' => [
                'attributes' => [
                    'appname',
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $specials = new ArrayDataProvider([
            'allModels' => $model->listAll(2),
            'key' => 'appname',
            'sort' => [
                'attributes' => [
                    'appname',
                ],
            ],             
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $tree = null;
        return $this->render('index', [
            'ivrs' => $ivrs,            
            'specials' => $specials, 
            'menu' => $menu->getMenu(),
            'tree' => $tree,
        ]);
    }
    /**
     * Deletes application
     * @return string
     */
    public function actionDelete()
    {   
        $app = new ApplicationForm();
        Yii::$app->cache->delete(Yii::$app->request->get('id'));
        $app->appname = Yii::$app->request->get('id');
        $app->delete();            
        return $this->redirect(Url::toRoute('asterisk/applications/index'));
    }
    /**
     * Renders edit IVR page and calls update function in model.
     * @return string
     */
    public function actionUpdateivr()
    {    
        $model = new ApplicationForm(['scenario' => 'updateivr']);
        $menu = new AsteriskSideMenu();
        $model->oldappname = $model->appname = Yii::$app->request->get('id');
        $this->loadDestinationArrays();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->updateivr()) {
                \app\models\dialplan\routeEngine::cacheIVR(Yii::$app->request->get('id'));
                return $this->redirect(Url::toRoute('asterisk/applications/index'));
            }
        }             
        return $this->render('updateivr', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
            'actions' => $this->populateActions(),
            'selected' => $model->listIvr(),
            'records' => $this->populateRecordsArray(),
            'keys' => $model->keys,
        ]);
    }
    /**
     * Renders create IVR page and calls create function in model.
     * @return string
     */
    public function actionCreateivr()
    {        
        $model = new ApplicationForm(['scenario' => 'createivr']);
        $menu = new AsteriskSideMenu();
        $this->loadDestinationArrays();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->createivr()) {
                \app\models\dialplan\routeEngine::cacheIVR(Yii::$app->request->get('id'));
                return $this->redirect(Url::toRoute('asterisk/applications/index'));
            }
        }             
        return $this->render('createivr', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
            'actions' => $this->populateActions(),
            'records' => $this->populateRecordsArray(),
            'keys' => $model->keys,
        ]);
    }
    /**
     * Renders index page with tree of application.
     * @return string
     */
    public function actionViewivr()
    {
        $model = new ApplicationForm(['scenario' => 'viewivr']);
        $menu = new AsteriskSideMenu();
        $ivrs = new ArrayDataProvider([
            'allModels' => $model->listAll(1),
            'key' => 'appname',
            'sort' => [
                'attributes' => [
                    'appname',
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $specials = new ArrayDataProvider([
            'allModels' => $model->listAll(2),
            'key' => 'appname',
            'sort' => [
                'attributes' => [
                    'appname',
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $tree['title'] = Yii::$app->request->get('id');
        $tree['data'] = $this->populateTree(Yii::$app->request->get('id'));
        return $this->render('index', [
            'ivrs' => $ivrs,   
            'specials' => $specials,  
            'menu' => $menu->getMenu(),
            'tree' => $tree,
        ]);
    }
    /**
     * Renders create special application page and calls create function in model.
     * @return string
     */
    public function actionCreatespecial()
    {        
        $model = new ApplicationForm(['scenario' => 'createspecial']);
        $menu = new AsteriskSideMenu();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->createspecial()) {
                return $this->redirect(Url::toRoute('asterisk/applications/index'));
            }
        }             
        return $this->render('createspecial', [
            'apps' => $model->apps,
            'model' => $model,         
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Renders edit special application page and calls edit function in model.
     * @return string
     */
    public function actionUpdatespecial()
    {        
        $model = new ApplicationForm(['scenario' => 'updateivr']);
        $menu = new AsteriskSideMenu();
        $model->oldappname = $model->appname = Yii::$app->request->get('id');
        $model->initSpecial();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->updatespecial()) {
                return $this->redirect(Url::toRoute('asterisk/applications/index'));
            }
        }             
        return $this->render('updatespecial', [
            'apps' => $model->apps,
            'model' => $model,         
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Renders view special application page.
     * @return string
     */
    public function actionViewspecial()
    {        
        $model = new ApplicationForm(['scenario' => 'viewspecial']);
        $menu = new AsteriskSideMenu();
        $model->oldappname = $model->appname = Yii::$app->request->get('id');
        $model->initSpecial();   
        return $this->render('viewspecial', [
            'apps' => $model->apps,
            'model' => $model,         
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Loads data for depdrop objects.
     * @return string
     */
    public function actionData() {
        $out = [];
        if (Yii::$app->request->post('depdrop_parents')) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                $cat_id = $parents[0];
                if (!strcmp('goto', $cat_id)) {
                    $out = Yii::$app->cache->get('goto');
                } else if (!strcmp('dial', $cat_id)) {
                    $out = Yii::$app->cache->get('dial');
                } else if (!strcmp('play', $cat_id)) {
                    $out = Yii::$app->cache->get('records');
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}