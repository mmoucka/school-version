<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\asterisk\CdrSearch;
use app\models\asterisk\Cdr;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles action over call history model.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */


class CdrsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_callHistory'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }    
    /**
     * Renders index page and results of search.
     * @return type
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            $entry = Cdr::findOne(Yii::$app->request->post('editableKey'));
            $post = [];
            $posted = current($_POST['Cdr']);
            $post['Cdr'] = $posted;
            if($entry->load($post))
            {
                $entry->update();
            }	
            echo \yii\helpers\Json::encode(['output'=>$entry->note, 'message'=>$entry->getErrors()]);
            return;
        }
        $menu = new AsteriskSideMenu();  
        $model = new CdrSearch(['scenario' => 'search']);                  
        $model->load(Yii::$app->request->post());
        $result = $model->search();   
        return $this->render('index', [
            'list' => $result,
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }    
}