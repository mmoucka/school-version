<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;
use app\models\asterisk\QueueForm;
use app\models\asterisk\TrunkForm;
use app\models\asterisk\DialoutEntry;
use app\models\asterisk\DialoutForm;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over call history model.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */


class DialoutsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteDialout'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['asterisk_updateDialout'],
                    ],
                    [
                        'actions' => ['add'],
                        'allow' => true,
                        'roles' => ['asterisk_addDialout'],
                    ],
                    [
                        'actions' => ['import'],
                        'allow' => true,
                        'roles' => ['asterisk_importDialout'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['asterisk_viewDialout'],
                    ],
                    [
                        'actions' => ['start'],
                        'allow' => true,
                        'roles' => ['asterisk_startDialout'],
                    ],
                    [
                        'actions' => ['stop'],
                        'allow' => true,
                        'roles' => ['asterisk_stopDialout'],
                    ],
                    [
                        'actions' => ['restartstatus'],
                        'allow' => true,
                        'roles' => ['asterisk_restartStatusDialoutEntry'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteDialout','asterisk_updateDialout',
                            'asterisk_addDialout','asterisk_importDialout','asterisk_viewDialout',
                            'asterisk_startDialout','asterisk_stopDialout','asterisk_restartStatusDialoutEntry'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }    
    /**
     * Populate array of queues.
     * @return array
     */
    protected function populateQueuesArray()
    {
        $queue = new QueueForm();
        $queues = $queue->listAll();     
        $destinations = [];
        foreach ($queues as $value)
        {
            $destinations[$value['name']] = $value['name'];
        }
        return $destinations;
    }
    protected function populateTrunksArray()
    {
        $trunk = new TrunkForm();
        $trunks = $trunk->listAll();     
        $destinations = [];
        foreach ($trunks as $value)
        {
            $destinations[$value['id']] = $value['id'];
        }
        return $destinations;
    }

    public function actionIndex()
    {
        $menu = new AsteriskSideMenu();    
        $model = new DialoutForm();
        $status = Yii::$app->cache->get('dialout') ? Yii::$app->cache->get('dialout') : 'stopped';
        $list = new ArrayDataProvider([
            'allModels' => $model->listAll(),
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'number', 'destqueue', 'callerid', 'trunk', 'attempts', 'status'
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('index', [
            'list' => $list,
            'status' => $status,
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }  
    public function actionAdd()
    {
        $model = new DialoutForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu(); 
        if ($model->load(Yii::$app->request->post())) {
            if($model->create())
            {
                return $this->redirect(Url::toRoute('asterisk/dialouts/index'));
            }
        }
        return $this->render('add', [
            'queues' => $this->populateQueuesArray(),
            'trunks' => $this->populateTrunksArray(),
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionUpdate()
    {
        $model = new DialoutForm(['scenario' => 'update']);
        $menu = new AsteriskSideMenu(); 
        $model->id = Yii::$app->request->get('id');
        $model->initialize();
        if ($model->load(Yii::$app->request->post())) {
            if($model->update())
            {
                return $this->redirect(Url::toRoute('asterisk/dialouts/index'));
            }
        }
        return $this->render('update', [
            'queues' => $this->populateQueuesArray(),
            'trunks' => $this->populateTrunksArray(),
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionImport()
    {
        $model = new DialoutForm(['scenario' => 'import']);
        $menu = new AsteriskSideMenu(); 
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->importCsv())
            {
                return $this->redirect(Url::toRoute('asterisk/dialouts/index'));
            }
        }
        return $this->render('import', [
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionDelete()
    {
        $entry = DialoutEntry::findOne(['id' => Yii::$app->request->get('id')]);
        if($entry)
        {
            $entry->delete();
        }
        return $this->redirect(Url::toRoute('asterisk/dialouts/index'));
    }
    public function actionStart()
    {
        $base = Yii::$app->basePath;
        if(strcmp('running', Yii::$app->cache->get('dialout')))
        {
            shell_exec("nohup php $base/yii dialoutcmd/start > /dev/null 2> /dev/null &");
        }
        return $this->redirect(Url::toRoute('asterisk/dialouts/index'));
    }
    public function actionStop()
    {
        $base = Yii::$app->basePath;
        if(!strcmp('running', Yii::$app->cache->get('dialout')))
        {
            shell_exec("nohup php $base/yii dialoutcmd/stop > /dev/null 2> /dev/null &");
        }
        return $this->redirect(Url::toRoute('asterisk/dialouts/index'));
    }
    public function actionRestartstatus()
    {
        $entry = DialoutEntry::findOne(['id' => Yii::$app->request->get('id')]);
        $entry->status = 'Waiting';
        $entry->attempts = 0;
        $entry->update();
        return $this->redirect(Url::toRoute('asterisk/dialouts/index'));
    }
}