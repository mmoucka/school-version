<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\asterisk\RouteForm;
use app\models\asterisk\Route;
use app\models\asterisk\TrunkForm;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over routes. Create, Sort, Delete and Update.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class EmergencynumsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteRoute'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['asterisk_updateRoute'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['asterisk_createRoute'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['asterisk_viewRoute'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteRoute','asterisk_updateRoute',
                            'asterisk_createRoute','asterisk_viewRoute','asterisk_sortRoute'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Populetes routes for sortinput object.
     * @param array $array array of routes
     * @param boolean $disabled
     * @return array
     */
    protected function populateRoutes($array, $disabled = false)
    {
        $routes = [];
        foreach ($array as $key => $value) {
            $routes[$value['id']] = ['content' => 'Number: <b>'. $value['pattern']. "</b><br>Destination: <b>" .
                explode(',', $value['data'])[1] . '</b>', 'disabled' => $disabled];
        }
        return $routes;
    }
    /**
     * Populates array of available destinations.
     * @param int $type 
     * @return array
     */
    protected function populateDestinationsArray()
    {
        $trunk = new TrunkForm();
        $trunks = $trunk->listAll();  
        $destinations = [];
        foreach ($trunks as $value)
        {
            $destinations['Trunk']['trunk,'.$value['id']] = $value['id'];
        }  
        return $destinations;
    }
    /**
     * Renders Index page. Allows to sort routes -> edit metric.
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionIndex()
    {
        $model = new RouteForm();
        $disabled = false;
        if(!Yii::$app->user->can('asterisk_sortRoute') && !Yii::$app->user->can('asterisk_updateRoute')  && !Yii::$app->user->can('asterisk_createRoute') 
                && Yii::$app->user->can('asterisk_viewRoute') && !isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin']) )
        {
            $disabled = true;
        }
        $numbers = $this->populateRoutes($model->listAll(1), $disabled);
        
        if($post = Yii::$app->request->post())
        {
            if((!Yii::$app->user->can('asterisk_sortRoute') && !isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin'])))
            {
                throw new \yii\web\HttpException(403, 'Forbidden.');
            }
            if(isset($post['save-button']))
            {
                $model->sort(Yii::$app->request->post('numbers'));
            }
            return $this->redirect(Url::toRoute('asterisk/emergencynums/index'));
        }
        $menu = new AsteriskSideMenu();
        return $this->render('index', [
            'numbers' => $numbers,
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Deletes route.
     * @return string
     */
    public function actionDelete()
    {   
        $route = Route::findOne(Yii::$app->request->get('id'));
        if($route)
        {
            $route->delete();
        }
        return $this->redirect(Url::toRoute('asterisk/emergencynums/index'));
    }
    /**
     * Renders Edit page and calls update function in model.
     * @return string
     */
    public function actionUpdate()
    {    
        $model = new RouteForm(['scenario' => 'update']);
        $route = Route::findOne(Yii::$app->request->get('id'));
        $menu = new AsteriskSideMenu();
        $model->id = $route->id;
        $model->pattern = $route->pattern;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                return $this->redirect(Url::toRoute('asterisk/emergencynums/index'));
            }
        }             
        return $this->render('update', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
            'destinations' => $this->populateDestinationsArray(),
            'selected' => $route->data,
        ]);
    }
    /**
     * Renders Create Inbound page and calls create function in model.
     * @return string
     */
    public function actionCreate()
    {        
        $model = new RouteForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu();
        $model->type = 1;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                return $this->redirect(Url::toRoute('asterisk/emergencynums/index'));
            }
        }             
        return $this->render('create', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
            'destinations' => $this->populateDestinationsArray(),
        ]);
    }
}