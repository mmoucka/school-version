<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use app\models\asterisk\ExtensionForm;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over extensions. Create, Delete and Update.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class ExtensionsController extends Controller
{
    private $codecs  = [
        'alaw' => ['content' => 'g711a'],
        'ulaw' => ['content' => 'g711u'],
        'g722' => ['content' => 'g722'],
        'g729' => ['content' => 'g729'],        
    ];
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteExtension'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['asterisk_updateExtension'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['asterisk_createExtension'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['asterisk_viewExtension'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteExtension','asterisk_updateExtension',
                            'asterisk_createExtension','asterisk_viewExtension'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
               ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Populates array of allowed codecs for droplist object.
     * @param array $allow
     * @param boolean $disabled true if data in droplist have to disabled.
     * @return type
     */
    protected function populateAllow($allow, $disabled = false)
    {
        if(empty($allow))
        {
            return [];
        }
        $codecs = explode(',', $allow);
        $allow = [];
        foreach ($codecs as $value) {
            if(!strcmp($value, 'alaw'))
            {
                $allow[$value] = ['content' => 'g711a', 'disabled' => $disabled];
            }
            else if(!strcmp($value, 'ulaw'))
            {
                $allow[$value] = ['content' => 'g711u', 'disabled' => $disabled];
            }
            else {
                $allow[$value] = ['content' => $value, 'disabled' => $disabled];
            }
        }
        return $allow;
    }
    /**
     * Renders index page.
     * @return string
     */
    public function actionIndex()
    {
        $model = new ExtensionForm();
        $list = new ArrayDataProvider([
            'allModels' => $model->listAll(),
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'id', 'username', 'contacts','user-agents'
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $menu = new AsteriskSideMenu();
        return $this->render('index', [
            'list' => $list,
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Deletes extension.
     * @return string
     */
    public function actionDelete()
    {   
        $model = new ExtensionForm();
        $model->id = Yii::$app->request->get('id');
        $model->delete();
        return $this->redirect(Url::toRoute('asterisk/extensions/index'));
    }
    /**
     * Renders update page and calls update function in model.
     * @return string
     */
    public function actionUpdate()
    {    
        $model = new ExtensionForm(['scenario' => 'update']);
        $menu = new AsteriskSideMenu();
        $model->oldid = $model->id = Yii::$app->request->get('id');
        $model->initialize();
        $model->oldusername = $model->username;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                return $this->redirect(Url::toRoute('asterisk/extensions/index'));
            }
        }             
        return $this->render('update', [
            'model' => $model,
            'allowed' => $this->populateAllow($model->allow),
            'codecs' => $this->codecs,
            'advanced' => $model->getAdvanced(),
            'menu' => $menu->getMenu(),
            'advancedNames' => $model->getAdvancedNames(),
            'schema' => $model->getSchema(),
        ]);
    }
    /**
     * Renders create page and calls create function in model.
     * @return string
     */
    public function actionCreate()
    {        
        $model = new ExtensionForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu();
        $advanced = [];
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                return $this->redirect(Url::toRoute('asterisk/extensions/index'));
            }
        }             
        return $this->render('create', [
            'model' => $model,      
            'codecs' => $this->codecs,
            'advanced' => $advanced,
            'menu' => $menu->getMenu(),
            'advancedNames' => $model->getAdvancedNames(),
            'schema' => $model->getSchema(),
        ]);
    }
    /**
     * Renders view page.
     * @return string
     */
    public function actionView()
    {
        $model = new ExtensionForm(['scenario' => 'view']);
        $menu = new AsteriskSideMenu();
        $model->id = Yii::$app->request->get('id');
        $model->initialize();
        return $this->render('view', [
            'model' => $model,
            'allowed' => $this->populateAllow($model->allow, true),
            'advanced' => $model->advanced,
            'menu' => $menu->getMenu(),
            'advancedNames' => $model->getAdvancedNames(),
            'schema' => $model->getSchema(),
        ]);
    }
}