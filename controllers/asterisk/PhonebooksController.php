<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;
use app\models\asterisk\ExtensionForm;
use app\models\asterisk\PhoneBookForm;
use app\models\asterisk\PhoneBookSearch;
use app\models\asterisk\PhoneBookEntry;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles action over user's phonebooks.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */


class PhonebooksController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [                    
                    [
                        'actions' => ['add'],
                        'allow' => true,
                        'roles' => ['asterisk_addToPhoneBook'],
                    ],
                    [
                        'actions' => ['import'],
                        'allow' => true,
                        'roles' => ['asterisk_importToPhoneBook'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['asterisk_viewPhoneBook'],
                    ],
                    [
                        'actions' => ['delete', ],
                        'allow' => true,
                        'roles' => ['asterisk_deleteInPhoneBook'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_addToPhoneBook', 'asterisk_importToPhoneBook', 'asterisk_viewPhoneBook', 'asterisk_deleteInPhoneBook'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }    
    /**
     * Renders index page and results of search.
     * @return type
     */
    public function actionIndex()
    {
        $menu = new AsteriskSideMenu();    
        $model = new ExtensionForm();
        $extensions = $model->listAll();
        foreach ($extensions as $key => $value)
        {
            $value['has_phonebook'] = PhoneBookForm::hasPhoneBook($value['id']) ? 'yes' : 'no';
            $extensions[$key] = $value;
        }
        $list = new ArrayDataProvider([
            'allModels' => $extensions,
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'id', 'has_phonebook'
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('index', [
            'list' => $list,
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }  
    public function actionAdd()
    {
        $model = new PhoneBookForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu(); 
        $model->auth = Yii::$app->request->get('id');
        if ($model->load(Yii::$app->request->post())) {
            if($model->create())
            {
                return $this->redirect(Url::toRoute('asterisk/phonebooks/index'));
            }
        }
        return $this->render('add', [
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionImport()
    {
        $model = new PhoneBookForm(['scenario' => 'import']);
        $menu = new AsteriskSideMenu(); 
        $model->auth = Yii::$app->request->get('id');
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->importCsv())
            {
                return $this->redirect(Url::toRoute('asterisk/phonebooks/index'));
            }
        }
        return $this->render('import', [
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionView()
    {
        if (Yii::$app->request->post('hasEditable')) {
            $entry = PhoneBookEntry::findOne(Yii::$app->request->post('editableKey'));
            $post = [];
            $posted = current($_POST['PhoneBookEntry']);
            $post['PhoneBookEntry'] = $posted;
            if($entry->load($post))
            {
                $entry->update();
            }	
            echo \yii\helpers\Json::encode(['output'=>'', 'message'=>$entry->getErrors()]);
            return;
        }
        $menu = new AsteriskSideMenu();  
        $model = new PhonebookForm(['scenario' => 'owner']);
        $searchModel = new PhoneBookSearch();
        $searchModel->auth = $model->auth = Yii::$app->request->get('id');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('view', [
            'list' => $dataProvider,            
            'searchModel' => $searchModel,
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    } 
    public function actionDelete ()
    {
        $entry = PhoneBookEntry::findOne(['id' => Yii::$app->request->get('id')]);
        if($entry)
        {
            $entry->delete();
        }
        return $this->redirect(Url::toRoute('asterisk/phonebooks/index'));
    }
}