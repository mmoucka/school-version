<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use app\models\asterisk\QueueForm;
use app\models\asterisk\ExtensionForm;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over queues. Create, Delete and Update.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class QueuesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteQueue'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['asterisk_updateQueue'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['asterisk_createQueue'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['asterisk_viewQueue'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteQueue','asterisk_updateQueue',
                            'asterisk_createQueue','asterisk_viewQueue'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Renders index page.
     * @return string
     */
    public function actionIndex()
    {
        $model = new QueueForm();
        $list = new ArrayDataProvider([
            'allModels' => $model->listAll(),
            'key' => 'name',
            'sort' => [
                'attributes' => [
                    'name', 'members', 'nextqueue'
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $menu = new AsteriskSideMenu();
        return $this->render('index', [
            'list' => $list,
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Populates array of available records on local storage
     * @return array
     */
    protected function populateRecordsArray()
    {
        $records = [];
        $customRecords = '/var/asterisk/records/';
        $asteriskSounds = '/var/lib/asterisk/sounds/en/';
        $files = [];
        if(file_exists($customRecords))
        {
            $files = \yii\helpers\FileHelper::findFiles($customRecords,['only'=>['*.mp3','*.gsm', '*.wav'], 'recursive' => true]);
        }
        foreach ($files as $value) {
            $file = basename($value, '.'.pathinfo($value)['extension']);
            $records['/var/asterisk/records/' . $file] = $file;
        }
        unset($files);
        $files = [];
        if(file_exists($asteriskSounds))
        {
            $files = \yii\helpers\FileHelper::findFiles($asteriskSounds,['only'=>['*.mp3','*.gsm', '*.wav'], 
            'recursive' => true, 'except' => ['digits/*', 'phonetic/*', 'silence/*', 'letters/*']]);
        }
        foreach ($files as $value) {
            $file = basename($value, '.'.pathinfo($value)['extension']);
            $records[$file] = $file;
        }
        return $records;
    }
    /**
     * Populates array of extensions.
     * @return array
     */
    protected function populateExtArray()
    {
        $extension = new ExtensionForm();
        $extensions = $extension->listAll();     
        $destinations = [];
        foreach ($extensions as $value)
        {
            $destinations[$value['id']] = ['content' => $value['id']];
        }
        return $destinations;
    }
    /**
     * Populate array of queues.
     * @return array
     */
    protected function populateQueuesArray()
    {
        $queue = new QueueForm();
        $queues = $queue->listAll();     
        $destinations = [];
        foreach ($queues as $value)
        {
            $destinations[$value['name']] = $value['name'];
        }
        return $destinations;
    }
    /**
     * Deletes queue.
     * @return string
     */
    public function actionDelete()
    {   
        $model = new QueueForm();
        $model->name = Yii::$app->request->get('id');
        $model->delete();
        return $this->redirect(Url::toRoute('asterisk/queues/index'));
    }
    /**
     * Renders edit page and calls update function in model.
     * @return string
     */
    public function actionUpdate()
    {    
        $model = new QueueForm(['scenario' => 'update']);
        $menu = new AsteriskSideMenu();
        $model->oldname = $model->name = Yii::$app->request->get('id');
        $model->initialize();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                return $this->redirect(Url::toRoute('asterisk/queues/index'));
            }
        }             
        return $this->render('update', [
            'extensions' => $this->populateExtArray(),
            'records' => $this->populateRecordsArray(),
            'queues' => $this->populateQueuesArray(),
            'withaudio' => $model->withaudio,
            'model' => $model,          
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Renders create page and calls create function in model.
     * @return string
     */
    public function actionCreate()
    {        
        $model = new QueueForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                return $this->redirect(Url::toRoute('asterisk/queues/index'));
            }
        }             
        return $this->render('create', [
            'extensions' => $this->populateExtArray(),
            'records' => $this->populateRecordsArray(),
            'queues' => $this->populateQueuesArray(),
            'withaudio' => $model->withaudio,
            'model' => $model,          
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Renders view page-
     * @return string
     */
    public function actionView()
    {
        $model = new QueueForm(['scenario' => 'view']);
        $menu = new AsteriskSideMenu();
        $model->name = Yii::$app->request->get('id');
        $model->initialize();
        return $this->render('view', [
            'extensions' => $this->populateExtArray(),
            'records' => $this->populateRecordsArray(),
            'queues' => $this->populateQueuesArray(),
            'withaudio' => $model->withaudio,
            'model' => $model,          
            'menu' => $menu->getMenu(),
        ]);
    }
}