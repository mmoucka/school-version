<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;
use app\models\asterisk\RecordForm;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over records. Upload, Delete and Download.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */


class RecordsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete', 'deletemoh'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteRecord'],
                    ],
                    [
                        'actions' => ['upload', 'uploadmoh'],
                        'allow' => true,
                        'roles' => ['asterisk_uploadRecord'],
                    ],
                    [
                        'actions' => ['download', 'downloadmoh'],
                        'allow' => true,
                        'roles' => ['asterisk_downloadRecord'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteRecord','asterisk_uploadRecord',
                            'asterisk_downloadRecord'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }    
    /**
     * Renders Index page.
     * @return string
     */
    public function actionIndex()
    {
        $model = new RecordForm();
        $menu = new AsteriskSideMenu();
        $list = new ArrayDataProvider([
            'allModels' => $model->listAllRecords(),
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'id', 'size'
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $mohs = new ArrayDataProvider([
            'allModels' => $model->listAllMohs(),
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'id', 'size'
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('index', [
            'list' => $list,
            'mohs' => $mohs,
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Deletes record from local storage.
     * @return string
     */
    public function actionDelete()
    {   
        $filename = basename(Yii::$app->request->get('id'));
        $file = '/var/asterisk/records/'.$filename;
        if(file_exists($file))
        {
            unlink($file);
        }
        return $this->redirect(Url::toRoute('asterisk/records/index'));
    }
    /**
     * Sends record to user.
     * @return string
     */
    public function actionDownload()
    {   
        $filename = basename(Yii::$app->request->get('id'));
        $file = '/var/asterisk/records/'.$filename;
        if(file_exists($file))
        {
            return Yii::$app->getResponse()->sendFile($file);
        }
        return $this->redirect(Url::toRoute('asterisk/records/index'));
    }
    /**
     * Renders record upload page and calls upload function in model.
     * @return string
     */
    public function actionUpload()
    {        
        $model = new RecordForm(['scenario' => 'upload']);
        $menu = new AsteriskSideMenu();
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
                return $this->redirect(Url::toRoute('asterisk/records/index'));
            }
        }             
        return $this->render('upload', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Deletes record from local storage.
     * @return string
     */
    public function actionDeletemoh()
    {   
        $filename = basename(Yii::$app->request->get('id'));
        $file = '/var/asterisk/moh/'.$filename;
        if(file_exists($file))
        {
            unlink($file);
            $reload = ['moh' => 'moh reload'];
            if ($AsteriskReload = Yii::$app->cache->get('AsteriskReload')) {
                $AsteriskReload['pjsip'] = 'pjsip reload';
                $AsteriskReload = Yii::$app->cache->set('AsteriskReload', $AsteriskReload);
            } else {
                $AsteriskReload = Yii::$app->cache->set('AsteriskReload', $reload);
            }
        }        
        return $this->redirect(Url::toRoute('asterisk/records/index'));
    }
    /**
     * Sends record to user.
     * @return string
     */
    public function actionDownloadmoh()
    {   
        $filename = basename(Yii::$app->request->get('id'));
        $file = '/var/asterisk/moh/'.$filename;
        if(file_exists($file))
        {
            return Yii::$app->getResponse()->sendFile($file);
        }
        return $this->redirect(Url::toRoute('asterisk/records/index'));
    }
    /**
     * Renders record upload page and calls upload function in model.
     * @return string
     */
    public function actionUploadmoh()
    {        
        $model = new RecordForm(['scenario' => 'upload']);
        $menu = new AsteriskSideMenu();
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->uploadmoh()) {
                return $this->redirect(Url::toRoute('asterisk/records/index'));
            }
            $reload = ['moh' => 'moh reload'];
            if ($AsteriskReload = Yii::$app->cache->get('AsteriskReload')) {
                $AsteriskReload['pjsip'] = 'pjsip reload';
                $AsteriskReload = Yii::$app->cache->set('AsteriskReload', $AsteriskReload);
            } else {
                $AsteriskReload = Yii::$app->cache->set('AsteriskReload', $reload);
            }
        }                     
        return $this->render('upload', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
        ]);
    }
}