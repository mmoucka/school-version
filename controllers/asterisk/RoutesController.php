<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\db\Query;
use app\models\asterisk\RouteForm;
use app\models\asterisk\Route;
use app\models\asterisk\TrunkForm;
use app\models\asterisk\ExtensionForm;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over routes. Create, Sort, Delete and Update.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class RoutesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteRoute'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['asterisk_updateRoute'],
                    ],
                    [
                        'actions' => ['create_outbound','create_inbound'],
                        'allow' => true,
                        'roles' => ['asterisk_createRoute'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['asterisk_viewRoute'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteRoute','asterisk_updateRoute',
                            'asterisk_createRoute','asterisk_viewRoute','asterisk_sortRoute'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Populetes routes for sortinput object.
     * @param array $array array of routes
     * @param boolean $disabled
     * @return array
     */
    protected function populateRoutes($array, $disabled = false)
    {
        $routes = [];
        foreach ($array as $key => $value) {
            $routes[$value['id']] = ['content' => 'Match: <b>'. $value['pattern']. "</b><br>Destination: <b>" .
                explode(',', $value['data'])[1] . '</b>', 'disabled' => $disabled];
        }
        return $routes;
    }
    /**
     * Populates array of available destinations.
     * @param int $type 
     * @return array
     */
    protected function populateDestinationsArray($type = 4)
    {
        $trunk = new TrunkForm();
        $extension = new ExtensionForm();
        $query = new Query();
        $trunks = $trunk->listAll();
        $extensions = $extension->listAll();   
        $destinations = [];
        foreach ($extensions as $value)
        {
            $destinations['Extension']['extension,'.$value['id']] = $value['id'];
        }
        foreach ($trunks as $value)
        {
            $destinations['Trunk']['trunk,'.$value['id']] = $value['id'];
        }  
        if($type == 5)
        {
            if (Yii::$app->dbAsterisk->schema->getTableSchema('applications', true) != null) {
                $query->from('applications');
                $applications = $query->all(Yii::$app->dbAsterisk);
            }         
            if (Yii::$app->dbAsterisk->schema->getTableSchema('queues', true) != null) {
                $query->from('queues');
                $queues = $query->all(Yii::$app->dbAsterisk);
            }            
            if(isset($applications))
            {
                foreach ($applications as $value) {
                    if($value['type'] == 1)
                    {
                        $destinations['Application']['app,' . $value['appname']] = $value['appname'];
                    }
                    elseif($value['type'] == 2)
                    {
                        $destinations['Application']['execute,' . $value['appname']] = $value['appname'];
                    }
                }
            }
            if(isset($queues))
            {
                foreach ($queues as $value) {
                    $destinations['Queue']['queue,' . $value['name']] = $value['name'];
                }
            }
        }
        return $destinations;
    }
    /**
     * Renders Index page. Allows to sort routes -> edit metric.
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionIndex()
    {
        $model = new RouteForm();
        $disabled = false;
        if(!Yii::$app->user->can('asterisk_sortRoute') && !Yii::$app->user->can('asterisk_updateRoute')  && !Yii::$app->user->can('asterisk_createRoute') 
                && Yii::$app->user->can('asterisk_viewRoute') && !isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin']) )
        {
            $disabled = true;
        }
        $inbounds = $this->populateRoutes($model->listAll(5), $disabled);
        $outbounds = $this->populateRoutes($model->listAll(4), $disabled);
        
        if($post = Yii::$app->request->post())
        {
            if((!Yii::$app->user->can('asterisk_sortRoute') && !isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin'])))
            {
                throw new \yii\web\HttpException(403, 'Forbidden.');
            }
            if(isset($post['inboundSave-button']))
            {
                $model->sort (Yii::$app->request->post('inbound'));
            }
            else if(isset($post['outboundSave-button']))
            {
                $model->sort (Yii::$app->request->post('outbound'));
            }
            return $this->redirect(Url::toRoute('asterisk/routes/index'));
        }
        $menu = new AsteriskSideMenu();
        return $this->render('index', [
            'inbounds' => $inbounds,
            'outbounds' => $outbounds,
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Deletes route.
     * @return string
     */
    public function actionDelete()
    {   
        $route = Route::findOne(Yii::$app->request->get('id'));
        if($route)
        {
            $route->delete();
        }
        return $this->redirect(Url::toRoute('asterisk/routes/index'));
    }
    /**
     * Renders Edit page and calls update function in model.
     * @return string
     */
    public function actionUpdate()
    {    
        $model = new RouteForm(['scenario' => 'update']);
        $route = Route::findOne(Yii::$app->request->get('id'));
        $menu = new AsteriskSideMenu();
        $model->id = $route->id;
        $model->pattern = $route->pattern;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                return $this->redirect(Url::toRoute('asterisk/routes/index'));
            }
        }             
        return $this->render('update', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
            'destinations' => $this->populateDestinationsArray($route->type),
            'selected' => $route->data,
        ]);
    }
    /**
     * Renders Create Outbound page and calls create function in model.
     * @return string
     */
    public function actionCreate_outbound()
    {        
        $model = new RouteForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu();
        $selected = [];
        $model->type = 4;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                return $this->redirect(Url::toRoute('asterisk/routes/index'));
            }
        }             
        return $this->render('create', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
            'destinations' => $this->populateDestinationsArray(),
            'selected' => $selected,
        ]);
    }
    /**
     * Renders Create Inbound page and calls create function in model.
     * @return string
     */
    public function actionCreate_inbound()
    {        
        $model = new RouteForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu();
        $selected = [];
        $model->type = 5;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                return $this->redirect(Url::toRoute('asterisk/routes/index'));
            }
        }             
        return $this->render('create', [
            'model' => $model,         
            'menu' => $menu->getMenu(),
            'destinations' => $this->populateDestinationsArray(5),
            'selected' => $selected,
        ]);
    }
}