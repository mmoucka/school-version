<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\asterisk\CdrSearch;
use app\models\asterisk\PhoneBookSearch;
use app\models\asterisk\PhoneBookForm;
use app\models\asterisk\PhoneBookEntry;
use app\models\asterisk\SelfcareSideMenu;
use app\models\asterisk\Cdr;

/**
 * Handles action over call history model.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */


class SelfcareController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return isset(Yii::$app->user->identity->isSipUser);
                        }
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }    
    /**
     * Renders index page and results of search.
     * @return type
     */
    public function actionIndex()
    {
        $menu = new SelfcareSideMenu();    
        $model = new CdrSearch(['scenario' => 'search']);
        $model->fromToOperator = 'or';
        if (Yii::$app->request->post('hasEditable')) {
            $entry = Cdr::findOne(Yii::$app->request->post('editableKey'));
            $post = [];
            $posted = current($_POST['Cdr']);
            $post['Cdr'] = $posted;
            if($entry->load($post))
            {
                $entry->update();
            }	
            echo \yii\helpers\Json::encode(['output'=>$entry->note, 'message'=>$entry->getErrors()]);
            //return;
        }
        $model->load(Yii::$app->request->post());
        $result = $model->search(Yii::$app->user->identity->id);
        return $this->render('index', [
            'list' => $result,
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }    
    public function actionPhonebook()
    {
        $menu = new SelfcareSideMenu();    
        $model = new PhonebookForm(['scenario' => 'owner']);
        $searchModel = new PhoneBookSearch();
        $searchModel->auth = $model->auth = Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (Yii::$app->request->post('hasEditable')) {
            $entry = PhoneBookEntry::findOne(Yii::$app->request->post('editableKey'));
            $post = [];
            $posted = current($_POST['PhoneBookEntry']);
            $post['PhoneBookEntry'] = $posted;
            if($entry->load($post))
            {
                $entry->update();
            }	
            echo \yii\helpers\Json::encode(['output'=>'', 'message'=>$entry->getErrors()]);
            return;
        }
        return $this->render('phonebook', [
            'list' => $dataProvider,            
            'searchModel' => $searchModel,
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    } 
    public function actionCreatephonebookentry()
    {
        $model = new PhoneBookForm(['scenario' => 'owner']);
        $model->auth = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post())) {
            $model->create();
        }
        return $this->redirect(Url::toRoute('asterisk/selfcare/phonebook'));
    }
    public function actionDeletephonebookentry ()
    {
        $entry = PhoneBookEntry::findOne(['id' => Yii::$app->request->get('id'), 'auth' => Yii::$app->user->identity->id]);
        if($entry)
        {
            $entry->delete();
        }
        return $this->redirect(Url::toRoute('asterisk/selfcare/phonebook'));
    }
    public function actionDeleteallphonebookentries ()
    {
        PhoneBookForm::deleteAllEntries(Yii::$app->user->identity->id);
        return $this->redirect(Url::toRoute('asterisk/selfcare/phonebook'));
    }
    public function actionCall()
    {
        $entry = PhoneBookEntry::findOne(Yii::$app->request->get('id'));
        //call to $entry->number;         
        return $this->redirect(Url::toRoute('asterisk/selfcare/phonebook'));
    }
}