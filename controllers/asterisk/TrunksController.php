<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\asterisk;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use app\models\asterisk\TrunkForm;
use app\models\asterisk\AsteriskSideMenu;

/**
 * Handles actions over trunks. Create, Delete and Update.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class TrunksController extends Controller
{
    private $codecs  = [
        'alaw' => ['content' => 'g711a'],
        'ulaw' => ['content' => 'g711u'],
        'g722' => ['content' => 'g722'],
        'g729' => ['content' => 'g729'],        
    ];
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteTrunk'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['asterisk_updateTrunk'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['asterisk_createTrunk'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['asterisk_viewTrunk'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['asterisk_deleteTrunk','asterisk_updateTrunk',
                            'asterisk_createTrunk','asterisk_viewTrunk'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * Populates array of allowed codecs for droplist object.
     * @param array $allow
     * @param boolean $disabled true if data in droplist have to disabled.
     * @return type
     */
    protected function populateAllow($allow, $disabled = false)
    {
        $codecs = explode(',', $allow);
        $allow = [];
        foreach ($codecs as $value) {
            if(!strcmp($value, 'alaw'))
            {
                $allow[$value] = ['content' => 'g711a', 'disabled' => $disabled];
            }
            else if(!strcmp($value, 'ulaw'))
            {
                $allow[$value] = ['content' => 'g711u', 'disabled' => $disabled];
            }
            else {
                $allow[$value] = ['content' => $value, 'disabled' => $disabled];
            }
        }
        return $allow;
    }
    /**
     * Sets parametr to reload pjsip module.
     */
    protected function setAsteriskReload()
    {
        $reload = ['pjsip' => 'pjsip reload'];
        if ($AsteriskReload = Yii::$app->cache->get('AsteriskReload')) {
            $AsteriskReload['pjsip'] = 'pjsip reload';
            $AsteriskReload = Yii::$app->cache->set('AsteriskReload', $AsteriskReload);
        } 
        else
        {
            $AsteriskReload = Yii::$app->cache->set('AsteriskReload', $reload);
        }
    }
    /**
     * Renders index page.
     * @return string
     */
    public function actionIndex()
    {
        $model = new TrunkForm();
        $list = new ArrayDataProvider([
            'allModels' => $model->listAll(),
            'key' => 'id',
            'sort' => [
                'attributes' => [
                    'id', 'username', 'status',
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $menu = new AsteriskSideMenu();
        return $this->render('index', [
            'list' => $list,
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Deletes trunk.
     * @return string
     */
    public function actionDelete()
    {   
        $model = new TrunkForm();
        $model->id = Yii::$app->request->get('id');
        $model->delete();
        $this->setAsteriskReload();
        return $this->redirect(Url::toRoute('asterisk/trunks/index'));
    }
    /**
     * Renders edit page and calls update function in model.
     * @return string
     */
    public function actionUpdate()
    {    
        $model = new TrunkForm(['scenario' => 'update']);
        $menu = new AsteriskSideMenu();
        $model->oldid = $model->id = Yii::$app->request->get('id');
        $model->initialize();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update()) {
                $this->setAsteriskReload();
                return $this->redirect(Url::toRoute('asterisk/trunks/index'));
            }
        }             
        return $this->render('update', [
            'model' => $model,
            'codecs' => $this->codecs,
            'allowed' => $this->populateAllow($model->allow),
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Renders create page and calls create function in model.
     * @return string
     */
    public function actionCreate()
    {        
        $model = new TrunkForm(['scenario' => 'create']);
        $menu = new AsteriskSideMenu();
        $model->port = '5060';
        $model->retryInterval = '60';
        if ($model->load(Yii::$app->request->post())) {
            if ($model->create()) {
                $this->setAsteriskReload();
                return $this->redirect(Url::toRoute('asterisk/trunks/index'));
            }
        }             
        return $this->render('create', [
            'model' => $model,            
            'codecs' => $this->codecs,
            'menu' => $menu->getMenu(),
        ]);
    }
    /**
     * Renders view page.
     * @return string
     */
    public function actionView()
    {
        $model = new TrunkForm(['scenario' => 'view']);
        $menu = new AsteriskSideMenu();
        $model->id = Yii::$app->request->get('id');
        $model->initialize();
        return $this->render('view', [
            'model' => $model,
            'allowed' => $this->populateAllow($model->allow, true),
            'menu' => $menu->getMenu(),
        ]);
    }
}