<?php

namespace app\controllers\general;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\GeneralSideMenu;
use app\models\PluginsForm;
use yii\web\UploadedFile;
use plugins;
use yii\helpers\BaseFileHelper;
use yii\data\ArrayDataProvider;
use yii\db\mysql\QueryBuilder;
use yii\db\Query;

class pluginsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['general_createPlugin'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['general_viewPlugin'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['general_deletePlugin'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {
        $menu = new GeneralSideMenu();
        $xml = new plugins();
        $names = $xml->getNames2();

        $provider = new ArrayDataProvider([
            'allModels' => $names,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('index', [
                    'menu' => $menu->getMenu(),
                    'provider' => $provider,
        ]);
    }

    public function actionCreate() {
        $menu = new GeneralSideMenu();
        $folder = new BaseFileHelper;
        $model = new PluginsForm();
        $xml = new plugins();
        $auth = Yii::$app->authManager;

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->module = UploadedFile::getInstances($model, 'module');
            if ($model->validate()) {
                $zip = new \ZipArchive();

                foreach ($model->module as $file) {
                    //creating folder, extracting///////////////////////////////
                    $folder->createDirectory('uploads/temp');
                    $file->saveAs('uploads/temp/' . $file->baseName . '.' . $file->extension);
                    if ($zip->open('../web/uploads/temp/' . $file->baseName . '.' . $file->extension) === TRUE) {
                        $zip->extractTo("../web/uploads/temp");
                        $zip->close();
                        $installFile = $folder->findFiles("../web/uploads/temp", ['only' => ['install.php']]);
                        //checking install.php//////////////////////////////////
                        if ($installFile == NULL) {
                            $folder->removeDirectory("uploads/temp");
                            throw new \yii\web\HttpException(400, 'File install.php is missing.');
                        }

                        include($installFile[0]);

                        $unzipFiles = $folder->findFiles("../web/uploads/temp/");
                        $pluginFiles = array();
                        $this->filesCheck($unzipFiles);
                        $i = 1;
                        $j = 1;
                        $k = 1;
                        //checking extracted files, copying dirs, files/////////
                        foreach ($unzipFiles as $unzipFile) {

                            if (strpos($unzipFile, "/controllers/") !== FALSE) {
                                $begin = strpos($unzipFile, "/controllers/");
                                $path = "../controllers/" . substr($unzipFile, $begin + 13, strlen($unzipFile));
                                if ($i == 1) {
                                    $folder->copyDirectory(substr($unzipFile, 0, $begin + 13), "../controllers/", $options = ['dirMode' => 0775, 'fileMode' => 0775]);
                                    $i++;
                                }
                                $pluginFiles[] = $path;
                                continue;
                            }

                            if (strpos($unzipFile, "/models/") !== FALSE) {
                                $begin = strpos($unzipFile, "/models/");
                                $path = "../models/" . substr($unzipFile, $begin + 8, strlen($unzipFile));
                                if ($j == 1) {
                                    $folder->copyDirectory(substr($unzipFile, 0, $begin + 8), "../models/", $options = ['dirMode' => 0775, 'fileMode' => 0775]);
                                    $j++;
                                }
                                $pluginFiles[] = $path;
                                $path = "";
                                continue;
                            }
                            if (strpos($unzipFile, "/views/") !== FALSE) {
                                $begin = strpos($unzipFile, "/views/");
                                $path = "../views/" . substr($unzipFile, $begin + 7, strlen($unzipFile));
                                if ($k == 1) {
                                    $folder->copyDirectory(substr($unzipFile, 0, $begin + 7), "../views/", $options = ['dirMode' => 0775, 'fileMode' => 0775]);
                                    $k++;
                                }
                                $pluginFiles[] = $path;
                                $path = "";
                                continue;
                            }
                        }
                        //checking existence of controller files////////////////
                        if ($i == 1) {
                            $this->eraseEvidence($pluginFiles);
                            $folder->removeDirectory("uploads/temp");
                            throw new \yii\web\HttpException(400, 'Some files are missing.');
                        }
                        //creating tables in db/////////////////////////////////
                        if (isset($database)) {
                            $xmlDb = array();
                            foreach ($database as $dB) {
                                foreach ($dB as $dbname => $table) {
                                    switch ($dbname) {
                                        case 'asterisk': {
                                                $builder = new QueryBuilder(Yii::$app->dbAsterisk);
                                                //checking if table exists//////////////                           
                                                $rows = Yii::$app->dbAsterisk->getTableSchema($table['name']);
                                                if ($rows != NULL) {
                                                    $this->eraseEvidence($pluginFiles);
                                                    $folder->removeDirectory("uploads/temp");
                                                    if ($xmlDb != null) {
                                                        $this->deleteDatabase($xmlDb);
                                                    }
                                                    throw new \yii\web\HttpException(400, 'Table in database allready exists.');
                                                }
                                                //creating table and columns////////////
                                                $cmd = Yii::$app->dbAsterisk->createCommand($builder->createTable($table['name'], $table['columns']));
                                                $cmd->execute();
                                                $xmlDb[] = ['database' => "asterisk",
                                                    'table' => $table['name']
                                                ];
                                                break;
                                            }
                                        case 'webinterface': {
                                                $builder = new QueryBuilder(Yii::$app->db);
                                                //checking if table exists//////////////
                                                $rows = Yii::$app->db->getTableSchema($table['name']);
                                                if ($rows != NULL) {
                                                    $this->eraseEvidence($pluginFiles);
                                                    $folder->removeDirectory("uploads/temp");
                                                    if ($xmlDb != null) {
                                                        $this->deleteDatabase($xmlDb);
                                                    }
                                                    throw new \yii\web\HttpException(400, 'Table in database allready exists.');
                                                }
                                                //creating table and columns////////////
                                                $cmd = Yii::$app->db->createCommand($builder->createTable($table['name'], $table['columns']));
                                                $cmd->execute();
                                                $xmlDb[] = ['database' => "webinterface",
                                                    'table' => $table['name']
                                                ];
                                                break;
                                            }
                                        case 'mikrotik': {
                                                $builder = new QueryBuilder(Yii::$app->dbMikrotik);
                                                //checking if table exists//////////////
                                                $rows = Yii::$app->dbMikrotik->getTableSchema($table['name']);
                                                if ($rows != NULL) {
                                                    $this->eraseEvidence($pluginFiles);
                                                    $folder->removeDirectory("uploads/temp");
                                                    if ($xmlDb != null) {
                                                        $this->deleteDatabase($xmlDb);
                                                    }
                                                    throw new \yii\web\HttpException(400, 'Table in database allready exists.');
                                                }
                                                //creating table and columns////////////
                                                $cmd = Yii::$app->db->createCommand($builder->createTable($table['name'], $table['columns']));
                                                $cmd->execute();
                                                $xmlDb[] = ['database' => "mikrotik",
                                                    'table' => $table['name']
                                                ];
                                                break;
                                            }
                                    }
                                }
                            }
                        } else {
                            $xmlDb = null;
                        }
                        //creating permissions//////////////////////////////////
                        $permCheck = $auth->getPermissions();
                        foreach ($install as $permissionName => $descriptions) {
                            $permissionsString[] = $plugin . "_" . $permissionName;
                            foreach ($permCheck as $value) {
                                if ($value->name == $plugin . "_" . $permissionName) {
                                    if ($xmlDb != null) {
                                        $this->deleteDatabase($xmlDb);
                                    }
                                    $this->eraseEvidence($pluginFiles);
                                    $folder->removeDirectory("uploads/temp");
                                    if (isset($permissionsString)) {
                                        $this->deletePermissions($permissionsString);
                                    }
                                    throw new \yii\web\HttpException(400, 'Permission allready exists.');
                                }
                            }

                            $createPost = $auth->createPermission($plugin . "_" . $permissionName);
                            $createPost->description = $descriptions;
                            $createPost->data = $plugin;
                            $auth->add($createPost);
                        }
                    }
                }
                $folder->removeDirectory("uploads/temp");
            } else {
                $model->getErrors();
            }
            $description = mb_convert_encoding($description, "UTF-8", "ASCII,JIS,UTF-8,EUC-JP,SJIS");
            $error = @$xml->create($plugin, $sideMenu, $pluginName, $description, $pluginFiles, $install, $xmlDb);
            if ($error != 'OK') {
                if ($xmlDb != null) {
                    $this->deleteDatabase($xmlDb);
                }
                $this->eraseEvidence($pluginFiles);
                if (isset($permissionsString)) {
                    $this->deletePermissions($permissionsString);
                }
                if ($error == -1) {
                    throw new \yii\web\HttpException(400, 'XML file: permission denied.');
                }
                if ($error == 0) {
                    throw new \yii\web\HttpException(400, 'XML file does not exists.');
                }
                throw new \yii\web\HttpException(400, 'Error in XML file.');
            }
            return $this->redirect(Url::toRoute('general/plugins/index'));
        }
        return $this->render('create', [
                    'menu' => $menu->getMenu(),
                    'model' => $model,
        ]);
    }

    public function actionDelete() {
        $id = Yii::$app->request->get('id');
        $xml = new plugins();
        $paths = $xml->getFilePaths($id);
        $dbInfo = $xml->getDbInfo($id);
        $perm = $xml->getPermissionsByName($id);

        $this->eraseEvidence($paths);
        $xml->delete($id);
        $this->deletePermissions($perm);
        if ($dbInfo != NULL) {
            $this->deleteDatabase($dbInfo);
        }

        return $this->redirect(Url::toRoute('general/plugins/index'));
    }

    public function actionView() {
        $menu = new GeneralSideMenu();
        $xml = new plugins();
        $name = Yii::$app->request->get('id');
        $description = $xml->getDescription($name);
        return $this->render('view', [
                    'menu' => $menu->getMenu(),
                    'name' => $name,
                    'description' => $description,
        ]);
    }

    public function eraseEvidence($evidence) {
        if ($evidence == NULL) {
            return;
        }
        $fileHelper = new BaseFileHelper;
        foreach ($evidence as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
        foreach ($evidence as $file) {
            $folder = explode("/", $file);
            for ($i = (count($folder) - 1); $i > 1; $i--) {
                $string = "../";
                for ($x = 1; $x < $i; $x++) {
                    $string.= $folder[$x] . "/";
                }
                if (count(glob($string . "*")) == 0) {
                    if (file_exists($string) && is_dir($string)) {
                        $fileHelper->removeDirectory($string);
                    }
                }
            }
        }
        return;
    }

    public function deleteDatabase($dbInfo) {
        foreach ($dbInfo as $db) {
            switch ($db['database']) {
                case 'asterisk':
                    $builder = new QueryBuilder(Yii::$app->dbAsterisk);
                    $cmd = Yii::$app->dbAsterisk->createCommand($builder->dropTable($db['table']));
                    $cmd->execute();
                    break;
                case 'webinterface':
                    $builder = new QueryBuilder(Yii::$app->db);
                    $cmd = Yii::$app->db->createCommand($builder->dropTable($db['table']));
                    $cmd->execute();
                    break;
                case 'mikrotik':
                    $builder = new QueryBuilder(Yii::$app->dbMikrotik);
                    $cmd = Yii::$app->dbMikrotik->createCommand($builder->dropTable($db['table']));
                    $cmd->execute();
                    break;
            }
        }
        return;
    }

    public function deletePermissions($permissions) {
        $auth = Yii::$app->authManager;
        $delete = $auth->getPermissions();

        foreach ($delete as $del) {
            foreach ($permissions as $value) {
                if ($del->name == $value) {
                    $auth->remove($del);
                }
            }
        }
        return;
    }

    public function filesCheck($files) {
        $folder = new BaseFileHelper;
        foreach ($files as $unzipFile) {
            if (strpos($unzipFile, "/controllers/") !== FALSE) {
                $begin = strpos($unzipFile, "/controllers/");
                $path = "../controllers/" . substr($unzipFile, $begin + 13, strlen($unzipFile));
                if (file_exists($path)) {
                    $folder->removeDirectory("uploads/temp");
                    throw new \yii\web\HttpException(400, 'File: ' . substr($unzipFile, $begin + 13, strlen($unzipFile)) . ' already exists.');
                }
                continue;
            }

            if (strpos($unzipFile, "/models/") !== FALSE) {
                $begin = strpos($unzipFile, "/models/");
                $path = "../models/" . substr($unzipFile, $begin + 8, strlen($unzipFile));
                if (file_exists($path)) {
                    $folder->removeDirectory("uploads/temp");
                    throw new \yii\web\HttpException(400, 'File: ' . substr($unzipFile, $begin + 8, strlen($unzipFile)) . ' already exists.');
                }
                continue;
            }
            if (strpos($unzipFile, "/views/") !== FALSE) {
                $begin = strpos($unzipFile, "/views/");
                $path = "../views/" . substr($unzipFile, $begin + 7, strlen($unzipFile));
                if (file_exists($path)) {
                    $folder->removeDirectory("uploads/temp");
                    throw new \yii\web\HttpException(400, 'File: ' . substr($unzipFile, $begin + 7, strlen($unzipFile)) . ' already exists.');
                }
                continue;
            }
        }
    }

}
