<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\controllers\general;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use app\models\GeneralSideMenu;
use app\models\RoleForm;

class RolesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['general_deleteRole'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['general_updateRole'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['general_createRole'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['general_viewRole'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['general_deleteRole','general_updateRole',
                            'general_createRole','general_viewRole'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['admin'],
                    ], 
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    protected function populatePermissionsArray($name = null)
    {
        $query = (new Query)->from('auth_item')->where(['type' => 2]);
        $auth = Yii::$app->authManager;
        $currentPerms = $auth->getPermissionsByRole($name);
        $permissions = [];
        foreach ($query->each() as $row) {
            $permissions[unserialize($row['data'])][$row['name']] = [
                'name' => str_replace(unserialize($row['data']) . '_', '', $row['name']),
                'description' => $row['description'],
                'created_at' => $row['created_at'],
                'checked' => isset($currentPerms[$row['name']]) ? true : false,
            ];
        }
        return $permissions;
    }
    public function populateDataProviders($params)
    {
        $plugins = ['asterisk', 'general', 'mikrotik'];
        $searchModel = new RoleSearch();
        $out = [];
        foreach ($plugins as $plugin) {
            $searchModel->plugin = $plugin;
            $out[$plugin] = [
                'searchModel' => $searchModel,
                'dataProvider' => $searchModel->search($params),        
            ];
        }        
        return $out;
    }

    public function actionIndex()
    {
        $model = new RoleForm();
        $list = new ArrayDataProvider([
            'allModels' => $model->listAll(),
            'key' => 'name',
            'sort' => [
                'attributes' => [
                    'name',
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $menu = new GeneralSideMenu();
        return $this->render('index', [
            'list' => $list,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionDelete()
    {   
        $auth = Yii::$app->authManager;
        $name = Yii::$app->request->get('id');
        if(strcmp($name, 'admin') && $auth->getRole($name))
        {
            $auth->remove($role);
        }        
        return $this->redirect(Url::toRoute('general/roles/index'));
    }
    public function actionUpdate()
    {        
        $model = new RoleForm(['scenario' => 'update']);
        $menu = new GeneralSideMenu();
        $name = Yii::$app->request->get('id');
        if(!strcmp($name, 'admin'))
        {
            return $this->redirect(Url::toRoute('general/roles/index'));
        }
        $model->oldname = $model->name = $name;
        $query = (new Query)->from('auth_item')->where(['name' => $name])->select('description');
        $model->description = implode("", $query->one());
        $permissions = $this->populatePermissionsArray($name);        
        if ($model->load(Yii::$app->request->post())) {
            $model->items = Yii::$app->request->post('permissions');
            if ($role = $model->update()) {                
                return $this->redirect(Url::toRoute('general/roles/index'));
            }
        }
        return $this->render('update', [
            'permissions' => $permissions,                
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionCreate()
    {        
        $model = new RoleForm(['scenario' => 'create']);
        $permissions = $this->populatePermissionsArray();
        $menu = new GeneralSideMenu();
        if ($model->load(Yii::$app->request->post())) {
            $model->items = Yii::$app->request->post('permissions');
            if ($role = $model->create()) {                
                return $this->redirect(Url::toRoute('general/roles/index'));
            }
        }
        return $this->render('create', [
            'permissions' => $permissions, 
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
    public function actionView()
    {
        $menu = new GeneralSideMenu();
        $model = new RoleForm(['scenario' => 'view']);
        $name = Yii::$app->request->get('id');
        $model->oldname = $model->name = $name;
        $permissions = $this->populatePermissionsArray($name);
        return $this->render('view', [
            'permissions' => $permissions,
            'model' => $model,
            'menu' => $menu->getMenu(),
        ]);
    }
}