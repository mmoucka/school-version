<?php

namespace app\models;

use Yii;
use yii\base\Model;
use Plugins;
/**
 * ContactForm is the model behind the contact form.
 */
class AddPluginForm extends Model {

    public $name;
    public $path;
    public $mainFile;
    

    public function rules() {
        return [
            [['name', 'path', 'mainFile'], 'required'],

        ];
    }
    
    public function create(){
            if ($this->validate()) {
            $plugin = new Plugins();
            $content = [$this->name, $this->mainFile, $this->path];
            $plugin->create($content);
            return;
        }

        return null;
    }

}
