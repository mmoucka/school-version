<?php

namespace app\models;

use app\models\User;
use yii\widgets\Menu;
use Yii;
use plugins;

//include_once '../vendor/plugins/plugins.php';

class GeneralSideMenu {

    private $menu;
    private $permissionsArray = [
        'users' =>
        [
            'general_deleteUser',
            'general_createUser',
            'general_updateUser',
            'general_viewUser'
        ],
        'roles' => [
            'general_deleteRole',
            'general_updateRole',
            'general_createRole',
            'general_viewRole'
        ],
    ];
    public function __construct() {
        // L. volani funkce pro nacitani pluginu do menu z XML 
        $items = $this->names();
        //var_dump($items);
        // L. staticke polozky do menu nutno zapisovat timto zpusobem
        // todo očetření ověřování práv k zobrazení -- DONE
        $menu[] = ['label' => 'Users', 'url' => ['general/users/index'],'visible' => $this->visibleCheck($this->permissionsArray['users'])];
        $menu[] = ['label' => 'Roles', 'url' => ['general/roles/index'],'visible' => $this->visibleCheck($this->permissionsArray['roles'])];
        $menu[] = ['label' => 'Plugins', 'url' => ['general/plugins/index']];
        if ($items != null) {
            foreach ($items as $value) {
                $menu[] = $value;
            }
        }
        $this->menu = [
            'linkTemplate' => '<a class="list-group-item" href="{url}">{label}</a>',
            'options' => ['class' => 'nav nav-pills nav-stacked'],
            'itemOptions' => ['role' => 'presentation'],
        ];
        $this->menu['items'] = $menu;

        //['label' => 'Only for logged', 'url' => ['site/login'], 'visible' => !Yii::$app->user->isGuest],
    }

    public function getMenu() {

        return $this->menu;
    }

    // L. generovani odkazu na pluginy do postranniho menu
    public function names() {
        $module = "general";
        $xml = new plugins();
//        $names = $xml->getNames($module);
//        $paths = $xml->getPaths($module);
//        $mainFiles = $xml->getMainPages($module);
//        $permissions = $xml->getPermissions($module);
        $string = $xml->getString($module);


        $menuStrings = array();
        for ($index = 0; $index < count($string); $index++) {
            $menuString = ['label' => $string[$index]['label'], 'url' => [$string[$index]['url']], 'visible' => $this->visibleCheck($string[$index]['permissions'])];
            $menuStrings[$index] = $menuString;
        }
        return $menuStrings;
        //return $xml->getMenuNames($names, $paths, $mainFiles);
    }

    public function visibleCheck($array) {
        //edit by Martin
        //returns always true if user is admin => can see all items
        if(isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin']))
        {
            return true;
        }
        $auth = Yii::$app->authManager;
        $perm = $auth->getPermissionsByUser(Yii::$app->user->getId());
        foreach ($perm as $value) {
            foreach ($array as $perm) {
                if ($perm == $value->name) {
                    return TRUE;
                }
            }
        }
        return false;
    }

}
?> 
