<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class PluginsForm extends Model {

    /**
     * @var UploadedFile|Null file attribute
     */
    public $module;



    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
           
            ['module', 'required'],    
            [['module'], 'file', 'maxFiles' => 10, 'skipOnEmpty' => false],

        ];
    }

       public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['name'];
        $scenarios['create'] = [];
        $scenarios['view'] = [];
        return $scenarios;
    }
    public function actionCreate() {
        
    }

}
