<?php 
namespace app\models;

use yii\db\Query;
use yii\base\Model;
use Yii;
/**
 * Model for manipulation with Roles
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */
class RoleForm extends Model
{
    /**
     * 
     * @var string Used for validation if role's name is changed during update.
     */
    public $oldname;
    /**
     *
     * @var string Role's name.
     */
    public $name;
    /**
     *
     * @var string Role's description.
     */
    public $description;
    /**
     *
     * @var array Permissions assigned to a roles.
     */
    public $items = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],            
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['name', 'unique', 'targetClass' => 'app\models\Role', 'message' => 'Role with this name already exists', 'when' => function()
            {
                return $this->oldname != $this->name;
            }],
            ['name', 'filter', 'filter' => 'trim'],
            ['description', 'string', 'min' => 0, 'max' => 255],
        ];
        return $rules;
    }
    public function listAll()
    {
        $query = new Query();
        $query->from('auth_item');
        $query->andWhere(['!=', 'name', 'admin'])->
        andWhere(['type' => 1]);
        return $query->all();
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['name', 'description'];
        $scenarios['create'] = ['name', 'description'];
        $scenarios['view'] = [];
        return $scenarios;
    }
    public function create()
    {
        if (strcmp($this->name, 'admin') && $this->validate()) {
            $auth = Yii::$app->authManager;
            $role = $auth->createRole($this->name);            
            $role->description = $this->description;
            $auth->update($this->name, $role);
            $auth->add($role);
            if($this->items)
            {
                foreach ($this->items as $permission) {
                    $item = $auth->getPermission($permission);
                    $auth->addChild($role, $item);
                }
            }
            return $role;
        }
        return null;
    }
    public function update()
    {
        if (strcmp($this->name, 'admin') && $this->validate()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($this->oldname);
            $role->name = $this->name;
            $role->description = $this->description;
            $auth->removeChildren($role);
            if($this->items)
            {
                foreach ($this->items as $permission) {
                    $item = $auth->getPermission($permission);
                    $auth->addChild($role, $item);
                }
            }
            $auth->update($this->oldname, $role);
            return $role;
        }

        return null;
    }
}
