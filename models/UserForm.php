<?php 
namespace app\models;

use app\models\User;
use yii\base\Model;
use Yii;

/**
 * 
 */
class UserForm extends Model
{
    public $oldusername;
    public $oldemail;
    public $username;
    public $email;
    public $role;
    public $password;
    public $password_repeat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],            
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This username has already been taken.', 'when' => function()
            {
                return $this->oldusername != $this->username;
            }],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This email address has already been taken.', 'when' =>  function()
            {
                return $this->oldemail != $this->email;
            }],
            ['role', 'filter', 'filter' => 'trim'],
            ['password', 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6],
            
            ['password_repeat', 'compare', 'compareAttribute'=>'password'],
        ];
        return $rules;
    }
    /**     *
     * @return User|null the saved model or null if saving fails
     */
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['username', 'email', 'password', 'role'];
        $scenarios['create'] = ['username', 'email', 'password', 'role'];
        $scenarios['view'] = [];
        return $scenarios;
    }
    public function create()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($this->role);
            $auth->assign($role, $user->getId());
            return $user;
        }

        return null;
    }
    public function update()
    {
        if ($this->validate()) {
            $user = User::findOne(['username' => $this->oldusername]);            
            $user->username = $this->username;
            $user->email = $this->email;
            if($this->password)
            {
                $user->setPassword($this->password);
            }            
            $user->update();
            if($user->getId() != 1)
            {
                $auth = Yii::$app->authManager;
                $auth->revokeAll($user->getId());
                $role = $auth->getRole($this->role);
                $auth->assign($role, $user->getId());
            }            
            return $user;
        }
        return null;
    }
}
