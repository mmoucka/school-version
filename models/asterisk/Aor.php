<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * ActiveRecord for manipulation with Aor in Asterisk DB
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class Aor extends ActiveRecord
{
    public static function tableName() 
    {
        return 'ps_aors';
    }
    public static function getDb()
    {
        return Yii::$app->dbAsterisk;
    }
    /**
     * Returns all contacts of endpoint from DB. Sperator is ",\n"
     * @param string $id
     * @return string
     */
    public function getContacts($id = null)
    {
        $contacts = [];
        $query = new Query();
        if($id)
            $query->from('ps_contacts')->select(['uri'])->where(['like', 'id', $id.'^3B@']);
        else
            $query->from('ps_contacts')->select(['uri'])->where(['like', 'id', $this->id.'^3B@']); 
        $contactsRaw = $query->all(Yii::$app->dbAsterisk);
        foreach ($contactsRaw as $row) {
            array_push($contacts, $row['uri']);
        }
        return implode(",\n", $contacts);
    }
    /**
     * Returns all user agents of endpoint from DB. Sperator is ",\n"
     * @param string $id
     * @return string
     */
    public function getUAs($id = null)
    {
        $uas = [];
        $query = new Query();
        if($id)
            $query->from('ps_contacts')->select(['user_agent'])->where(['like', 'id', $id.'^3B@']);
        else
            $query->from('ps_contacts')->select(['user_agent'])->where(['like', 'id', $this->id.'^3B@']);  
        $uasRaw = $query->all(Yii::$app->dbAsterisk);
        foreach ($uasRaw as $row) {
            array_push($uas, $row['user_agent']);
        }
        return implode(",\n", $uas);
    }
}
