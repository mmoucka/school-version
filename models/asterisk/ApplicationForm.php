<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\base\Model;
use yii\db\Query;
use app\models\asterisk\ApplicationLine;

use Yii;

/**
 * Model which validates and manipulates with application (IVR and Special app)
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class ApplicationForm extends Model
{
    /**
     * @var string Name of the application.
     */
    public $appname;
    /**
     * @var string Used for validation if appname is changed during update.
     */
    public $oldappname;
    /**
     * @var string Name of/Path to file which is played at start of IVR.
     */
    public $intro;
    /**
     *
     * @var string Description of the application. 
     */
    public $description;
    /**
     *
     * @var array Determining dialplan application and its options which will be called as special application.
     */
    public $data = [];
    /**
     *
     * @var array List of keys allowed to use in IVR.
     */
    public $keys = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    /**
     *
     * @var arry List of dialplan applications used in autocomplete when updating or creating special application.
     */
    public $apps = ['Playback', 'Dial'];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['appname', 'filter', 'filter' => 'trim'],
            ['appname', 'required'],
            ['appname', 'unique', 'targetClass' => 'app\models\asterisk\ApplicationLine', 'message' => 'This name has already been taken.', 'when' =>  function()
            {
                return $this->appname != $this->oldappname;
            }],
            ['description', 'string'],
            ['data', 'required'],
            ['intro', 'required'], 
        ];
        return $rules;
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['updateivr'] = ['appname', 'data', 'intro', 'description'];
        $scenarios['createivr'] = ['appname', 'data', 'intro', 'description'];
        $scenarios['updatespecial'] = ['appname', 'data', 'description'];
        $scenarios['createspecial'] = ['appname', 'data', 'description'];
        $scenarios['viewivr'] = [];
        $scenarios['viewspecial'] = [];
        return $scenarios;
    }
    /**
     * Return list of application set in database.
     * @param integer $type 1 - IVR, 2 - Special Application
     * @return array
     */
    public function listAll($type)
    {
        $query = new Query();
        $query->from('applications')->orderBy('appname')->where(['type' => $type]);
        $list = $query->all(Yii::$app->dbAsterisk);
        $listFormated = [];
        if(is_array($list))
        {
            $name = '';
            $key = -1;
            foreach ($list as $value) {
                if(strcmp($name, $value['appname']))
                {
                    $key++;
                    $name = $value['appname'];
                    $listFormated[$key]['appname'] = $name;
                    $listFormated[$key]['description'] = $value['description'];
                }
            }
            return $listFormated;  
        }                      
        return [];
    }    
    /**
     * Returns list of options available in IVR.
     * @return array
     */
    public function listIvr()
    {
        $query = new Query();
        $query->from('applications')->where(['appname' => $this->appname]);
        $list = $query->all(Yii::$app->dbAsterisk);
        $listFormated;
        if(is_array($list))
        {
            foreach ($list as $value) {
                if (!strcmp($value['action'], 'intro'))
                {
                    $listFormated['intro'] = $value['data'];
                    $this->description = $value['description'];
                }                    
                else
                {
                    $key = explode(',', $value['action'])[1];
                    switch (explode(',', $value['data'])[0]) {
                        case 'play':
                            $listFormated[$key]['action'] = 'play';
                            $listFormated[$key]['data'] = [$value['data'] => basename($value['data'])];
                            break;
                        case 'extension':
                            $listFormated[$key]['action'] = 'dial';
                            $listFormated[$key]['data'] = [$value['data'] => explode(',', $value['data'])[0]];
                            break;
                        case 'app':
                            $listFormated[$key]['action'] = 'goto';
                            $listFormated[$key]['data'] = [ $value['data'] => explode(',', $value['data'])[0]];
                            break;
                        case 'queue':
                            $listFormated[$key]['action'] = 'goto';
                            $listFormated[$key]['data'] = [$value['data'] => explode(',', $value['data'])[0]];
                            break;
                        case 'execute':
                            $listFormated[$key]['action'] = 'goto';
                            $listFormated[$key]['data'] = [$value['data'] => explode(',', $value['data'])[0]];
                            break;
                        default:
                            $listFormated[$key]['action'] = '';
                            $listFormated[$key]['data'] = [];
                            break;
                    }
                }                   
            }
            return $listFormated;  
        }                      
        return [];
    } 
    /**
     * Initializes special application from database.
     */
    public function initSpecial()
    {
        $app = ApplicationLine::findOne(['appname' => $this->appname, 'action' => 'special']);
        $this->description = $app->description;
        $this->data['options'] = explode('\\', $app->data)[1];
        $this->data['app'] = explode(',', $app->data)[1];        
    }
    /**
     * Returns type of specified Application
     * @param string $appname 1 - IVR, 2 - Special
     * @return integer
     */
    public function getType($appname)
    {
        $query = new Query();
        $query->from('applications')->where(['type' => $type, ['appname' => $this->appname]]);
        $result = $query->one(Yii::$app->dbAsterisk);
        return $result['type'];
    }
    /**
     * If inserted values are valid, creates new special application.
     * @return boolean
     */
    public function createspecial()
    {
        if ($this->validate()) {
            $app = new ApplicationLine();
            $app->appname = $this->appname;
            $app->action = 'special';
            $app->data = 'execute,'.$this->data['app'].',\\'.$this->data['options'];
            $app->description = $this->description;
            $app->type = 2;
            $app->save();
            return true;
        }
        return null;
    }
    /**
     * If inserted values are valid, udpates new special application.
     * @return boolean
     */
    public function updatespecial()
    {
        if ($this->validate()) {
            $app = ApplicationLine::findOne(['appname' => $this->oldappname, 'action' => 'special']);
            $app->appname = $this->appname;
            $app->data = 'execute,'.$this->data['app'].','.$this->data['options'];
            $app->description = $this->description;
            $app->type = 2;
            $app->save();
            return true;
        }
        return null;
    }
    /**
     * If inserted values are valid, creates new IVR.
     * @return boolean
     */
    public function createivr()
    {
        if ($this->validate()) {
            $app = new ApplicationLine();
            $app->appname = $this->appname;
            $app->action = 'intro';
            $app->data = $this->intro;
            $app->description = $this->description;
            $app->type = 1;
            $app->save();
            foreach ($this->data as $key => $value) {
                if($value)
                {
                    $app = new ApplicationLine();
                    $app->appname = $this->appname;
                    $app->description = $this->description;
                    $app->type = 1;
                    $app->action = 'waitfordigit,' . $key;
                    $app->data = $value;
                    $app->save();
                }                
            }
            return true;
        }
        return null;
    }
    /**
     * If inserted values are valid, edits new IVR.
     * @return boolean
     */
    public function updateivr()
    {
        if ($this->validate()) {
            $app = ApplicationLine::findOne(['appname' => $this->oldappname, 'action' => 'intro']);
            $app->appname = $this->appname;
            $app->data = $this->intro;
            $app->description = $this->description;
            $app->type = 1;
            $app->save();
            foreach ($this->data as $key => $value) {
                if($app = ApplicationLine::findOne(['appname' => $this->oldappname, 'action' => 'waitfordigit,'.$key]))
                {
                    $app->appname = $this->appname;
                    $app->description = $this->description;
                    $app->type = 1;
                    $app->data = $value;
                    $app->save();
                }
                else
                {
                    $app = new ApplicationLine();
                    $app->appname = $this->appname;
                    $app->description = $this->description;
                    $app->type = 1;
                    $app->action = 'waitfordigit,' . $key;
                    $app->data = $value;
                    $app->save();
                }
                
            }
            return true;
        }
        return null;
    }
    /**
     * Deletes application regarding @var $appname
     */
    public function delete()
    {
        $app = new ApplicationLine();
        $app->deleteAll(['appname' => $this->appname]);
    }
}
