<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use Yii;
use plugins;


class AsteriskSideMenu {

    private $menu;
    private $permissionsArray = [
        'extensions' =>
        [
            'asterisk_deleteExtension',
            'asterisk_updateExtension',
            'asterisk_createExtension',
            'asterisk_viewExtension'
        ],
        'trunks' => [
            'asterisk_deleteTrunk',
            'asterisk_updateTrunk',
            'asterisk_createTrunk',
            'asterisk_viewTrunk'
        ],
        'routes' =>
        [
            'asterisk_deleteRoute',
            'asterisk_updateRoute',
            'asterisk_createRoute',
            'asterisk_viewRoute',
            'asterisk_sortRoute'
        ],
        'emergencynums' => [
            'asterisk_deleteRoute',
            'asterisk_updateRoute',
            'asterisk_createRoute',
            'asterisk_viewRoute',
            'asterisk_sortRoute'
        ],
        'callshistory' =>
        [
            'asterisk_callHistory'
        ],
    ];
    
    public function __construct() {
        $items = $this->names();
        $menu[] = ['label' => 'Apply', 'url' => ['asterisk/reload'], 'template' => '<a class="list-group-item list-group-item-danger" href="{url}">{label}</a>',
            'visible' => Yii::$app->cache->exists('AsteriskReload') ? true : false];
        $menu[] = ['label' => 'Dashboard', 'url' => ['asterisk/index'], 'visible' => function ($rule, $action) {
                            $permissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->getId());
                            return preg_match('"asterisk"', implode(',', array_keys($permissions)));
                        }];
        $menu[] = ['label' => 'Extensions', 'url' => ['asterisk/extensions/index'], 'visible' => $this->visibleCheck($this->permissionsArray['extensions'])];
        $menu[] = ['label' => 'Trunks', 'url' => ['asterisk/trunks/index'], 'visible' => $this->visibleCheck($this->permissionsArray['trunks'])];
        $menu[] = ['label' => 'Routes', 'url' => ['asterisk/routes/index'], 'visible' => $this->visibleCheck($this->permissionsArray['routes'])];
        $menu[] = ['label' => 'Emergency Numbers', 'url' => ['asterisk/emergencynums/index'], 'visible' => $this->visibleCheck($this->permissionsArray['emergencynums'])];
        $menu[] = ['label' => 'Calls history', 'url' => ['asterisk/cdrs/index'], 'visible' => $this->visibleCheck($this->permissionsArray['callshistory'])];
        if ($items != null) {
            foreach ($items as $value) {
                $menu[] = $value;
            }
        }        
        $this->menu = [
            'linkTemplate' => '<a class="list-group-item" href="{url}">{label}</a>',
            'options' => ['class' => 'nav nav-pills nav-stacked'],
            'itemOptions' => ['role' => 'presentation'],
        ];
        $this->menu['items'] = $menu;
    }
    
    public function getMenu() {

        return $this->menu;
    }
    public function names() {
        $module = 'asterisk';
        $xml = new plugins();
        $string = $xml->getString($module);

        $menuStrings = array();
        for ($index = 0; $index < count($string); $index++) {
            $menuString = ['label' => $string[$index]['label'], 'url' => [$string[$index]['url']], 'visible' => $this->visibleCheck($string[$index]['permissions'])];
            $menuStrings[$index] = $menuString;
        }
        return $menuStrings;
    }

    public function visibleCheck($array) {
        if(isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId())['admin']))
        {
            return true;
        }
        $auth = Yii::$app->authManager;
        $perm = $auth->getPermissionsByUser(Yii::$app->user->getId());
        foreach ($perm as $value) {
            foreach ($array as $perm) {
                if ($perm == $value->name) {
                    return TRUE;
                }
            }
        }
        return false;
    }

}
?> 
