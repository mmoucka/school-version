<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * ActiveRecord used to manipulate with Auth in Asterisk DB.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class Auth extends ActiveRecord
{
    public static function tableName() 
    {
        return 'ps_auths';
    }
    public static function getDb()
    {
        return Yii::$app->dbAsterisk;
    }
    /**
     * Returns Username used during registration of endpoint.
     * @param string $id
     * @return string
     */
    public function getUsername($id = null)
    {
        if($id)
        {
            $query = new Query();
            $query->from('ps_auths')->select(['username'])->where(['id' => $id]);
            $result = $query->one(Yii::$app->dbAsterisk);
            return $result['username'];
        }        
        return $this->username;
    }
}
