<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;
use yii\base\Model;
use app\models\asterisk\Cdr;
use yii\data\ActiveDataProvider;

/**
 * Model used to search in Cdr table in Asterisk DB.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class CdrSearch extends Model {
    /**
     *
     * @var string From - To is searched. Format is "Y-m-d G:i - Y-m-d G:i".
     */
    public $dateRange;
    /**
     *
     * @var string Source of searched calls.
     */
    public $from;
    /**
     *
     * @var string Destination of searched calls.
     */
    public $to;
    /**
     *
     * @var integer Duration of searched calls.
     */
    public $duration;
    /**
     *
     * @var string State of searched calls.
     */
    public $state;
    /**
     *
     * @var string Operator between from and to fields.
     */
    public $fromToOperator;
    /**
     *
     * @var string Says if model is looking for greater, lower or equal duration of a call.
     */
    public $durationOperator;
    /**
     *
     * @var string Searches by a note.
     */
    public $note;
    
    public function rules()
    {
        $rules = [
            ['from', 'string', 'max' => 255],
            ['to', 'string', 'max' => 255],
            ['note', 'string', 'max' => 255],
            ['duration', 'integer', 'max' => 3600],
        ];
        return $rules;
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['search'] = ['dateRange', 'from', 'to', 'duration', 'state', 'durationOperator', 'fromToOperator', 'note'];
        return $scenarios;
    }
    /**
     * Returns list of all calls in history.
     * @return ActiveDataProvider
     */
    public function listAll()
    {
        $query = Cdr::find();
        $data = new ActiveDataProvider(['query' => $query, 'key' => 'id']);
        return $data;
    }
    /**
     * Search in call history by set parameters.
     * @param string $auth Allows to mitigate searching only on one auth.
     * @return ActiveDataProvider
     */
    public function search($auth = null)
    {
        if ($this->validate()) {
            $query = Cdr::find();
            
            $data = new ActiveDataProvider(['query' => $query, 'key' => 'id']);
            if($this->to && $this->from)
            {
                switch(strtolower($this->fromToOperator))
                {
                    case 'or':
                          $query->andFilterWhere(['or', ['like', 'dst', $this->to], ['like', 'dstchannel', $this->to], ['like', 'userfield', $this->to], ['like', 'src', $this->from]]);
                        break;                    
                    case 'and':  
                        $query->andFilterWhere(['like', 'src', $this->from]);
                        $query->andFilterWhere(['or', ['like', 'dst', $this->to], ['like', 'dstchannel', $this->to], ['like', 'userfield', $this->to]]);
                        break;
                }
            }
            else if($this->to)
            {
                $query->andFilterWhere(['or', ['like', 'dst', $this->to], ['like', 'dstchannel', $this->to], ['like', 'userfield', $this->to]]);
            }
            else if($this->from)
            {
                $query->andFilterWhere(['like', 'src', $this->from]);
            }
            if($auth)
            {
                $query->andFilterWhere(['or', ['like', 'dst', $auth], ['like', 'dstchannel', $auth], ['like', 'userfield', $auth], ['like', 'src', $auth]]);
            }
            if($this->duration)
            {
                switch ($this->durationOperator) {
                    case 'equal':
                        $query->andFilterWhere(['=', 'duration', $this->duration]);
                        break;
                    case 'greater':
                        $query->andFilterWhere(['>', 'duration', $this->duration]);
                        break;
                    case 'lower':
                        $query->andFilterWhere(['<', 'duration', $this->duration]);
                        break;
                }
            }
            if($this->dateRange)
            {
                $from = explode('to', $this->dateRange)[0];
                $to = explode('to', $this->dateRange)[1];
                $query->andFilterWhere(['between', 'calldate', trim($from, " "), trim($to, " ")]);
            }   
            if($this->state)
            {
                $query->andFilterWhere(['like', 'disposition', $this->state]);
            }
            if($this->note)
            {
                $query->andFilterWhere(['like', 'note', $this->note]);
            }
            return $data;
        }
        return null;
    }
}
