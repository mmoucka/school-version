<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use Yii;
use yii\db\ActiveRecord;

/**
 * ActiveRecord to used manipulate with dialout's entry in Asterisk DB.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class DialoutEntry  extends ActiveRecord
{
    public function rules()
    {
        return [
            ['destqueue', 'filter', 'filter' => 'trim'],
            ['destqueue', 'required'],            
            ['destqueue', 'string', 'min' => 0, 'max' => 255],
            ['number', 'number', 'max' => 99999999999999],
            ['number', 'required'],
            ['callerid', 'string', 'min' => 0, 'max' => 255], 
        ];
    }
    public static function tableName() 
    {
        return 'dialout';
    }    
    public static function getDb()
    {
        return Yii::$app->dbAsterisk;
    }
}
