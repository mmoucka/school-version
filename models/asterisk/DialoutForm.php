<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\asterisk\DialoutEntry;

/**
 * Model which manipulates with dialouts stored in database.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class DialoutForm extends Model
{
    public $id;
    public $destqueue;
    public $file;
    public $callerid;
    public $trunk;
    public $number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['destqueue', 'filter', 'filter' => 'trim'],
            ['destqueue', 'required'],            
            ['destqueue', 'string', 'min' => 0, 'max' => 255],
            ['number', 'integer', 'max' => 99999999999999],
            ['number', 'required'],
            ['file', 'required'],
            ['file', 'file', 'maxSize' => 20000000],
            ['callerid', 'string', 'min' => 0, 'max' => 255],    
            ['trunk', 'string', 'min' => 0, 'max' => 255],  
        ];
        return $rules;
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['import'] = ['file'];
        $scenarios['create'] = ['number', 'destqueue', 'trunk', 'callerid'];
        $scenarios['update'] = ['number', 'destqueue', 'trunk', 'callerid'];
        $scenarios['view'] = [];
        return $scenarios;
    }    
    static function listAll()
    {
        $query = new Query();
        $query->from('dialout')->orderBy('attempts');
        return $query->all(Yii::$app->dbAsterisk);
    }   
    public function initialize()
    {
        $entry = DialoutEntry::findOne(['id' => $this->id]);
        foreach ($this->attributes() as $value) {
            if (strcmp('id', $value) && strcmp('file', $value)) {
                $this->$value = $entry->$value;
            }
        }
    }
    static function updateQueue($oldQueue, $newQueue)
    {
        while($entry = DialoutEntry::findOne(['destqueue' => $oldQueue]))
        {
            $entry->destqueue = $newQueue;
            $entry->update();
        }
    }
    static function updateTrunk($oldTrunk, $newTrunk)
    {
        while($entry = DialoutEntry::findOne(['trunk' => $oldTrunk]))
        {
            $entry->destqueue = $newTrunk;
            $entry->update();
        }
    }
    static function deleteAllEntries()
    {
        while($entry = DialoutEntry::findOne())
        {
            $entry->delete();
        }
    }
    public function importCsv()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $this->file->name, true);
            $csv = array_map('str_getcsv', file('uploads/' . $this->file->name));
            unlink('uploads/' . $this->file);
            $entries = ['DialoutForm'];
            $header = ['number', 'destqueue', 'trunk', 'callerid'];
            foreach ($csv as $value) {
                foreach ($value as $key => $attribute) {
                    $entries['DialoutForm'][$header[$key]] = $attribute;
                }
                $model = new DialoutForm(['scenario' => 'create']);
                $model->load($entries);
                if (!$model->create()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    public function create()
    {
        if ($this->validate()) {
            $entry = new DialoutEntry();
            foreach ($this->attributes() as $value)
            {
                if(strcmp('id', $value) && strcmp('file', $value))
                {
                    $entry->$value = $this->$value;
                }
            }
            $entry->save();
            return true;
        }
        return false;
    }
    public function update()
    {
        if ($this->validate()) {
            $entry = DialoutEntry::findOne(['id' => $this->id]);
            foreach ($this->attributes() as $value)
            {
                if(strcmp('id', $value) && strcmp('file', $value))
                {
                    $entry->$value = $this->$value;
                }
            }
            $entry->update();
            return true;
        }
        return false;
    }
}
