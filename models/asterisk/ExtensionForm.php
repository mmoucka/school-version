<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\base\Model;
use yii\db\Query;
use app\models\asterisk\Endpoint;
use app\models\asterisk\Aor;
use app\models\asterisk\Auth;
use app\models\asterisk\PhoneBookForm;

use Yii;

/**
 * Model used to list, create and update Extensions.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class ExtensionForm extends Model
{
    /**
     *
     * @var string Username used in registration of Endpoint.
     */
    public $username;
    /**
     *
     * @var string Used for validation if username is changed during update.
     */
    public $oldusername;
    /**
     *
     * @var integer Maximum of registrations of Extension. 
     */
    public $max_contacts;
    /**
     *
     * @var string Password used in registration of Endpoint. 
     */
    public $password;
    /**
     * 
     * @var string To be called on.
     */
    public $id;
    /**
     *
     * @var string Used for validation if id is changed during update.
     */
    public $oldid;
    /**
     *
     * @var string Allowed codecs for this Extension 
     */
    public $allow;
    /**
     *
     * @var array Advanced settings of Extension. 
     */
    public $advanced;
    /**
     * @inheritdoc
     */        
    
    public $mac;
    
    public function rules()
    {
        $rules = [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],            
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass' => 'app\models\asterisk\Auth', 'message' => 'This username has already been taken.', 'when' => function()
            {
                return $this->oldusername != $this->username;
            }],
            ['id', 'filter', 'filter' => 'trim'],
            ['id', 'required'],
            ['id', 'compare', 'compareValue' => 1, 'operator' => '!='],
            ['id', 'unique', 'targetClass' => 'app\models\asterisk\Endpoint', 'message' => 'This id has already been taken.', 'when' =>  function()
            {
                return $this->oldid != $this->id;
            }],
            ['password', 'required'],
            ['allow', 'string'],
            ['max_contacts', 'required'],
            ['max_contacts', 'integer'],
            ['mac', 'validateMac']
        ];
        foreach ($this->getAdvancedNames() as $value) {
            $schema = $this->getSchema();
            $type = substr($schema->columns[$value]->dbType, 0, strpos($schema->columns[$value]->dbType, '('));
            if(!strcmp($type, 'int'))
            {
                $rules[] = [$value, 'integer'];
            }
            else if (!strcmp($type, 'varchar') || !strcmp($type, 'text'))
            {
                $rules[] = [$value, 'string'];
            }
        }
        return $rules;
    }
    public function validateMac($attribute, $params)
    {
        preg_match("/^([0-9A-F]{2}[:]){5}([0-9A-F]{2})$/", $this->$attribute, $match);
        if(empty($match))
        {
            $this->addError($attribute, "Is not valid MAC address");
        }
    }
    public function scenarios() {
        $scenarios = parent::scenarios();        
        $scenarios['update'] = ['username', 'id', 'password', 'max_contacts', 'allow', 'mac'];
        $scenarios['create'] = ['username', 'id', 'password', 'max_contacts', 'allow', 'mac'];
        $scenarios['view'] = [];
        foreach ($this->getAdvancedNames() as $value) {
            $scenarios['create'][] = $value;
            $scenarios['update'][] = $value;
        }
        return $scenarios;
    }
    /**
     * Returns schema of table 'ps_endpoints' in DB.
     * @return
     */
    public function getSchema()
    {
        $db = Yii::$app->dbAsterisk;
        $table = $db->getTableSchema('ps_endpoints');
        return $table;
    }
    /**
     * Returns names of advanced settings options.
     * @return array
     */
    public function getAdvancedNames()
    {
        $forbidenAttributes = ['id', 'transport', 'aors', 'auth', 'context', 'disallow', 'allow'];
        $endpoint = new Endpoint();
        $rawAttributes = $endpoint->attributes();
        $attributes = [];
        foreach ($rawAttributes as $value) {
            if(!in_array($value, $forbidenAttributes))
            {
                $attributes[] = $value;
            }
        }
        return $attributes;
    }
    /**
     * Returns values of advanced settings options.
     * @return array
     */
    public function getAdvanced()
    {
        $forbiddenAttributes = ['id', 'transport', 'aors', 'auth', 'context', 'disallow', 'allow'];
        $endpoint = Endpoint::findOne($this->id);
        $attributes = $endpoint->attributes();
        $advanced = [];
        foreach ($attributes as $value) {
            if(!in_array($value, $forbiddenAttributes))
            {
                $advanced[$value] = $endpoint->$value;
            }
        }
        return $advanced;
    }
    /**
     * Lists all extensions set in DB.
     * @return array
     */
    public function listAll()
    {
        $list = [];
        $aor = new Aor();
        $auth = new Auth();
        $query = new Query();
        $query->from('ps_endpoints')->select(['id'])->where(['context' => 'local']);
        $endpoints = $query->all(Yii::$app->dbAsterisk);
        foreach ($endpoints as $row) {
            array_push($list, 
                    [
                        'id' => $row['id'],
                        'username' => $auth->getUsername($row['id']),
                        'contacts' => $aor->getContacts($row['id']),
                        'user-agents' => $aor->getUAs($row['id']),
                    ]);
        }
        return $list;
    }    
    /**
     * Returns ID of Extension.
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }    
    /**
     * @see Aor
     * @return string
     */
    public function getContacts()
    {
        $aor = Aor::findOne($this->id);
        return $aor->getContacts();
    }
    /**
     * Creates new Extension
     * @return boolean
     */
    public function create()
    {
        if ($this->validate()) {
            $aor = new Aor();
            $auth = new Auth();
            $endpoint = new Endpoint();

            $aor->id = $auth->id = $this->id;
            $aor->max_contacts = $this->max_contacts;
            $aor->save();

            $auth->password = $this->password;
            $auth->username = $this->username;
            $auth->auth_type = 'userpass';
            $auth->save();

            $endpoint->id = $endpoint->aors = $endpoint->auth = $this->id;
            $endpoint->allow = $this->allow;
            $endpoint->disallow = 'all';
            $endpoint->context = 'local';
            $endpoint->transport = 'transport-udp';
            if($this->mac)
            {
                $this->generateCiscoConfig();
                $this->advanced['force_rport'] = 'no';
            }
            if ($this->advanced) {
                foreach ($this->advanced as $name => $value) {
                    if(!strcmp($value, ''))
                    {
                        $endpoint->$name = NULL;  
                    }                                                  
                    else
                    {
                        $endpoint->$name = $value;
                    }
                }
            }
            $endpoint->save();
            return true;
        }
        
        return false;
    }
    /**
     * Edits existing Extension.
     * @return boolean
     */
    public function update()
    {
        if ($this->validate()) {
            $aor = Aor::findOne($this->oldid);
            $auth = Auth::findOne($this->oldid);
            $endpoint = Endpoint::findOne($this->oldid);
            if((strcmp($this->id, $this->oldid)) && PhoneBookForm::hasPhoneBook($this->oldid))
            {
                PhoneBookForm::updateAuth($this->oldid, $this->id);
            }            
            $aor->id = $auth->id = $this->id;
            $aor->max_contacts = $this->max_contacts;
            $aor->update();
            
            $auth->password = $this->password;
            $auth->username = $this->username;
            $auth->update();
            
            $endpoint->id = $endpoint->aors = $endpoint->auth = $this->id;
            $endpoint->allow = $this->allow;
            $endpoint->disallow = 'all';
            if($this->mac)
            {
                $this->generateCiscoConfig();
                $this->advanced['force_rport'] = 'no';
            }
            if ($this->advanced) {
                foreach ($this->advanced as $name => $value) {
                    if(!strcmp($value, ''))
                    {
                        $endpoint->$name = NULL; 
                    }                           
                    else
                    {
                        $endpoint->$name = $value;
                    }
                }
            }            
            $endpoint->update();
            return true;
        }
        return false;
    }
    /**
     * Initializes Extension from DB.
     */
    public function initialize()
    {        
        $aor = Aor::findOne($this->id);
        $auth = Auth::findOne($this->id);
        $endpoint = Endpoint::findOne($this->id);
        $this->username = $auth->username;
        $this->max_contacts = $aor->max_contacts;
        $this->password = $auth->password;
        $this->advanced = $this->getAdvanced();
        $this->allow = $endpoint->allow;
        $this->mac = $this->parseMac();
    }
    public function parseMac()
    {
        $files = glob('/tftpboot/SEP*.cnf.xml.txt');
        error_log(print_r($files, true));
        foreach ($files as $file) {
            error_log($file);
            if (strpos(file_get_contents($file), "<authName>{$this->username}</authName>")) {
                $filesFound = $file;
            }
        }
        if(isset($filesFound))
        {
            preg_match("/[0-9A-F]{12}/", $file, $match);
            $i = 0;
            $result = '';
            while($i < strlen($match[0]))
            {
                if ($i > 0 && $i % 2 == 0) {
                    $result .= ':' . $match[0][$i];
                }
                else
                {
                    $result .= $match[0][$i];
                }
                $i++;
            }
            return $result;
        }
        return null;
    }
    /**
     * Deletes Extension from DB.
     * @return boolean
     */
    public function delete()
    {
        $aor = Aor::findOne(['id' => $this->id]);
        $auth = Auth::findOne(['id' => $this->id]);
        $endpoint = Endpoint::findOne(['id' => $this->id]);
        if($aor && $auth && $endpoint)
        {
            $aor->delete();
            $auth->delete();                        
            $endpoint->delete();
            if(PhoneBookForm::hasPhoneBook($this->id))
            {
                PhoneBookForm::deleteAllEntries($this->id);
            }     
            return true;
        }
        return false;
    }
    public function __get($name) {
        if(in_array($name, $this->getAdvancedNames()))
        {
            return isset($this->advanced[$name]) ? $this->advanced[$name] : null;
        }
        else
        {
            parent::__get($name); 
        }
    }
    public function __set($name, $value) {
        if(in_array($name, $this->getAdvancedNames()))
        {
            $this->advanced[$name] = $value;
        }
        else 
        {
            parent::__set($name, $value);        
        }        
    }
    public function __isset($name) {
        if(in_array($name, $this->getAdvancedNames()))
        {
            return isset($this->advanced[$name]) ? true : false;
        }
        else
        {
            parent::__isset($name);  
        }             
    }
    public function setAttributes($values, $safeOnly = true)
    {
        if (is_array($values)) {
            $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes());
            foreach ($values as $name => $value) {
                if(in_array($name, $this->getAdvancedNames()))
                {
                    $this->advanced[$name] = $value;
                }
                else if (isset($attributes[$name])) {
                    $this->$name = $value;
                } elseif ($safeOnly) {
                    $this->onUnsafeAttribute($name, $value);
                }
            }
        }
    }
    protected function generateCiscoConfig()
    {
        $mac = preg_replace("/:/", '', $this->mac);
        $file = fopen("/tftpboot/SEP{$mac}.cnf.xml.txt", "w");
        $config = "<device>
            <fullConfig>true</fullConfig>
            <deviceProtocol>SIP</deviceProtocol>
            <sshUserId>admin</sshUserId>
            <sshPassword>cisco</sshPassword>
            <devicePool>
            <dateTimeSetting> 
            <dateTemplate>D/M/Y</dateTemplate> 
            <timeZone>GET Standard/Daylight Time</timeZone> 
            <ntps> 
            <ntp>
            <name>81.0.246.244</name> #adresa NTP serveru 
            <ntpMode>Unicast</ntpMode> 
            </ntp>
            </ntps>
            </dateTimeSetting>
            <callManagerGroup>
            <tftpDefault>true</tftpDefault>
            <members>
            <member priority=\"0\">
            <callManager>
            <ports>
            <ethernetPhonePort>2000</ethernetPhonePort>
            <sipPort>5060</sipPort>
            <securedSipPort>5062</securedSipPort>
            </ports>
            <processNodeName>{$_SERVER['SERVER_ADDR']}</processNodeName>
            </callManager>
            </member>
            </members>
            </callManagerGroup>
            </devicePool>
            <commonProfile>
            <phonePassword></phonePassword>
            <backgroundImageAccess>true</backgroundImageAccess>
            <callLogBlfEnabled>0</callLogBlfEnabled>
            </commonProfile>
            <loadInformation309 model=\"Cisco 7961G-G\">SIP41.8-5-3S</loadInformation309>
            <vendorConfig>
            <disableSpeaker>false</disableSpeaker>
            <disableSpeakerAndHeadset>false</disableSpeakerAndHeadset>
            <pcPort>0</pcPort>
            <settingsAccess>1</settingsAccess>
            <garp>0</garp>
            <voiceVlanAccess>0</voiceVlanAccess>
            <videoCapability>0</videoCapability>
            <autoSelectLineEnable>0</autoSelectLineEnable>
            <daysDisplayNotActive>1,2,3,4,5,6,7</daysDisplayNotActive>
            <displayOnTime>10:00</displayOnTime>
            <displayOnDuration>00:01</displayOnDuration>
            <displayIdleTimeout>00:05</displayIdleTimeout> 
            <webAccess>0</webAccess>
            <spanToPCPort>1</spanToPCPort>
            <loggingDisplay>1</loggingDisplay>
            <loadServer></loadServer>
            </vendorConfig>
            <userLocale>
            <name>English_United_States</name>
            <uid>1</uid>
            <langCode>en_US</langCode>
            <version>1.0.0.0-1</version>
            <winCharSet>iso-8859-1</winCharSet>
            </userLocale>
            <networkLocale>United_States</networkLocale> 
            <networkLocaleInfo> 
            <name>United_States</name> 
            <uid>64</uid> 
            <version>1.0.0.0-1</version> 
            </networkLocaleInfo> 
            <deviceSecurityMode>1</deviceSecurityMode>
            <authenticationURL></authenticationURL>
            <directoryURL></directoryURL> 
            <idleTimeout>10</idleTimeout>
            <idleURL></idleURL>
            <informationURL></informationURL>
            <messagesURL></messagesURL>
            <proxyServerURL></proxyServerURL>
            <servicesURL>http://cisco.internect.net/</servicesURL>
            <dscpForSCCPPhoneConfig>96</dscpForSCCPPhoneConfig>
            <dscpForSCCPPhoneServices>0</dscpForSCCPPhoneServices>
            <dscpForCm2Dvce>96</dscpForCm2Dvce>
            <transportLayerProtocol>4</transportLayerProtocol>
            <capfAuthMode>0</capfAuthMode>
            <capfList>
            <capf>
            <phonePort>3804</phonePort>
            </capf>
            </capfList>
            <certHash></certHash>
            <encrConfig>false</encrConfig>
            <sipProfile>
            <sipProxies>
            <backupProxy>{$_SERVER['SERVER_ADDR']}</backupProxy>
            <backupProxyPort>5060</backupProxyPort>
            <emergencyProxy></emergencyProxy>
            <emergencyProxyPort></emergencyProxyPort>
            <outboundProxy></outboundProxy>
            <outboundProxyPort></outboundProxyPort>
            <registerWithProxy>true</registerWithProxy>
            </sipProxies>
            <sipCallFeatures>
            <cnfJoinEnabled>true</cnfJoinEnabled>
            <callForwardURI>x--serviceuri-cfwdall</callForwardURI>
            <callPickupURI>x-cisco-serviceuri-pickup</callPickupURI>
            <callPickupListURI>x-cisco-serviceuri-opickup</callPickupListURI>
            <callPickupGroupURI>x-cisco-serviceuri-gpickup</callPickupGroupURI>
            <meetMeServiceURI>x-cisco-serviceuri-meetme</meetMeServiceURI>
            <abbreviatedDialURI>x-cisco-serviceuri-abbrdial</abbreviatedDialURI>
            <rfc2543Hold>true</rfc2543Hold>
            <callHoldRingback>2</callHoldRingback>
            <localCfwdEnable>true</localCfwdEnable>
            <semiAttendedTransfer>true</semiAttendedTransfer>
            <anonymousCallBlock>2</anonymousCallBlock>
            <callerIdBlocking>0</callerIdBlocking>
            <dndControl>0</dndControl>
            <remoteCcEnable>true</remoteCcEnable>
            </sipCallFeatures>
            <sipStack>
            <sipInviteRetx>6</sipInviteRetx>
            <sipRetx>10</sipRetx>
            <timerInviteExpires>180</timerInviteExpires>
            <timerRegisterExpires>120</timerRegisterExpires>
            <timerRegisterDelta>5</timerRegisterDelta>
            <timerKeepAliveExpires>120</timerKeepAliveExpires>
            <timerSubscribeExpires>120</timerSubscribeExpires>
            <timerSubscribeDelta>5</timerSubscribeDelta>
            <timerT1>500</timerT1>
            <timerT2>4000</timerT2>
            <maxRedirects>70</maxRedirects>
            <remotePartyID>false</remotePartyID>
            <userInfo>None</userInfo>
            </sipStack>
            <autoAnswerTimer>1</autoAnswerTimer>
            <autoAnswerAltBehavior>false</autoAnswerAltBehavior>
            <autoAnswerOverride>true</autoAnswerOverride>
            <transferOnhookEnabled>true</transferOnhookEnabled>
            <enableVad>false</enableVad>
            <preferredCodec>none</preferredCodec>
            <dtmfAvtPayload>101</dtmfAvtPayload>
            <dtmfDbLevel>3</dtmfDbLevel>
            <dtmfOutofBand>avt</dtmfOutofBand>
            <alwaysUsePrimeLine>false</alwaysUsePrimeLine>
            <alwaysUsePrimeLineVoiceMail>false</alwaysUsePrimeLineVoiceMail>
            <kpml>3</kpml>
            <stutterMsgWaiting>1</stutterMsgWaiting>
            <callStats>false</callStats>
            <silentPeriodBetweenCallWaitingBursts>10</silentPeriodBetweenCallWaitingBursts>
            <disableLocalSpeedDialConfig>false</disableLocalSpeedDialConfig>
            <startMediaPort>16384</startMediaPort>
            <stopMediaPort>16399</stopMediaPort>
            <voipControlPort>5061</voipControlPort>
            <dscpForAudio>184</dscpForAudio>
            <ringSettingBusyStationPolicy>0</ringSettingBusyStationPolicy>
            <dialTemplate>dialplan.xml</dialTemplate>
            <phoneLabel>OPTOKON</phoneLabel>
            <natEnabled>false</natEnabled> 
            <natAddress></natAddress> 
            <sipLines>
            <line button=\"1\">
            <featureID>9</featureID>
            <featureLabel>{$this->username} - {$this->id}</featureLabel>
            <name>{$this->id}</name>
            <displayName>{$this->id}</displayName>
            <contact>{$this->id}</contact>
            <proxy>{$_SERVER['SERVER_ADDR']}</proxy>
            <port>5060</port>
            <autoAnswer>
            <autoAnswerEnabled>2</autoAnswerEnabled>
            </autoAnswer>
            <callWaiting>3</callWaiting>
            <authName>{$this->username}</authName>
            <authPassword>{$this->password}</authPassword>
            <sharedLine>false</sharedLine>
            <messageWaitingLampPolicy>1</messageWaitingLampPolicy>
            <messagesNumber>*79</messagesNumber>
            <ringSettingIdle>4</ringSettingIdle>
            <ringSettingActive>5</ringSettingActive>
            <forwardCallInfoDisplay>
            <callerName>true</callerName>
            <callerNumber>false</callerNumber>
            <redirectedNumber>false</redirectedNumber>
            <dialedNumber>true</dialedNumber>
            </forwardCallInfoDisplay>
            </line>
            <line button=\"2\">
            <featureID>9</featureID>
            <featureLabel>Line 2</featureLabel>
            <name></name>
            <displayName></displayName>
            <contact></contact>
            <proxy></proxy>
            <port>5060</port>
            <autoAnswer>
            <autoAnswerEnabled>2</autoAnswerEnabled>
            </autoAnswer>
            <callWaiting>3</callWaiting>
            <authName></authName>
            <authPassword></authPassword>
            <sharedLine>false</sharedLine>
            <messageWaitingLampPolicy>1</messageWaitingLampPolicy>
            <messagesNumber>*97</messagesNumber>
            <ringSettingIdle>4</ringSettingIdle>
            <ringSettingActive>5</ringSettingActive>
            <forwardCallInfoDisplay>
            <callerName>true</callerName>
            <callerNumber>false</callerNumber>
            <redirectedNumber>false</redirectedNumber>
            <dialedNumber>true</dialedNumber>
            </forwardCallInfoDisplay>
            </line>
            </sipLines>
            </sipProfile>

            </device>";
        fwrite($file, $config);    
    }
}
