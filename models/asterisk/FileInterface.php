<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use Yii;
use yii\db\Query;
/**
 * Interface used to manipulate with static realtime configuration files in Asterisk DB.
 * Needs to be extended to operate with specific file.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class FileInterface {
    
    /**
     * 
     * @var string Name of configuration file.
     */
    public $filename;
    /**
     *
     * @var ActiveRecord Used to manipulate with line in its table.
     */
    public $line;
    /**
     *
     * @var string Name of table in database.
     */
    public $table;
    
    public function __construct() {
        $this->filename = static::getFilename();
        $this->table = static::getTable();
        $this->line = static::getLine();
    }
    /**
     * Returns name of a file. Internal usage
     * @return string
     */
    protected static function getFilename() {
        return;
    }
    /**
     * 
     * @return ActiveRecord Internal usage
     */
    protected static function getLine() {
        return;
    }
    /**
     * Return table name. Internal usage
     * @return string
     */
    protected static function getTable() {
        return;
    }
    /**
     * Adds new category to a configuration file. Automatically increases category metric.
     * @param string $categoryName Category's name.
     * @param array $options Options to be saved in category.
     */
    public function addCategory ($categoryName, $options = [])
    {
        $catMetric = $this->getLastCatMetric() + 1;
        foreach ($options as $key => $params) {
            $line = static::getLine();
            $line->filename = $this->filename;
            $line->category = $categoryName;
            $line->cat_metric = $catMetric;
            $line->var_metric = $key;
            foreach ($params as $name => $value) {
                $line->var_name = $name;
                $line->var_val = $value;
            }
            $line->save();
        }
    }
    /**
     * Deletes whole category.
     * @param string $categoryName Category's name.
     */
    public function deleteCategory($categoryName)
    {
        $line = $this->line;
        $line->deleteAll(['category' => $categoryName, 'filename' => $this->filename]);
    }
    /**
     * Returns all options set in category.
     * @param string $categoryName Category's name.
     * @return array
     */
    public function getAllFromCategory($categoryName)
    {
        $query = new Query();
        $query->from($this->table)->where(['category' => $categoryName, 'filename' => $this->filename]);
        $result = $query->all(Yii::$app->dbAsterisk);
        return $result;
    }
    /**
     * Returns value of variable from specified category.
     * @param string $categoryName Category's name.
     * @param string $varname Variable's name.
     * @return string
     */
    public function getFromCategory($categoryName, $varname)
    {
        $query = new Query();
        $query->from($this->table)->select(['var_val'])->where(['category' => $categoryName, 'filename' => $this->filename, 'var_name' => $varname]);
        $result = $query->one(Yii::$app->dbAsterisk);
        return implode('', $result);
    }
    /**
     * Returns metric of last category in a file.
     * @return integer
     */
    protected function getLastCatMetric ()
    {
        $query = new Query();
        $query->from($this->table)->select(['cat_metric'])->orderBy('cat_metric DESC');
        $result = $query->one(Yii::$app->dbAsterisk);
        return implode('', $result);
    }
}
