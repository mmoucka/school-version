<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\base\Model;
use app\models\asterisk\PhoneBookEntry;

/**
 * Model which manipulates with phonebooks stored in database.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class PhoneBookForm extends Model
{
    /**
     *
     * @var int Primary key of a phone book entry.
     */
    public $id;
    /**
     *
     * @var string To which extension is PhoneBook joined.
     */
    public $auth;
    /**
     *
     * @var string Name of a contact.
     */
    public $name;
    /**
     *
     * @var file Uploaded file .csv for entries importing.
     */
    public $file;
    /**
     *
     * @var string Description of an entry.
     */
    public $description;
    /**
     *
     * @var int Phone number of a contact.
     */
    public $number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],            
            ['name', 'string', 'min' => 0, 'max' => 255],
            ['number', 'integer', 'max' => 99999999999999],
            ['number', 'required'],
            ['file', 'required'],
            ['file', 'file', 'maxSize' => 20000000],
            ['description', 'filter', 'filter' => 'trim'],
            ['description', 'string'],               
        ];
        return $rules;
    }
    /**
     * @inheritdoc
     */
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['owner'] = ['name', 'number', 'description'];
        $scenarios['import'] = ['file'];
        $scenarios['create'] = ['name', 'number', 'description'];
        $scenarios['view'] = [];
        return $scenarios;
    }    
    /**
     * Returns array of PhoneBook entries.
     * @param string $auth To which extension is PhoneBook joined.
     * @return array
     */
    public function listAllEntries($auth = null)
    {
        $list = PhoneBookEntry::findAll(['auth' => $auth ? $auth : $this->auth]);
        return $list;
    }   
    /**
     * Returns true when there is PhoneBook joined to the extension.
     * @param string $auth To which extension is PhoneBook joined.
     * @return boolean
     */
    static function hasPhoneBook($auth = null)
    {
        $list = PhoneBookEntry::findOne(['auth' => $auth ? $auth : $this->auth]);
        return empty($list) ? false : true;
    }
    /**
     * Moves PhoneBook to another extension or updates extension's ID.
     * @param string $oldAuth To which extension is PhoneBook joined.
     * @param string $newAuth New extension ID.
     */
    static function updateAuth ($oldAuth, $newAuth)
    {
        while($entry = PhoneBookEntry::findOne(['auth' => $oldAuth]))
        {
            $entry->auth = $newAuth;
            $entry->update();
        }
    }
    /**
     * Deletes all entries in PhoneBook
     * @param string $auth To which extension is PhoneBook joined.
     */
    static function deleteAllEntries($auth)
    {
        while($entry = PhoneBookEntry::findOne(['auth' => $auth]))
        {
            $entry->delete();
        }
    }
    /**
     * Imports entries from CSV to PhoneBook
     * @return boolean
     */
    public function importCsv()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $this->file->name, true);
            $csv = array_map('str_getcsv', file('uploads/' . $this->file->name));
            unlink('uploads/' . $this->file);
            $entries = ['PhoneBookForm'];
            $header = ['name', 'number', 'description'];
            $error = '';
            foreach ($csv as $value) {
                foreach ($value as $key => $attribute) {
                    $entries['PhoneBookForm'][$header[$key]] = $attribute;
                }
                $model = new PhoneBookForm(['scenario' => 'create']);
                $model->auth = $this->auth;
                $model->load($entries);
                if (!$model->create()) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    /**
     * Creates new PhoneBook's entry.
     * @return boolean
     */
    public function create()
    {
        if ($this->validate()) {
            $entry = new PhoneBookEntry();
            foreach ($this->attributes() as $value)
            {
                if(strcmp('id', $value) && strcmp('file', $value))
                {
                    $entry->$value = $this->$value;
                }
            }
            $entry->save();
            return true;
        }
        return false;
    }
    /**
     * Updates existing PhoneBook's entry.
     * @return boolean
     */
    public function update()
    {
        if ($this->validate()) {
            $entry = PhoneBookEntry::findOne(['id' => $this->id]);
            foreach ($this->attributes() as $value)
            {
                if(strcmp('id', $value) && strcmp('file', $value))
                {
                    $entry->$value = $this->$value;
                }
            }
            $entry->update();
            return true;
        }
        return false;
    }
}
