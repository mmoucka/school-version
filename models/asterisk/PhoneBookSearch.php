<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\data\ActiveDataProvider;
/**
 * Model which searches in phonebooks stored in database.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class PhoneBookSearch extends PhoneBookForm
{
    public function rules()
    {
        return [
            [['name', 'number', 'description'], 'safe'],
        ];
    }
    /**
     * Returns results of search as an ActiveDataProvider
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PhoneBookEntry::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andFilterWhere(['auth' => $this->auth]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }        
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'number', $this->number]);
        $query->andFilterWhere(['like', 'description', $this->description]);
        return $dataProvider;
    }
}
