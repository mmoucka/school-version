<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\base\Model;
use yii\db\Query;
use app\models\asterisk\Queue;
use app\models\asterisk\QueueMember;

use Yii;
/**
 * Model which allows create, update or delete Queue.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */
class QueueForm extends Model
{
    /**
     * @var string  Name of the queue.
     */
    public $name;
    /**
     * @var string Used for verification if queue name is changed during update.
     */
    public $oldname;
    /**
     * @var string  Local into which queue will be put.
     */
    public $context = 'local';
    /**
     * @var string Source of Muusic on hold (default/custom)
     */
    public $musiconhold;
    /**
     * @var string Name of file which is played when caller enters queue.
     */
    public $announce;
    /**
     *
     * @var int Timeout in seconds after caller is kicked from queue.
     */
    public $timeout;
    /**
     *
     * @var string
     */
    public $ringinuse;
    /**
     *
     * @var string
     */
    public $queue_youarenext;
    /**
     *
     * @var string
     */
    public $queue_thereare;
    /**
     *
     * @var string
     */
    public $queue_callswaiting;
    /**
     *
     * @var string
     */
    public $queue_quantity1;
    /**
     *
     * @var string
     */
    public $queue_quantity2;
    /**
     *
     * @var string
     */
    public $queue_holdtime;
    /**
     *
     * @var string
     */
    public $queue_minutes;
    /**
     *
     * @var string
     */
    public $queue_minute;
    /**
     *
     * @var string
     */
    public $queue_seconds;
    /**
     *
     * @var string
     */
    public $queue_thankyou;
    /**
     *
     * @var string
     */
    public $queue_callerannounce;
    /**
     *
     * @var string
     */
    public $queue_reporthold;
    /**
     *
     * @var int
     */
    public $announce_frequency;
    /**
     *
     * @var string
     */
    public $announce_to_first_user;
    /**
     *
     * @var int
     */
    public $min_announce_frequency;
    /**
     *
     * @var int
     */
    public $announce_round_seconds;
    /**
     *
     * @var int
     */
    public $announce_holdtime;
    /**
     *
     * @var string
     */
    public $announce_position;
    /**
     *
     * @var int
     */
    public $announce_position_limit;
    /**
     *
     * @var string
     */
    public $periodic_announce;
    /**
     *
     * @var int
     */
    public $periodic_announce_frequency;
    /**
     *
     * @var string
     */
    public $relative_periodic_announce;
    /**
     *
     * @var string
     */
    public $random_periodic_announce;
    /**
     *
     * @var int
     */
    public $retry;
    /**
     *
     * @var int
     */
    public $wrapuptime;
    /**
     *
     * @var string
     */
    public $autofill;
    /**
     *
     * @var string
     */
    public $autopause;
    /**
     *
     * @var int
     */
    public $autopausedelay;
    /**
     *
     * @var string
     */
    public $autopausebusy;
    /**
     *
     * @var string
     */
    public $autopauseunavail;
    /**
     *
     * @var int
     */
    public $maxlen;
    /**
     *
     * @var string
     */
    public $strategy;
    /**
     *
     * @var string
     */
    public $joinempty;
    /**
     *
     * @var string
     */
    public $leavewhenempty;
    /**
     *
     * @var int
     */
    public $reportholdtime;
    /**
     *
     * @var int
     */
    public $memberdelay;
    /**
     *
     * @var string
     */
    public $nextqueue;
    /**
     *
     * @var string
     */
    public $members;
    /**
     *
     * @var string
     */
    public $withaudio = ['periodic_announce','queue_youarenext','queue_thereare','queue_callswaiting','queue_quantity1','queue_quantity2',
        'queue_holdtime','queue_minutes','queue_minute','queue_seconds','queue_thankyou','queue_callerannounce','queue_reporthold'];
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],            
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['name', 'unique', 'targetClass' => 'app\models\asterisk\Queue', 'message' => 'This name has already been taken.', 'when' => function()
            {
                return $this->oldname != $this->name;
            }],
            ['retry', 'integer', 'max' => 3600],
            ['maxlen', 'integer', 'max' => 3600],
            ['timeout', 'integer', 'max' => 3600],
            ['announce_frequency', 'integer', 'max' => 3600],
            ['min_announce_frequency', 'integer', 'max' => 3600],
            ['announce_round_seconds', 'integer', 'max' => 3600],
            ['announce_position_limit', 'integer', 'max' => 3600],
            ['periodic_announce_frequency', 'integer', 'max' => 3600],
            ['relative_periodic_announce', 'integer', 'max' => 3600],
            ['wrapuptime', 'integer', 'max' => 3600],
            ['autopausedelay', 'integer', 'max' => 3600],
            ['memberdelay', 'integer', 'max' => 3600],
        ];
        return $rules;
    }
    /**     *
     * @return User|null the saved model or null if saving fails
     */
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['name','musiconhold','announce','timeout','ringinuse',
            'queue_youarenext','queue_thereare','queue_callswaiting','queue_quantity1',
            'queue_quantity2','queue_holdtime','queue_minutes','queue_minute','queue_seconds',
            'queue_thankyou','queue_callerannounce','queue_reporthold','announce_frequency',
            'announce_to_first_user','min_announce_frequency','announce_round_seconds','announce_holdtime',
            'announce_position','announce_position_limit','periodic_announce','periodic_announce_frequency',
            'relative_periodic_announce','random_periodic_announce','retry','wrapuptime','autofill','autopause',
            'autopausedelay','autopausebusy','autopauseunavail','maxlen','strategy','joinempty','leavewhenempty',
            'reportholdtime','memberdelay','nextqueue','members'];
        $scenarios['create'] = ['name','musiconhold','announce','timeout','ringinuse',
            'queue_youarenext','queue_thereare','queue_callswaiting','queue_quantity1',
            'queue_quantity2','queue_holdtime','queue_minutes','queue_minute','queue_seconds',
            'queue_thankyou','queue_callerannounce','queue_reporthold','announce_frequency',
            'announce_to_first_user','min_announce_frequency','announce_round_seconds','announce_holdtime',
            'announce_position','announce_position_limit','periodic_announce','periodic_announce_frequency',
            'relative_periodic_announce','random_periodic_announce','retry','wrapuptime','autofill','autopause',
            'autopausedelay','autopausebusy','autopauseunavail','maxlen','strategy','joinempty','leavewhenempty',
            'reportholdtime','memberdelay','nextqueue','members'];
        $scenarios['view'] = [];
        return $scenarios;
    }
    public function listAll()
    {
        $query = new Query();
        $query->from('queues');
        $list = $query->all(Yii::$app->dbAsterisk);
        if(is_array($list))
        {
            foreach ($list as $key => $value) {
                $query = new Query();
                $query->from('queue_members')->where(['queue_name' => $value['name']]);
                $members = $query->all(Yii::$app->dbAsterisk);
                $list[$key]['members'] = count(array_keys($members));
            }
            return $list;  
        }
                      
        return [];
    }     
    static function getMembers($queue)
    {
        $query = new Query();
        $query->from('queue_members')->where(['queue_name' => $queue]);
        $members = $query->all(Yii::$app->dbAsterisk);
        return $members;
    }
    public function  initialize()
    {
        $queue = Queue::findOne(['name' => $this->name]);
        foreach ($this->attributes as $key => $value) {
            if (strcmp('oldname', $key) && strcmp('members', $key) && strcmp('withaudio', $key)) {
                 $this->$key = $queue->$key;
            }
        }
        $query = new Query();
        $query->from('queue_members')->where(['queue_name' => $this->name]);
        $members = $query->all(Yii::$app->dbAsterisk);
        foreach ($members as $value) {
            $names[] = $value['membername'];
        }
        $this->members = implode(',', $names);
    }
    public function create()
    {
        if ($this->validate()) {
            $queue = new Queue();            
            foreach ($this->attributes as $key => $value) {
                if(strcmp('oldname', $key) && strcmp('members', $key) && strcmp('withaudio', $key))
                {
                    $queue->$key = $this->$key ? $this->$key : null;
                }
            }
            $members = explode(',', $this->members);
            foreach ($members as $extension) {
                $member = new QueueMember();
                $member->queue_name = $this->name;
                $member->interface = $member->state_interface = 'PJSIP/' . $extension;
                $member->membername = $extension;
                $member->penalty = $member->paused = 0;
                $member->save();
            }
            $queue->save();
            return $queue;
        }
        return null;
    }
    public function update()
    {
         if ($this->validate()) {
            $queue = Queue::findOne(['name' => $this->oldname]);  
            foreach ($this->attributes as $key => $value) {
                if(strcmp('oldname', $key) && strcmp('members', $key) && strcmp('withaudio', $key))
                {
                    $queue->$key = $this->$key ? $this->$key : null;
                }
            }
            $member = new QueueMember();
            $member->deleteAll(['queue_name' => $this->oldname]);
            $members = explode(',', $this->members);
            foreach ($members as $extension) {
                $member = new QueueMember();
                $member->queue_name = $this->name;
                $member->interface = $member->state_interface = 'PJSIP/' . $extension;
                $member->membername = $extension;
                $member->penalty = $member->paused = 0;
                $member->save();
            }
            $queue->update();
            return $queue;
        }
        return null;
    }
    public function delete()
    {
        $member = new QueueMember();
        $queue = Queue::findOne(['name' => $this->name]);
        $queue->delete();
        $member->deleteAll(['queue_name' => $this->name]);
        return true;
    }
}
