<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\base\Model;
use yii\db\Query;
use app\models\asterisk\Route;

use Yii;

/**
 * Model used for listing and uploading record files.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */
class RecordForm extends Model
{
    /**
     *
     * @var file File to be uploaded. 
     */
    public $file;
    public function rules()
    {
        $rules = [
            ['file', 'required'],
            ['file', 'file', 'maxSize' => 1024 * 1024 * 20],
        ];
        return $rules;
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['upload'] = ['file'];
        return $scenarios;
    }
    /**
     * Return list of all files stored in "/var/asterisk/records/"
     * @return array
     */
    public function listAllRecords()
    {
        $list = [];
        $files = \yii\helpers\FileHelper::findFiles('/var/asterisk/records/',['recursive' => true]);
        foreach ($files as $value) {
            $file = basename($value);
            $size = filesize($value)/1000;
            array_push($list, ['id' => $file, 'size' => $size]);
        }       
        return $list;
    }     
    public function listAllMohs()
    {
        $list = [];
        $files = \yii\helpers\FileHelper::findFiles('/var/asterisk/moh/',['recursive' => true]);
        foreach ($files as $value) {
            $file = basename($value);
            $size = filesize($value)/1000;
            array_push($list, ['id' => $file, 'size' => $size]);
        }       
        return $list;
    }  
    /**
     * Uploads file to "/var/asterisk/records/"
     * @return boolean
     */
    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs('/var/asterisk/records/'.$this->file->name, true);
            return true;
        }
        return false;
    }
    public function uploadmoh()
    {
        if ($this->validate()) {
            $this->file->saveAs('/var/asterisk/moh/'.$this->file->name, true);
            return true;
        }
        return false;
    }
}
