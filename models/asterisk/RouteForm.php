<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\base\Model;
use yii\db\Query;
use app\models\asterisk\Route;

use Yii;

/**
 * Model which manipulates with routes stored in database.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */

class RouteForm extends Model
{
    /**
     *
     * @var integer Unique ID of a route. 
     */
    public $id;
    /**
     *
     * @var integer Route's metric.
     */
    public $metric;
    /**
     * 1 - Emergency, 2 - Feature code, 3 - Local number, 4 - Outbound routes
     * 5 - Inbound routes
     * @var integer Type of route.
     * 
     */
    public $type;
    /**
     *
     * @var string Pattern in format used by Asterisk's dialplan.
     */
    public $pattern;
    /**
     *
     * @var string Action to be done after pattern is matched.
     */
    public $action;
    /**
     *
     * @var string Data used by action.
     */
    public $data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['pattern', 'filter', 'filter' => 'trim'],
            ['pattern', 'required'],            
            ['pattern', 'string', 'min' => 0, 'max' => 255],
            ['data', 'required'],               
        ];
        return $rules;
    }
    /**
     * Returns last (highest) metric.
     * @return integer
     */
    protected function getLastMetric ()
    {
        $query = new Query();
        $query->from('dialplan')->select(['metric'])->orderBy('metric DESC');
        $result = $query->one(Yii::$app->dbAsterisk);
        if(!$result)
        {
            $result = [1];
        }
        return implode('', $result);
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['pattern', 'data'];
        $scenarios['create'] = ['pattern', 'data'];
        $scenarios['view'] = [];
        return $scenarios;
    }
    /**
     * Returns list of all routes in DB of the specified type.
     * @param integer $type
     * @return array
     */
    public function listAll($type)
    {
        $query = new Query();
        $query->from('dialplan')->where(['type' => $type])->orderBy('metric');
        $list = $query->all(Yii::$app->dbAsterisk);
        if(is_array($list))
            return $list;            
        return [];
    }     
    /**
     * Edits routes metric by their possition in array.
     * @param array $routes
     */
    public function sort($routes)
    {
        $array = explode(',', $routes);
        foreach ($array as $key => $value) {
            $route = Route::findOne($value);
            $route->metric = $key;
            $route->update();
        }
    }
    /**
     * Creates new route.
     * @return boolean
     */
    public function create()
    {
        if ($this->validate()) {
            $route = new Route();
            $dataArray = explode(',', $this->data);
            if(!strcmp($dataArray[0], 'extension') || !strcmp($dataArray[0], 'trunk'))
            {
                 $this->action = 'dial';
            }
            else if(!strcmp($dataArray[0], 'app') || !strcmp($dataArray[0], 'queue'))
            {
                $this->action = 'goto';
            }
            $route->metric = $this->getLastMetric() + 1;
            $route->pattern = $this->pattern;
            $route->type = $this->type;
            $route->action = $this->action;
            $route->data = $this->data;
            $route->save();
            return true;
        }
        return false;
    }
    /**
     * Edits existing route.
     * @return boolean
     */
    public function update()
    {
        if ($this->validate()) {
            $route = Route::findOne($this->id);
            $dataArray = explode(',', $this->data);
            if(!strcmp($dataArray[0], 'extension') || !strcmp($dataArray[0], 'trunk'))
            {
                $this->action = 'dial';
            }                    
            else if(!strcmp($dataArray[0], 'app') || !strcmp($dataArray[0], 'queue'))
            {
                $this->action = 'goto';
            }
            $route->pattern = $this->pattern;
            $route->action = $this->action;
            $route->data = $this->data;
            $route->update();
            return true;
        }
        return false;
    }
}
