<?php 
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use yii\base\Model;
use yii\db\Query;
use app\models\asterisk\pjsipFileInterface;
use app\models\asterisk\Endpoint;
use app\models\asterisk\Aor;
use app\models\asterisk\Auth;
use phpari\AGI_AsteriskManager;

use Yii;
/**
 * Model which manipulates with trunks to external provider stored in database.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */
class TrunkForm extends Model
{
    /**
     *
     * @var string Used during registrion to provider.
     */
    public $username;
    /**
     *
     * @var string Used during registrion to provider.
     */
    public $password;
    /**
     *
     * @var string Unique trunk's name.
     */
    public $id;
    /**
     * @var string Used for validation if trunk's name is changed during update.
     */
    public $oldid;
    /**
     *
     * @var string Allowed codecs for a trunk.
     */
    public $allow;
    /**
     *
     * @var integer Port number used during registration to provider.
     */
    public $port;
    /**
     *
     * @var string Providers registrar server's URL/IP
     */
    public $registrar;
    /**
     *
     * @var integer Interval between register attempts. When registration is invalid.
     */
    public $retryInterval;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],            
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['id', 'filter', 'filter' => 'trim'],
            ['id', 'required'],
            ['id', 'unique', 'targetClass' => 'app\models\asterisk\Endpoint', 'message' => 'This id has already been taken.', 'when' =>  function()
            {
                return $this->oldid != $this->id;
            }],
            ['password', 'required'],
            ['allow', 'string'],
            ['registrar', 'string'],
            ['registrar', 'required'],
            ['port','integer'],
            ['retryInterval','integer'],        
            
        ];
        return $rules;
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['username', 'id', 'password', 'allow', 'registrar', 'port', 'retryInterval'];
        $scenarios['create'] = ['username', 'id', 'password', 'allow', 'registrar', 'port', 'retryInterval'];
        $scenarios['view'] = [];
        return $scenarios;
    }
    /**
     * Returns list of all trunks.
     * @return array
     */
    public function listAll()
    {
        $list = [];
        $auth = new Auth();
        $query = new Query();
        $ami = new AGI_AsteriskManager();
        $connected = $ami->connect() ? true : false;
        $query->from('ps_endpoints')->select(['id'])->where(['context' => 'external']);
        $endpoints = $query->all(Yii::$app->dbAsterisk);
        foreach ($endpoints as $row) {
            if($connected)
            {
                $response = $ami->Command("pjsip show registration " . $row['id'])['data'];
                switch (true) {
                    case stripos($response, 'Registered') !== false:
                        $status = 'Registered';
                        break;
                    case stripos($response, 'Rejected') !== false:
                        $status = 'Rejected';
                        break;
                    case stripos($response, 'Unregistered') !== false:
                        $status = 'Unregistered';
                        break;
                    default:
                        $status = 'Unknown';
                        break;
                }                       
            }
            else
            {
                $status = 'Unknown';
            }
            array_push($list, 
                    [
                        'id' => $row['id'],
                        'username' => $auth->getUsername($row['id']),
                        'status' => $status,
                    ]);
        }
        if($connected)
            $ami->disconnect();
        return $list;
    }    
    /**
     * Return ID of a trunk.
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }    
    /**
     * Creates new trunk.
     * @return boolean
     */
    public function create()
    {
        if ($this->validate()) {
            $aor = new Aor();
            $auth = new Auth();
            $endpoint = new Endpoint();
            $file = new pjsipFileInterface();
            $clientUri = $aor->contact = 'sip:'.$this->username.'@'.$this->registrar;            
            
            $aor->id = $auth->id = $this->id;
            $aor->contact = $clientUri.':'.$this->port;
            $aor->save();

            $auth->password = $this->password;
            $auth->username = $this->username;
            $auth->auth_type = 'userpass';
            $auth->save();

            $endpoint->id = $endpoint->aors = $endpoint->outbound_auth = $this->id;
            $endpoint->allow = $this->allow;
            $endpoint->disallow = 'all';
            $endpoint->context = 'external';
            $endpoint->transport = 'transport-udp';
            $endpoint->save();
            
            $registration[] = ['type' => 'registration'];
            $registration[] = ['transport' => 'transport-udp'];
            $registration[] = [ 'outbound_auth' => $this->id];
            $registration[] = ['server_uri' => 'sip:'.$this->registrar];
            $registration[] = ['client_uri' => $clientUri];
            $registration[] = ['retry_interval' => $this->retryInterval];
            
            $identify[] = ['type' => 'identify'];
            $identify[] = ['endpoint' => $this->id];
            $identify[] = ['match' => $this->registrar];
            
            $file->addCategory($this->id, $registration);
            $file->addCategory($this->id, $identify);
            return true;
        }
        return false;
    }
    /**
     * Edits existing trunk.
     * @return boolean
     */
    public function update()
    {
        if ($this->validate()) {
            $aor = Aor::findOne($this->oldid);
            $auth = Auth::findOne($this->oldid);
            $endpoint = Endpoint::findOne($this->oldid);
            $file = new pjsipFileInterface();
            $clientUri = $aor->contact = 'sip:'.$this->username.'@'.$this->registrar;           
            
            $aor->id = $auth->id = $this->id;
            $aor->contact = $clientUri.':'.$this->port;
            $aor->update();
            
            $auth->password = $this->password;
            $auth->username = $this->username;
            $auth->update();
            
            $endpoint->id = $endpoint->aors = $this->id;
            $endpoint->allow = $this->allow;
            $endpoint->disallow = 'all';
            $endpoint->update();
            
            $registration[] = ['type' => 'registration'];
            $registration[] = ['transport' => 'transport-udp'];
            $registration[] = [ 'outbound_auth' => $this->id];
            $registration[] = ['server_uri' => 'sip:'.$this->registrar];
            $registration[] = ['client_uri' => $clientUri];
            $registration[] = ['retry_interval' => $this->retryInterval];
            
            $identify[] = ['type' => 'identify'];
            $identify[] = ['endpoint' => $this->id];
            $identify[] = ['match' => $this->registrar];
            
            $file->deleteCategory($this->oldid);
            $file->addCategory($this->id, $registration);
            $file->addCategory($this->id, $identify);
            
            return true;
        }
        return false;
    }
    /**
     * Initializes trunk from DB.
     */
    public function initialize()
    {        
        $aor = Aor::findOne($this->id);
        $auth = Auth::findOne($this->id);
        $endpoint = Endpoint::findOne($this->id);
        $file = new pjsipFileInterface();      
        $fullUri = explode(':', $aor->contact);
        $this->port = $fullUri[2];
        $serverUri = explode(':', $file->getFromCategory($this->id, 'server_uri'));
        $this->registrar = $serverUri[1];
        $this->retryInterval = $file->getFromCategory($this->id, 'retry_interval');
        $this->username = $auth->username;
        $this->password = $auth->password;
        $this->allow = $endpoint->allow;
    }
    /**
     * Deletes trunk.
     * @return boolean
     */
    public function delete()
    {
        $aor = Aor::findOne(['id' => $this->id]);
        $auth = Auth::findOne(['id' => $this->id]);
        $endpoint = Endpoint::findOne(['id' => $this->id]);
        $file = new pjsipFileInterface();
        if($aor && $auth && $endpoint)
        {
            $aor->delete();
            $auth->delete();                        
            $endpoint->delete();
            $file->deleteCategory($this->id);
            return true;
        }
        return false;
    }
}
