<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\asterisk;

use Yii;
use yii\db\ActiveRecord;
use app\models\asterisk\Endpoint;
use yii\web\IdentityInterface;

class sipUser extends ActiveRecord implements IdentityInterface
{
    public $isSipUser = true;
    /**
     * @inheritdoc
     */
    public static function tableName() 
    {
        return 'ps_auths';
    }    
    /**
     * @inheritdoc
     */
    public static function getDb()
    {
        return Yii::$app->dbAsterisk;
    }   
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return false;
    }
    public function getAuthKey()
    {
        return false;
    }
    public function validateAuthKey($authKey)
    {
        return true;
    }
    
    public function validatePassword($password)            
    {
        if(Endpoint::findOne(['id' => $this->id, 'context' => 'local']) && ($this->password == $password))
                return true;
        return false;
    }
}
