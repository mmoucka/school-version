<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
namespace app\models\dialplan;
use Yii;
use yii\db\Query;
/**
 * Class which is responsible for finding correct path to destination pursuant
 * to informations stored in database and state of endpoints.
 * 
 * @author Martin Moucka <moucka.m@gmail.com>
 */
class routeEngine {
    public function __construct() {
        return true;
    }
    /**
     * Builds regular expression from pattern in Astersik's dialplan format.
     * @param string $pattern
     * @return boolean|string
     */
    protected function buildRegExpession ($pattern)
    {
        $pattern = trim($pattern);
        $p_array = str_split($pattern);
        $tmp = "";
        $expression = "";
        $error = false;
        $wildcard = false;
        $match = $pattern ? true : false;
        $regx_num = "/^\[[0-9]+(\-*[0-9])[0-9]*\]/i";
        $regx_alp = "/^\[[a-z]+(\-*[a-z])[a-z]*\]/i";

        // Try to build a Regular Expression from the dial pattern
        $i = 0;
        while (($i < strlen($pattern)) && (!$error) && ($pattern)) {
            switch (strtolower($p_array[$i])) {
                case '_':
                    if(i > 0)
                    {
                        $error = "You can't use start line character '_' here";
                    }
                    break;
                case 'x':
                    // Match any number between 0 and 9
                    $expression .= $tmp . "[0-9]";
                    $tmp = "";
                    break;
                case '*':
                    // Match *
                    $expression .= $tmp . "\*";
                    $tmp = "";
                    break;
                case 'z':
                    // Match any number between 1 and 9
                    $expression .= $tmp . "[1-9]";
                    $tmp = "";
                    break;
                case 'n':
                    // Match any number between 2 and 9
                    $expression .= $tmp . "[2-9]";
                    $tmp = "";
                    break;
                case '[':
                    // Find out if what's between the brackets is a valid expression.
                    // If so, add it to the regular expression.
                    if (preg_match($regx_num, substr($pattern, $i), $matches) || preg_match($regx_alp, substr(strtolower($pattern), $i), $matches)) {
                        $expression .= $tmp . "" . $matches[0];
                        $i = $i + (strlen($matches[0]) - 1);
                        $tmp = "";
                    } else {
                        $error = "Invalid character class";
                    }
                    break;
                case '.':
                    // Match one or more occurrences of any number
                    if (!$wildcard) {
                        $wildcard = true;
                        $expression .= $tmp . "[0-9]+";
                        $tmp = "";
                    } else {
                        $error = "Cannot have more than one wildcard";
                    }
                    break;
                case '!':
                    // zero or more occurrences of any number
                    if (!$wildcard) {
                        $wildcard = true;
                        $expression .= $tmp . "[0-9]*";
                        $tmp = "";
                    } else {
                        $error = "Cannot have more than one wildcard";
                    }
                    break;
                default:
                    if (preg_match("/[0-9]/i", strtoupper($p_array[$i]))) {
                        $tmp .= strtoupper($p_array[$i]);
                    } else {
                        $error = "Invalid character '" . $p_array[$i] . "' in pattern";
                    }
            }
            $i++;
        }
        $expression .= $tmp;
        $tmp = "";
        if($error)
            return false;
        return $expression;
    }
    /**
     * Decides if called extensions matches pattern of a route.
     * @param string $pattern
     * @param string $extension
     * @return boolean
     */
    protected function matchPattern($pattern, $extension)
    {
        if($regExpression = $this->buildRegExpession($pattern))
        {
            if (preg_match("/^" . $regExpression . "$/i", $extension)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Searching suitable path for called extension
     * @param integer $type @see RouteForm
     * @param string $extension
     * @return array|boolean
     */
    public function findPath($type, $extension)
    {
        $query = new Query();
        $query->from('dialplan')->where(['type' => $type])->orderBy('metric');
        $result = $query->all(Yii::$app->dbAsterisk);
        foreach ($result as $value) {
            if($this->matchPattern($value['pattern'], $extension))
                return $value;
        }
        return false;
    }
    /**
     * Returns name of a next queue for the present queue if has some.
     * @param string $queue
     * @return string|boolean
     */
    public function getNextQueue($queue)
    {
        $query = new Query();
        $query->from('queues')->where(['name' => $queue]);
        $result = $query->one(Yii::$app->dbAsterisk);
        if($result)
        {
            return $result['nextqueue'];
        }
        return false;
    }
    /**
     * Stores IVR in a cache and returns it.
     * @param string $name
     * @return array
     */
    public function cacheIVR($name)
    {
        $ivr = [];
        $keys = '';
        $query = new Query();        
        $query->from('applications')->where(['appname' => $name])->orderBy('action');
        $result = $query->all(Yii::$app->dbAsterisk);
        foreach ($result as $value) {            
            $action = explode(',', $value['action']);
            if (!strcmp($action[0], 'intro')) {
                $ivr['intro'] = explode(',', $value['data'])[1];
            } 
            else if (!strcmp($action[0], 'waitfordigit')) {
                $data = explode(',', $value['data']);
                if(!strcmp($data[0], 'execute'))
                {
                    $query = new Query();
                    $query->select('data')->from('applications')->where(['appname' => $data[1]]);
                    $result = $query->one(Yii::$app->dbAsterisk);
                    $ivr[$action[1]] = $result['data'];
                    $keys .= $action[1];
                }
                else
                {
                    $ivr[$action[1]] = $value['data'];
                    $keys .= $action[1];
                }
            }
        }
        if($ivr)
            $ivr['keys'] = $keys;
        Yii::$app->cache->set('IVR'.$name, $ivr);
        return $ivr;
    }
    /**
     * Returns IVR's menu from cache or from database.
     * @param string $name
     * @return boolean|array
     */
    public function getIvrMenu($name)
    {
        if($ivr = Yii::$app->cache->get('IVR'.$name))
        {
            return $ivr;
        }
        else
        {
            return $this->cacheIVR($name);
        }
        return false;
    }
}
