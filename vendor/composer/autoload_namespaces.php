<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'HTMLPurifier' => array($vendorDir . '/ezyang/htmlpurifier/library'),
    'Faker\\PHPUnit' => array($vendorDir . '/fzaninotto/faker/test'),
    'Faker' => array($vendorDir . '/fzaninotto/faker/src'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
    'Devristo\\Phpws\\' => array($vendorDir . '/devristo/phpws/src'),
);
