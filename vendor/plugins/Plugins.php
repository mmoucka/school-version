<?php

/**
 * Description of xmlRepository
 *
 * @author Lukas
 */
class Plugins {

    private $xmlFile = './plugins.xml';

    private function getFile() {
        return $this->xmlFile;
    }

    public function setFile($xmlFile) {

        if (file_exists($xmlFile)) {
            $this->xmlFile = $xmlFile;
        }
    }

    /*
     * paste new plugin info into XML file
     * 
     * @param type $content array of new plugin [menuName, mainPage, pathToPluginFolder]
     * @return 0 if fails 
     */

    public function create($content) {
        if ($content[0] == NULL || $content[1] == NULL || $content[2] == NULL) {
            return 0;
        }
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $newItem = $xml->createElement('plugin');
            $newItem->appendChild($xml->createElement('menuName', $content[0]));
            $newItem->appendChild($xml->createElement('mainPage', $content[1]));
            $newItem->appendChild($xml->createElement('pathToPluginFolder', $content[2]));
            $xml->getElementsByTagName('plugins')->item(0)->appendChild($newItem);
            $xml->save($this->getFile());
            return;
        } return 0;
    }

    /*
     * delete plugin out of xml file by given menuName
     * 
     * @param type $plugin menuName of plugin to delete
     * @return 0 if fails 
     */

    public function delete($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('menuName')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    $value->parentNode->removeChild($value);
                    return $xml->save($this->getFile());
                }
            }
        } return 0;
    }

    /*
     * return array of plugins menuNames
     * 
     * @return 0 if fails 
     */

    public function getNames() {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $return = $value->getElementsByTagName('menuName')->item(0);
                $return = $return->nodeValue;
                $returns[] = $return;
            }
            return $returns;
        } return 0;
    }

    /*
     * return array of plugins mainPages
     * 
     * @return 0 if fails 
     */

    public function getMainPages() {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $return = $value->getElementsByTagName('mainPage')->item(0);
                $return = $return->nodeValue;
                $returns[] = $return;
            }
            return $returns;
        } return 0;
    }

    /*
     * return array of plugins folder paths
     * 
     * @return 0 if fails 
     */

    public function getPaths() {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $return = $value->getElementsByTagName('pathToPluginFolder')->item(0);
                $return = $return->nodeValue;
                $returns[] = $return;
            }
            return $returns;
        } return 0;
    }

    public function getMenuNames($names, $paths, $mainFiles) {
        $menuString = array();
        for ($index = 0; $index < count($names); $index++) {
            $menuString = ['label' => $names[$index], 'url' => [$paths[$index] . "/" . $mainFiles[$index]]];
            $menuStrings[$index] = $menuString;
        }
        if ($menuString != null) {
            return $menuStrings;
        } else
            return NULL;
    }

}
