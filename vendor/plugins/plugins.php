<?php

/**
 * Description of xmlRepository
 *
 * @author Lukas
 */
class Plugins {

    private $xmlFile = './plugins.xml';

    private function getFile() {
        return $this->xmlFile;
    }

    public function setFile($xmlFile) {

        if (file_exists($xmlFile)) {
            $this->xmlFile = $xmlFile;
        }
    }

    /*
     * paste new plugin info into XML file
     * 
     * @param type $content array of new plugin [menuName, mainPage, pathToPluginFolder]
     * @return 0 if fails 
     */

    public function create($module, $name, $path, $mainFile, $description, $files = array(), $permissions = array(), $db = array()) {
        if ($name == NULL || $path == NULL || $mainFile == NULL) {
            return NULL;
        }
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $newItem = $xml->createElement('plugin');
            $newItem->appendChild($xml->createElement('menuName', $name));
            $newItem->appendChild($xml->createElement('mainPage', $mainFile));
            $newItem->appendChild($xml->createElement('pathToPluginFolder', $path));
            $newItem->appendChild($xml->createElement('description', $description));
            $newItem->appendChild($xml->createElement('module', $module));
            foreach ($files as $file) {
                $newItem->appendChild($xml->createElement('pluginFile', $file));
            }
            foreach ($permissions as $permission => $key) {
                $newItem->appendChild($xml->createElement('permission', $permission));
            }
            $newItem->appendChild($xml->createElement('database', $db[0]));
            $newItem->appendChild($xml->createElement('table', $db[1]));
            $xml->getElementsByTagName('plugins')->item(0)->appendChild($newItem);
            $xml->save($this->getFile());
            return;
        } return 0;
    }

    /*
     * delete plugin out of xml file by given menuName
     * 
     * @param type $plugin menuName of plugin to delete
     * @return 0 if fails 
     */

    public function delete($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('pathToPluginFolder')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    $value->parentNode->removeChild($value);
                    return $xml->save($this->getFile());
                }
            }
        } return 0;
    }

    /*
     * return array of plugins menuNames
     * 
     * @return 0 if fails 
     */

    public function getNames($module) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    $return = $value->getElementsByTagName('menuName')->item(0);
                    $return = $return->nodeValue;
                    $returns[] = $return;
                }
            }
            return $returns;
        } return 0;
    }

    public function getNames2() {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $return = $value->getElementsByTagName('pathToPluginFolder')->item(0);
                $return = $return->nodeValue;
                $returns[$return]['name'] = $return;
            }
            return $returns;
        } return 0;
    }

    /*
     * return array of plugins mainPages
     * 
     * @return 0 if fails 
     */

    public function getMainPages($module) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    $return = $value->getElementsByTagName('mainPage')->item(0);
                    $return = $return->nodeValue;
                    $returns[] = $return;
                }
            }
            return $returns;
        } return 0;
    }

    /*
     * return array of plugins folder paths
     * 
     * @return 0 if fails 
     */

    public function getPaths($module) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    $return = $value->getElementsByTagName('pathToPluginFolder')->item(0);
                    $return = $return->nodeValue;
                    $returns[] = $return;
                }
            }
            return $returns;
        } return 0;
    }

    public function getMenuNames($names, $paths, $mainFiles) {
        $menuString = array();
        for ($index = 0; $index < count($names); $index++) {
            $this->getPermissions($names[$index]);
            $menuString = ['label' => $names[$index], 'url' => [$paths[$index] . "/" . $mainFiles[$index]]];
            $menuStrings[$index] = $menuString;
        }
        if ($menuString != null) {
            return $menuStrings;
        } else
            return NULL;
    }

    public function getId() {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            $return = 0;
            foreach ($element as $value) {
                $return++;
            }
            $return++;
            return $return;
        } return 0;
    }

    public function getDescription($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('menuName')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    $desc_r = $value->getElementsByTagName('description')->item(0);
                    $return = $desc_r->nodeValue;
                    return $return;
                }
            }
        } return 0;
    }

    public function getFilePaths($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('pathToPluginFolder')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    $desc_r = $value->getElementsByTagName('pluginFile');
                    $return = array();
                    foreach ($desc_r as $fajl) {
                        $return[] = $fajl->nodeValue;
                    }
                    return $return;
                }
            }
        } return 0;
    }

    public function getPermissions($module) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if ($xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            if ($element == NULL){
                return NULL;
            }
            $return = array();
            $i = 0;
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    $plugName = $value->getElementsByTagName('pathToPluginFolder')->item(0);
                    $desc_r = $value->getElementsByTagName('permission');
                    
                    foreach ($desc_r as $fajl) {
                        $return[$i][] = $plugName->nodeValue."_".$fajl->nodeValue;
                    }
                    $i++;
                }
            }
            return $return;
        } return 0;
    }

}
