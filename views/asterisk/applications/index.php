<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;

$this->title = 'Applications';
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
$this->registerCssFile("/vendor/nice-tree/css/easyTree.css");
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">     
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                        <h4 class="panel-title">
                            IVRs
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse <?=  isset($tree) ? 'in': ''?>">
                        <div class="panel-body">
                            <?php
                            echo GridView::widget([
                                'dataProvider' => $ivrs,
                                'columns' => [
                                    'appname:text:Application Name',
                                    'description',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'controller' => 'asterisk/applications',
                                        'template' => '{viewivr}{updateivr}{delete}',
                                        'buttons' => [
                                            'viewivr' => function ($url, $model, $key) {
                                                return Html::a('<span class="glyphicon glyphicon-eye-open" style="margin-right: 2%"></span>', $url, ['title' => 'View', 'data-pjax' => '0']);
                                                },
                                             'updateivr' => function ($url, $model, $key) {
                                                return Html::a('<span class="glyphicon glyphicon-pencil" style="margin-right: 2%"></span>', $url, ['title' => 'Update', 'data-pjax' => '0']);
                                                },
                                        ],
                                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' =>
                                            'js:document.location.href="' . Url::toRoute('asterisk/applications/createivr') . '"']),
                                    ],
                                ],
                                'showFooter' => true,
                                'id' => 'username',
                            ]);
                            if ($tree) {
                                echo '<div class="panel panel-default">
                                <div class="panel-heading"><b>"' . $tree['title'] . '" tree</b></div>
                                <div class="panel-body" >
                                <div class="easy-tree">';
                                echo $tree['data'];
                                echo '</div>
                                    </div>
                                </div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                        <h4 class="panel-title">
                            Special applications
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?php
                            echo GridView::widget([
                                'dataProvider' => $specials,
                                'columns' => [
                                    'appname:text:Application Name',
                                    'description',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'controller' => 'asterisk/applications',
                                        'template' => '{viewspecial}{updatespecial}{delete}',
                                        'buttons' => [
                                            'viewspecial' => function ($url, $model, $key) {
                                                return Html::a('<span class="glyphicon glyphicon-eye-open" style="margin-right: 2%"></span>', $url, ['title' => 'View', 'data-pjax' => '0']);
                                                },
                                             'updatespecial' => function ($url, $model, $key) {
                                                return Html::a('<span class="glyphicon glyphicon-pencil" style="margin-right: 2%"></span>', $url, ['title' => 'Update', 'data-pjax' => '0']);
                                                },
                                        ],
                                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' =>
                                            'js:document.location.href="' . Url::toRoute('asterisk/applications/createspecial') . '"']),
                                    ],
                                ],
                                'showFooter' => true,
                                'id' => 'username',
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
