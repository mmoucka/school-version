<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\widgets\Menu;

$this->title = 'Update ' . $model->appname;
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Applications',
    $this->title,
];

?>
<div class="site-updateapplication">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to edit the application:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-updateapplication']); ?>
                <?= $form->field($model, 'appname')->label('Application name') ?>
                <?= $form->field($model, 'description') ?>
                <?= $form->field($model, 'data[app]')->widget(AutoComplete::className(), ['clientOptions' => ['source' => $apps], 'options' => ['class' => 'form-control']])->label('Dialplan application') ?>
                <?= $form->field($model, 'data[options]', ['enableClientValidation' => false])->label('Options') ?>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>