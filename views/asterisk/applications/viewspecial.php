<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\AutoComplete;
use yii\widgets\Menu;

$this->title = 'View ' . $model->appname;
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Applications',
    $this->title,
];

?>
<div class="site-updateapplication">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'form-updateapplication']); ?>
                <?= $form->field($model, 'appname',['inputOptions' => ['readonly' => true]])->label('Application name') ?>
                <?= $form->field($model, 'description',['inputOptions' => ['readonly' => true]]) ?>
                <?= $form->field($model, 'data[app]')->widget(AutoComplete::className(), ['clientOptions' => ['source' => $apps], 'options' => ['class' => 'form-control', 'disabled' => true]])->label('Dialplan application') ?>
                <?= $form->field($model, 'data[options]', ['enableClientValidation' => false, 'inputOptions' => ['readonly' => true]])->label('Options') ?>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>