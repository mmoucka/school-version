<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;

$this->title = 'Dialouts';
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-9">            
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-info">
                        <div class="panel-heading"><b>Process control</b></div>
                        <div class="panel-body" >
                            <?php
                            echo "<label>Status: </label> $status <br>";
                            echo Html::a('Stop', 'stop', ['type' => 'button', 'Stop', 'class' => 'btn btn-danger', 'name' => 'add-button',
                                'data-confirm' => 'Are you sure you want to stop dialout\'s proccess?']) . " ";
                            echo Html::a('Start', 'start', ['type' => 'button', 'Start', 'class' => 'btn btn-success', 'name' => 'add-button']);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                echo GridView::widget([
                'dataProvider' => $list,
                'layout' => '{items}{pager}',
                'columns' => [
                    'number',
                    'destqueue:text:Destination Queue',
                    'trunk',
                    'callerid',
                    'attempts',
                    'status',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'asterisk/dialouts',
                        'template' => '{update} {delete} {restartstatus}',
                        'buttons' =>
                        [
                            'restartstatus' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-repeat"></span>', $url, ['title' => 'Restart Status']);
                            },
                        ],
                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' => 
                            'js:document.location.href="'. Url::toRoute('asterisk/dialouts/add').'"']) . ' ' .
                                    Html::button('Import', ['class' => 'btn btn-success', 'name' => 'add-button', 'onclick' => 
                            'js:document.location.href="'. Url::toRoute('asterisk/dialouts/import').'"'])
                    ],                    
                ],
                'showFooter' => true,
                'id' => 'id',
            ]);                
            ?>
        </div>
    </div>
</div>
