<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;

$this->title = 'Add Emergency number';
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Emergency Numbers',
    $this->title,
];
?>
<div class="site-createroute">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <?php $form = ActiveForm::begin(['id' => 'form-createenum']); ?>
                <?= $form->field($model, 'pattern')->label('Number/Pattern') ?>
                <div class="form-group field-routeform-destination">
                <label class="control-label" for="routeform-destination">Destination</label>
                <?= Html::dropDownList('RouteForm[data]', [], $destinations, ['id' => 'routeform-destination', 'class' => 'form-control']) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Add', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>