<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\sortinput\SortableInput;
use yii\widgets\Menu;

$this->title = 'Emergency Numbers';
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <?php
            $form = ActiveForm::begin(['id' => 'form-enums']);
            echo SortableInput::widget([
                'name' => 'numbers',
                'items' => $numbers,
                'hideInput' => true,
                'sortableOptions' => [
                    'itemOptions' => ['aria-grabbed' => 'false',
                        'class' => 'item',
                        'ondblclick' => 'itemClicked(this)',
                    ],
                ],
                'options' => ['class' => 'form-control', 'readonly' => true]
            ]);
            echo Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'save-button']);
            echo " ";
            echo Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' => 'js:document.location.href="'
                . Url::toRoute('asterisk/emergencynums/create') . '"']);
            ActiveForm::end();
            $this->registerJs('function itemClicked(target){
            var url = " '. Url::toRoute('asterisk/emergencynums/update') . '"; 
            var id = "?id=";
            var sender = target.getAttribute("data-key");
            location.replace(url.concat(id, sender)); 
            }', $this::POS_END);
            ?>
        </div>        
    </div>
</div>