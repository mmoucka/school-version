<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use yii\bootstrap\Collapse;
use yii\helpers\Inflector;
use kartik\sortinput\SortableInput;
use yii\widgets\MaskedInput;

$this->title = 'Create Extension';
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Extensions',
    $this->title,
];
?>
<div class="site-createextension">
    <div class="row">
        <div class="col-lg-3">
            <?php
               echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to add new extension:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-createextension']); ?>
                <?= $form->field($model, 'id') ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password') ?>
                <label>Codecs</label>
                <div class="row">                    
                    <div class="col-sm-6">                        
                        <?=$form->field($model, 'allow')->widget(SortableInput::classname(), [
                                'items' => [],
                                'hideInput' => true,
                                'sortableOptions' => [
                                    'connected' => 'codecs',
                                ],
                                'options' => ['class'=>'form-control', 'readonly'=>true]
                            ])->label('Allow:');?>
                    </div>
                    <div class="col-sm-6">
                    <?php
                        echo Html::label('Available codecs', 'available');
                        echo SortableInput::widget([
                            'name' => 'available',
                            'items' => $codecs,
                            'hideInput' => true,
                            'sortableOptions' => [
                                'connected' => 'codecs',
                            ],
                            'options' => ['class' => 'form-control', 'readonly' => true]
                        ]);
                        $advancedlist = '';
                        foreach ($advancedNames as $value) {
                            $type = substr($schema->columns[$value]->dbType, 0, strpos($schema->columns[$value]->dbType, '('));
                            if (!strcmp($type, 'enum')) {
                                $options = [];
                                foreach ($schema->columns[$value]->enumValues as $key) {
                                    $options[$key] = $key;
                                }
                                $model->$value = $schema->columns[$value]->defaultValue;
                                $advancedlist .= $form->field($model, $value)->dropDownList($options, ['prompt' => 'None'])->label(Inflector::camel2words($value, true));
                            } else if (!strcmp($type, 'int') || (!strcmp($type, 'varchar') || !strcmp($type, 'text'))) {
                                $model->$value = $schema->columns[$value]->defaultValue;
                                $advancedlist .= $form->field($model, $value)->label(Inflector::camel2words($value, true));
                            }
                        }
                    ?>
                    </div>
                </div>
                <?= $form->field($model, 'max_contacts')->label('Maximum Registrations')?>
                <?= $form->field($model, 'mac')->label('Fill MAC address to generate config') ?>
                <?= Collapse::widget([
                    'items' => [
                        [
                            'label' => 'Advanced',
                            'content' => $advancedlist,
                            'contentOptions' => []
                        ],
                    ]
                    ]); ?>
                <div class="form-group">
                    <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>