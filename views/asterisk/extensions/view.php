<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use yii\bootstrap\Collapse;
use kartik\sortinput\SortableInput;

$this->title = $model->id;
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Extensions',
    $this->title,
];
?>
<div class="site-viewextension">
    <div class="row">
        <div class="col-lg-3">
            <?php
               echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px">View <?= Html::encode($this->title) ?></h1>
            
            <?php $form = ActiveForm::begin(['id' => 'form-viewextension']); ?>
                <?= $form->field($model, 'id', ['inputOptions' => ['readonly' => true]]) ?>
                <?= $form->field($model, 'username', ['inputOptions' => ['readonly' => true]]) ?>
                <?= $form->field($model, 'password', ['inputOptions' => ['readonly' => true]]) ?>
                <label>Codecs</label>
                <div class="row">                    
                    <div class="col-sm-6">                        
                        <?php
                        echo Html::label('Allowed codecs', 'allowed');
                        echo SortableInput::widget([
                            'name' => 'allowed',
                            'items' => $allowed,
                            'hideInput' => true,
                            'sortableOptions' => [
                                'itemOptions' => ['aria-grabbed' => 'false'],
                            ],
                            'options' => ['class' => 'form-control', 'readonly' => true]
                        ]);
                        $advancedlist = '';
                        foreach ($advancedNames as $value) {
                            $type = substr($schema->columns[$value]->dbType, 0, strpos($schema->columns[$value]->dbType, '('));
                            if (!strcmp($type, 'enum')) {
                                $options = [];
                                $options[NULL] = NULL;
                                foreach ($schema->columns[$value]->enumValues as $key) {
                                    $options[$key] = $key;
                                }
                                $advancedlist .=  Html::label($value, 'extensionform-' . $value);
                                $advancedlist .=  Html::dropDownList('advanced[' . $value . ']', $advanced[$value],
                                        $options, ['id' => 'extensionform-' . $value, 'class' => 'form-control', 'disabled' => true]);
                            } else if (!strcmp($type, 'int')) {
                                $advancedlist .=  Html::label($value . ' [' . $schema->columns[$value]->type . ']', 'extensionform-' . $value);
                                $advancedlist .=  Html::textInput('advanced[' . $value . ']', $advanced[$value],
                                        ['id' => 'extensionform-' . $value, 'class' => 'form-control', 'disabled' => true]);
                            } else if (!strcmp($type, 'varchar') || !strcmp($type, 'text')) {

                                $advancedlist .=  Html::label($value . ' [' . $schema->columns[$value]->type . ']', 'extensionform-' . $value);
                                $advancedlist .=  Html::textInput('advanced[' . $value . ']', $advanced[$value],
                                        ['id' => 'extensionform-' . $value, 'class' => 'form-control', 'disabled' => true]);
                            }
                        }
                        ?>
                    </div>
                </div>
                <?= $form->field($model, 'max_contacts', ['inputOptions' => ['readonly' => true]])->label('Maximum Registrations')?>
                <?= $form->field($model, 'mac', ['inputOptions' => ['readonly' => true]])->label('Fill MAC address to generate config') ?>
                <?= Collapse::widget([
                    'items' => [
                        [
                            'label' => 'Advanced',
                            'content' => $advancedlist,
                            'contentOptions' => []
                        ],
                    ]
                    ]); ?>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>