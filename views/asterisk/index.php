<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="row">
        <div class="col-xs-3">
                <?php
                    echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-9">
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading"><b>Asterisk information</b></div>
                    <div class="panel-body" >
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $information,
                            'tableOptions' => ['class' => 'table'],
                            'layout' => '{items}',
                            'showHeader' => false,
                            'columns' => [
                                'key',
                                'value',
                            ],
                            'id' => 'id',
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading"><b>Statistics</b></div>
                    <div class="panel-body" >
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $statistics,
                            'tableOptions' => ['class' => 'table'],
                            'layout' => '{items}',
                            'showHeader' => false,
                            'columns' => [
                                'key',
                                'value',
                            ],
                            'id' => 'id',
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><b>Active calls</b></div>
                    <div class="panel-body" >
                        <?php
                        Pjax::begin(['id' => 'reloadGrid']);
                        echo GridView::widget([
                            'dataProvider' => $calls,
                            'columns' => [
                                'from',
                                'to',
                                'state',
                                [
                                    'attribute' => 'duration',
                                    'format' => ['date', 'HH:mm:ss'],
                                ],
                                [
                                    'template' => '{spy} {hangup}',
                                    'class' => 'yii\grid\ActionColumn',
                                    'controller' => 'asterisk',
                                    'buttons' => [
                                        'spy' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-earphone" style="margin-right: 5%"></span>', $url, ['title' => 'Spy']);
                                        },
                                                'hangup' => function ($url, $model, $key) {
                                            return Html::a('<span class="glyphicon glyphicon-off" style="margin-right: 5%"></span>', $url, ['title' => 'Hangup']);
                                        },
                                            ],
                                        ],
                                    ],
                                    'id' => 'channel_id',
                                ]);
                                Pjax::end();
                                $this->registerJs('function refresh() {
                                        $.pjax.reload({container:"#reloadGrid"});
                                        setTimeout(refresh, 15000);
                                    }
                                    refresh();');
                                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
