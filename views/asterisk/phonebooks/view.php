<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\widgets\Menu;
use kartik\grid\GridView;

$this->title = 'Phone Book - ' . $model->auth;
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        
        <div class="col-lg-9">
            <?php
            $columns = [
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'hiddenFromExport' => true,
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'header' => 'Name',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'number',
                    'hiddenFromExport' => true,
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'header' => 'Number',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'description',
                    'hiddenFromExport' => true,
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'header' => 'Description',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'attribute' => 'name',
                    'hidden' => true,
                    'filter' => false,
                    
                ],
                [
                    'attribute' => 'number',
                    'hidden' => true,
                    'filter' => false,
                ],
                [
                    'attribute' => 'description',
                    'hidden' => true,
                    'filter' => false,
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',                    
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash""></span>', $url, ['title' => 'Delete', 'data-confirm' => 'Are you sure you want to delete this item?']);
                        },
                    ],
            ]
            ];
            echo GridView::widget([
                'dataProvider' => $list,
                'filterModel' => $searchModel,
                'columns' => $columns,
                'toolbar' => [
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                    'target' => GridView::TARGET_SELF,       
                ],
                'exportConfig' => [
                    GridView::CSV => [
                        'label' => 'CSV',
                        'icon' => 'floppy-open',
                        'iconOptions' => ['class' => 'text-primary'],
                        'showHeader' => false,
                        'showPageSummary' => false,
                        'showFooter' => false,
                        'showCaption' => false,
                        'filename' => 'phonebook',
                        'alertMsg' => 'The CSV export file will be generated for download.',
                        'options' => ['title' => 'Comma Separated Values'],
                        'mime' => 'application/csv',
                    ],
                ],                
                'panel' => [
                    'type' => GridView::TYPE_INFO,
                    'heading' => '<b>' . $this->title .'</b>',
                ],
                'responsive' => true,
                'hover' => true
            ]);
            ?>
        </div>
    </div>
</div>