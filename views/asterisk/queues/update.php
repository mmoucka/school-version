<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use yii\bootstrap\Collapse;
use kartik\sortinput\SortableInput;

$this->title = 'Update '.$model->name;
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Queues',
    $this->title,
];
?>
<div class="site-updatequeue">
    <div class="row">
        <div class="col-lg-3">
            <?php
               echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to add new queue:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-updatequeue']); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'musiconhold')->label('Music On Hold')->dropDownList(['default' => 'default', 'custom' => 'custom']) ?>
                <?= $form->field($model, 'announce')->dropDownList($records, ['prompt' => 'none']) ?>
                <?= $form->field($model, 'strategy')->dropDownList(['ringall','leastrecent','fewestcalls','random','rrmemory','linear','wrandom','rrordered']) ?>
                <?= $form->field($model, 'joinempty')->dropDownList(['yes', 'no'])->label('Join Empty') ?>
                <?= $form->field($model, 'leavewhenempty')->dropDownList(['yes', 'no'])->label('Leave When Empty') ?>
                <?= $form->field($model, 'retry') ?>
                <?= $form->field($model, 'maxlen', ['inputOptions' => ['value' => '0']])->label('Max Calls') ?>
                <?= $form->field($model, 'timeout') ?>
                <?= $form->field($model, 'nextqueue')->dropDownList($queues, ['prompt' => 'none'])->label('Next Queue') ?>
                <div class="row">                    
                    <div class="col-sm-6">                        
                        <?=$form->field($model, 'members')->widget(SortableInput::classname(), [
                                'items' => $extensions,
                                'hideInput' => true,
                                'sortableOptions' => [
                                    'connected' => 'members',
                                ],
                                'options' => ['class'=>'form-control', 'readonly'=>true]
                            ]);?>
                    </div>
                    <div class="col-sm-6">
                    <?php
                        foreach (explode(',', $model->members) as $value) {
                            unset($extensions[$value]);
                        }
                        echo Html::label('Available members', 'available');
                        echo SortableInput::widget([
                            'name' => 'available',
                            'items' => $extensions,
                            'hideInput' => true,
                            'sortableOptions' => [
                                'connected' => 'members',
                            ],
                            'options' => ['class' => 'form-control', 'readonly' => true]
                        ]);
                    ?>
                    </div>
                </div>
                <?php
                    $advanced = $form->field($model, 'ringinuse')->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'announce_frequency');
                    $advanced .= $form->field($model, 'announce_to_first_user')->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'announce_frequency');
                    $advanced .= $form->field($model, 'announce_round_seconds');
                    $advanced .= $form->field($model, 'announce_holdtime')->dropDownList(['no', 'yes', 'once']);
                    $advanced .= $form->field($model, 'announce_position')->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'announce_position_limit');
                    $advanced .= $form->field($model, 'periodic_announce_frequency');
                    $advanced .= $form->field($model, 'relative_periodic_announce');
                    $advanced .= $form->field($model, 'random_periodic_announce')->dropDownList(['no','yes']);
                    foreach ($withaudio as $value)
                    {
                        $advanced .= $form->field($model, $value)->dropDownList($records, ['prompt' => 'none']);
                    }
                    $advanced .= $form->field($model, 'wrapuptime');
                    $advanced .= $form->field($model, 'autofill')->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'autopause')->dropDownList(['no','yes', 'all']);
                    $advanced .= $form->field($model, 'autopausedelay')->label('Autopause Delay');
                    $advanced .= $form->field($model, 'autopausebusy')->dropDownList(['no','yes'])->label('Autopause Busy');
                    $advanced .= $form->field($model, 'autopauseunavail')->dropDownList(['no','yes'])->label('Autopause Unavailable');
                    $advanced .= $form->field($model, 'reportholdtime')->dropDownList(['no','yes'])->label('Report Holdtime');
                    $advanced .= $form->field($model, 'memberdelay')->label('Memer Delay');
                    echo Collapse::widget([
                    'items' => [
                        [
                            'label' => 'Advanced',
                            'content' => $advanced,
                            'contentOptions' => []
                        ],
                    ]
                    ]);
                ?>
                <div class="form-group">
                    <?= Html::submitButton('update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>