<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use yii\bootstrap\Collapse;
use kartik\sortinput\SortableInput;

$this->title = 'View '.$model->name;
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Queues',
    $this->title,
];
?>
<div class="site-viewqueue">
    <div class="row">
        <div class="col-lg-3">
            <?php
               echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to add new queue:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-viewqueue']); ?>
                <?= $form->field($model, 'name', ['inputOptions' => ['readonly' => true]]) ?>
                <?= $form->field($model, 'musiconhold', ['inputOptions' => ['readonly' => true]])->label('Music On Hold')->dropDownList(['default' => 'default', 'custom' => 'custom']) ?>
                <?= $form->field($model, 'announce', ['inputOptions' => ['readonly' => true]])->dropDownList($records, ['prompt' => 'none']) ?>
                <?= $form->field($model, 'strategy', ['inputOptions' => ['readonly' => true]])->dropDownList(['ringall','leastrecent','fewestcalls','random','rrmemory','linear','wrandom','rrordered']) ?>
                <?= $form->field($model, 'joinempty', ['inputOptions' => ['readonly' => true]])->dropDownList(['yes', 'no'])->label('Join Empty') ?>
                <?= $form->field($model, 'leavewhenempty', ['inputOptions' => ['readonly' => true]])->dropDownList(['yes', 'no'])->label('Leave When Empty') ?>
                <?= $form->field($model, 'retry', ['inputOptions' => ['readonly' => true]]) ?>
                <?= $form->field($model, 'maxlen', ['inputOptions' => ['value' => '0', 'readonly' => true]])->label('Max Calls') ?>
                <?= $form->field($model, 'timeout',['inputOptions' => ['readonly' => true]]) ?>
                <?= $form->field($model, 'nextqueue', ['inputOptions' => ['readonly' => true]])->dropDownList($queues, ['prompt' => 'none'])->label('Next Queue') ?>
                <label>Members</label>
                <div class="row">                    
                    <div class="col-sm-6">                        
                        <?=$form->field($model, 'members')->widget(SortableInput::classname(), [
                                'items' => $extensions,
                                'hideInput' => true,
                                'sortableOptions' => [
                                    'connected' => 'members',
                                ],
                                'options' => ['class'=>'form-control', 'readonly'=>true]
                            ])->label(false);?>
                    </div>
                </div>
                <?php
                    $advanced = $form->field($model, 'ringinuse', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'announce_frequency', ['inputOptions' => ['readonly' => true]]);
                    $advanced .= $form->field($model, 'announce_to_first_user', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'announce_frequency', ['inputOptions' => ['readonly' => true]]);
                    $advanced .= $form->field($model, 'announce_round_seconds', ['inputOptions' => ['readonly' => true]]);
                    $advanced .= $form->field($model, 'announce_holdtime', ['inputOptions' => ['readonly' => true]])->dropDownList(['no', 'yes', 'once']);
                    $advanced .= $form->field($model, 'announce_position', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'announce_position_limit', ['inputOptions' => ['readonly' => true]]);
                    $advanced .= $form->field($model, 'periodic_announce_frequency', ['inputOptions' => ['readonly' => true]]);
                    $advanced .= $form->field($model, 'relative_periodic_announce', ['inputOptions' => ['readonly' => true]]);
                    $advanced .= $form->field($model, 'random_periodic_announce', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes']);
                    foreach ($withaudio as $value)
                    {
                        $advanced .= $form->field($model, $value, ['inputOptions' => ['readonly' => true]])->dropDownList($records, ['prompt' => 'none']);
                    }
                    $advanced .= $form->field($model, 'wrapuptime', ['inputOptions' => ['readonly' => true]]);
                    $advanced .= $form->field($model, 'autofill', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes']);
                    $advanced .= $form->field($model, 'autopause', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes', 'all']);
                    $advanced .= $form->field($model, 'autopausedelay', ['inputOptions' => ['readonly' => true]])->label('Autopause Delay');
                    $advanced .= $form->field($model, 'autopausebusy', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes'])->label('Autopause Busy');
                    $advanced .= $form->field($model, 'autopauseunavail', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes'])->label('Autopause Unavailable');
                    $advanced .= $form->field($model, 'reportholdtime', ['inputOptions' => ['readonly' => true]])->dropDownList(['no','yes'])->label('Report Holdtime');
                    $advanced .= $form->field($model, 'memberdelay', ['inputOptions' => ['readonly' => true]])->label('Memer Delay');
                    echo Collapse::widget([
                    'items' => [
                        [
                            'label' => 'Advanced',
                            'content' => $advanced,
                            'contentOptions' => []
                        ],
                    ]
                    ]);
                ?>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>