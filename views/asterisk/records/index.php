<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;

$this->title = 'Records';
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <?php
            echo Html::label('Records');
            echo GridView::widget([
                'dataProvider' => $list,
                'columns' => [
                    'id:text:Filename', 
                    'size:text:Size [kB]',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'asterisk/records',       
                        'template' => '{download} {delete}',
                        'buttons' => [
                            'delete',
                            'download' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-download-alt" style="margin-right: 5%"></span>', $url, ['title' => 'Download']);
                            },
                            ],
                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' => 
                            'js:document.location.href="'. Url::toRoute('asterisk/records/upload').'"']),
                    ],                    
                ],
                'showFooter' => true,
                'id' => 'filename',
            ]);
            echo Html::label('Music On Hold');
            echo GridView::widget([
            'dataProvider' => $mohs,
            'columns' => [
                'id:text:Filename',
                'size:text:Size [kB]',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'asterisk/records',
                    'template' => '{downloadmoh} {deletemoh}',
                    'buttons' => [
                        'deletemoh'=> function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash" style="margin-right: 5%"></span>', $url, ['title' => 'Delete', 
                                'data-confirm' => 'Are you sure you want to delete this item?']);
                        },                        
                        'downloadmoh' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-download-alt" style="margin-right: 5%"></span>', $url, ['title' => 'Download']);
                        },
                            ],
                            'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' =>
                                'js:document.location.href="' . Url::toRoute('asterisk/records/uploadmoh') . '"']),
                        ],
                    ],
                    'showFooter' => true,
                    'id' => 'filename',
            ]);
            ?>
        </div>
    </div>
</div>
