<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use yii\helpers\Url;

$this->title = $model->pattern;
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Routes',
    $this->title,
];
?>
<div class="site-updateroute">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to update route:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-updateroute']); ?>
                <?= $form->field($model, 'pattern') ?>
                <div class="form-group field-routeform-destination">
                <label class="control-label" for="routeform-destination">Destination</label>
                <?= Html::dropDownList('RouteForm[data]', $selected, $destinations, ['id' => 'routeform-destination', 'class' => 'form-control']) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                    <?= Html::button('Delete', ['class' => 'btn btn-danger', 'name' => 'delete-button', 'onclick' => 'js:document.location.href="'
                                        . Url::toRoute('asterisk/routes/delete').'?id='.$model->id.'"'])?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>