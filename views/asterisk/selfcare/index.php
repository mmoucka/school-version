<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use kartik\grid\GridView;
use kartik\popover\PopoverX;
use kartik\daterange\DateRangePicker;

$this->title = 'Calls history';
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        
        <div class="col-lg-9">
            <div class="panel panel-primary">
                <div class="panel-heading"><b>Search</b></div>
                <div class="panel-body" >
                    <?php $form = ActiveForm::begin(['id' => 'form-search']); ?>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'from') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'fromToOperator')->label('Operator')->dropDownList(['and' => 'and','or' => 'or']) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'to') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'durationOperator')->dropDownList(['equal' => 'Equal', 'greater' => 'Greater than', 'lower' => 'Lower than']) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'duration') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'state') ?>
                    </div>
                    <div class="col-md-8">
                        <label class="control-label">Date Range</label>
                        <div class="drp-container">
                            <?php
                            echo DateRangePicker::widget([
                                'name' => 'CdrSearch[dateRange]',
                                'convertFormat' => true,
                                'presetDropdown' => true,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'separator' => ' to ',
                                    'timePicker' => true,
                                    'timePickerIncrement' => 15,
                                    'format' => 'Y-m-d G:i',
                                ]
                            ]);
                            ?>
                        </div>
                        <br>
                        <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search-button']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'note') ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?> 
        </div>
        <div class="col-lg-9 col-xs-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading"><b><?= Html::encode($this->title) ?></b></div>
                <div class="panel-body" >
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $list,
                        'layout' => '{items}{pager}',
                        'export' => false,
                        'columns' => [
                            'calldate:text:Date',
                            'src:text:From',
                            [
                                'attribute' => 'dst',
                                'label' => 'To',
                                'value' => function ($model, $key, $index, $column)
                                {
                                    if(!strcmp('s', $model['dst']))
                                    {
                                        return $model['userfield'] ? $model['userfield'] : explode(',', $model['lastdata'])[0];
                                    }
                                    return $model['dst'];
                                }
                            ],
                            [
                                'attribute' => 'dstchannel',
                                'label' => 'Agent',
                                'value' => function ($model, $key, $index, $column)
                                {
                                    $extension = '';
                                    if(!empty($model['dstchannel']))
                                    {
                                        $channelID = explode('/', $model['dstchannel'])[1];
                                        $extension = explode('-', $channelID)[0];
                                    }
                                    return $extension;
                                }
                            ],
                            [
                                'attribute' => 'duration',
                                'format' => ['date', 'HH:mm:ss']
                            ],
                            'disposition:text:State',
                            [
                                'class' => 'kartik\grid\EditableColumn',
                                'attribute' => 'note',
                                'refreshGrid' => true,
                                'value' => function ($model, $key, $index, $column)
                                {
                                    return $model['note'] ? $model['note'] : 'Click to add note';
                                },
                                'editableOptions' => [
                                    'header' => 'Note',
                                    'placement' => PopoverX::ALIGN_LEFT,
                                    'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                                ],
                            ],
                        ],
                        'id' => 'id',
                        'showFooter' => false,
                    ]);
                    ?>  
                </div>
            </div>              
        </div>        
    </div>
</div>