<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use kartik\popover\PopoverX;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use yii\helpers\Url;
use kartik\grid\GridView;

$this->title = 'Phone Book';
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        
        <div class="col-lg-9">
            <?php
            ob_start();
            $form = ActiveForm::begin(['action' => 'createphonebookentry']);
            PopoverX::begin([
                'placement' => PopoverX::ALIGN_LEFT,
                'size' => PopoverX::SIZE_LARGE,
                'toggleButton' => ['label'=>'<i class="glyphicon glyphicon-plus"></i>', 'class'=>'btn btn-success'],
                'header' => 'Enter contact information',
                'footer'=>Html::submitButton('Add', ['class'=>'btn btn-sm btn-primary']).
                         Html::resetButton('Reset', ['class'=>'btn btn-sm btn-default'])
            ]);
            echo $form->field($model, 'name')->textInput(['placeholder'=>'Enter name...']);
            echo $form->field($model, 'number')->textInput(['placeholder'=>'Enter number...']);
            echo $form->field($model, 'description')->textInput(['placeholder'=>'Enter description...']);
            PopoverX::end();
            ActiveForm::end();
            $addform = $output = ob_get_clean();
            $columns = [
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'name',
                    'hiddenFromExport' => true,
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'header' => 'Name',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                    
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'number',
                    'hiddenFromExport' => true,
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'header' => 'Number',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'description',
                    'hiddenFromExport' => true,
                    'refreshGrid' => true,
                    'editableOptions' => [
                        'header' => 'Description',
                        'inputType' => \kartik\editable\Editable::INPUT_TEXT,
                    ],
                ],
                [
                    'attribute' => 'name',
                    'hidden' => true,
                    'filter' => false,
                    
                ],
                [
                    'attribute' => 'number',
                    'hidden' => true,
                    'filter' => false,
                ],
                [
                    'attribute' => 'description',
                    'hidden' => true,
                    'filter' => false,
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',                    
                    'template' => '{call} {deletephonebookentry}',
                    'buttons' => [
                        'deletephonebookentry' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash""></span>', $url, ['title' => 'Delete', 'data-confirm' => 'Are you sure you want to delete this item?']);
                        },
                        'call' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-earphone""></span>', $url, ['title' => 'Call']);
                        },
                    ],
            ]
            ];
            echo GridView::widget([
                'dataProvider' => $list,
                'filterModel' => $searchModel,
                'columns' => $columns,
                'toolbar' => [
                    [
                        'content' => $addform,
                        
                    ],
                    [
                        'content' => Html::a('<i class="glyphicon glyphicon-trash"></i> Delete All','deleteallphonebookentries',['type' => 'button', 'Delete All', 'class' => 'btn btn-danger', 'name' => 'add-button',
                            'data-confirm' => 'Are you sure you want to delete all contacts?']),
                    ],
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                    'target' => GridView::TARGET_SELF,       
                ],
                'exportConfig' => [
                    GridView::CSV => [
                        'label' => 'CSV',
                        'icon' => 'floppy-open',
                        'iconOptions' => ['class' => 'text-primary'],
                        'showHeader' => false,
                        'showPageSummary' => false,
                        'showFooter' => false,
                        'showCaption' => false,
                        'filename' => 'phonebook',
                        'alertMsg' => 'The CSV export file will be generated for download.',
                        'options' => ['title' => 'Comma Separated Values'],
                        'mime' => 'application/csv',
                    ],
                ],                
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<b>' . $this->title .'</b>',
                ],
                'responsive' => true,
                'hover' => true
            ]);
            ?>
        </div>
    </div>
</div>