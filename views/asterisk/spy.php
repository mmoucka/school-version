<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;

$this->title = 'Spy on channel';
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Spy',
    $this->title,
];
?>
<div class="site-createspy">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'form-spy']); ?>
                <div class="form-group field-spyform-destination">
                <label class="control-label" for="spyform-destination">Agent</label>
                <?= Html::dropDownList('spy', '', $destinations, ['id' => 'spy-destination', 'class' => 'form-control']) ?>
                <label class="control-label" for="whisper">Whisper</label>
                <?= Html::checkbox('whisper') ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Spy', ['class' => 'btn btn-primary', 'name' => 'spy-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>