<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;
use kartik\sortinput\SortableInput;

$this->title = 'Create Trunk';
$this->params['breadcrumbs'] = [
    'Asterisk',
    'Trunks',
    $this->title,
];
?>
<div class="site-createtrunk">
    <div class="row">
        <div class="col-lg-3">
            <?php
               echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to add new trunk:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-createtrunk']); ?>
                <?= $form->field($model, 'id') ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password') ?>
                <?= $form->field($model, 'registrar') ?>
                <?= $form->field($model, 'port') ?>
                <?= $form->field($model, 'retryInterval')?>
                <label>Codecs</label>
                <div class="row">                    
                    <div class="col-sm-6">                        
                        <?=$form->field($model, 'allow')->widget(SortableInput::classname(), [
                                'items' => [],
                                'hideInput' => true,
                                'sortableOptions' => [
                                    'connected' => 'codecs',
                                    'itemOptions' => ['aria-grabbed' => 'false'],
                                ],
                                'options' => ['class'=>'form-control', 'readonly'=>true]
                            ])->label('Allow:');?>
                    </div>
                    <div class="col-sm-6">
                    <?php
                        echo Html::label('Availabel codecs', 'availabel');
                        echo SortableInput::widget([
                            'name' => 'availabel',
                            'items' => $codecs,
                            'hideInput' => true,
                            'sortableOptions' => [
                                'connected' => 'codecs',
                                'itemOptions' => ['aria-grabbed' => 'false'],
                            ],
                            'options' => ['class' => 'form-control', 'readonly' => true]
                        ]);
                    ?>
                    </div>
                </div>          
                <div class="form-group">
                    <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>