<?php
/**
 * @package Webinterface
 * @author Martin Moucka <moucka.m@gmail.com>
 * @license GNU/GPL, see license.txt
 * Webinterface is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License 2
 * as published by the Free Software Foundation.
 * 
 * Webinterface is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Webinterface; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * or see http://www.gnu.org/licenses/.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;

$this->title = 'Trunks';
$this->params['breadcrumbs'] = [
    'Asterisk',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>List of trunks:</p>
            <?php
                echo GridView::widget([
                'dataProvider' => $list,
                'columns' => [
                    'id',
                    'username',
                    'status',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'asterisk/trunks',                        
                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' => 
                            'js:document.location.href="'. Url::toRoute('asterisk/trunks/create').'"']),
                    ],                    
                ],
                'showFooter' => true,
                'id' => 'id',
            ]);
            ?>
        </div>
    </div>
</div>
