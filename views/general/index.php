<?php
use yii\helpers\Html;
use yii\widgets\Menu;

$this->title = 'General';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="row">
        <div class="col-lg-3">
                <?php
                    echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
</div>