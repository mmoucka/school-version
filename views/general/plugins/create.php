<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\ActiveForm;


$this->title = 'Plugins';
$this->params['breadcrumbs'] = [
    'General',
    $this->title,
];
?>
<div class="site-plugins">
    <div class="row">
        <div class="col-lg-3">
            <?php
            echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-md-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>

           
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <?= $form->field($model, 'module[]')->fileInput(['multiple' => true])->label('Plugin archive')->hint('.zip archive required') ?>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>

            <?php ActiveForm::end()
            ?>
        </div>
    </div>
</div>



