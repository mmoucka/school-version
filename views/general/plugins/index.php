<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;

$this->title = 'Plugins';
$this->params['breadcrumbs'] = [
    'General',
    $this->title,
];
?>
<div class="site-plugins">
    <div class="row">
        <div class="col-lg-3">
            <?php
            echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-md-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>

            <p>List of plugins:</p>

            <?php
            echo GridView::widget([
                'dataProvider' => $provider,
                'id' => 'name',
                'columns' => [
                    'name', 'module',
                    [
                        'class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}',
                        'controller' => 'general/plugins',
                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' =>
                            'js:document.location.href="' . Url::toRoute('general/plugins/create') . '"']),
                    ],
                ],
                'showFooter' => true,
            ]);
            ?>
        </div>
    </div>
</div>



