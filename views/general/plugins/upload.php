<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;
use yii\widgets\ActiveForm;


$this->title = 'Plugins';
$this->params['breadcrumbs'] = [
    'General',
    $this->title,
];
?>
<div class="site-plugins">
    <div class="row">
        <div class="col-lg-3">
            <?php
            echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-md-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>

            <p>List of plugins:</p>
            
            <?php
                echo GridView::widget([
                'dataProvider' => $provider,
                'columns' => [
                    'name' ,
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'general/Plugins',                        
                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' => 
                            'js:document.location.href="'. Url::toRoute('general/plugins/create').'"']),
                    ],                    
                ],
                'showFooter' => true,
                'id' => 'name',
            ]);
            ?>
            
            
            <?php

            
            $files=\yii\helpers\FileHelper::findFiles('uploads/1/',['recursive'=>FALSE]);
            if (isset($files[0])) {
            foreach ($files as $index => $file) {
            $nameFicheiro = substr($file, strrpos($file, '/') + 1);
            echo Html::a($nameFicheiro, Url::base().'/uploads/1/'.$nameFicheiro) . "<br/>" . "<br/>" ;
            }
            } else {
            echo "There are no files available for download.";
            }        
            ?>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <?= $form->field($model, 'pluginName') ?>
            <?= $form->field($model, 'model[]')->fileInput(['multiple' => true]) ?>
            <?= $form->field($model, 'view[]')->fileInput(['multiple' => true]) ?>
            <?= $form->field($model, 'controller[]')->fileInput(['multiple' => true]) ?>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-primary']) ?>

            <?php ActiveForm::end()
            ?>
        </div>
    </div>
</div>



