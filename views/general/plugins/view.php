<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;
use yii\widgets\ActiveForm;


$this->title = 'Plugin: '.$name;
$this->params['breadcrumbs'] = [
    'General',
    $this->title,
];
?>
<div class="site-plugins">
    <div class="row">
        <div class="col-lg-3">
            <?php
            echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-md-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <?php
            echo $description;
            ?>
        </div>
    </div>
</div>



