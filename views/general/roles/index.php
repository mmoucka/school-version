<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;

$this->title = 'Roles';
$this->params['breadcrumbs'] = [
    'General',
    $this->title,
];
?>
<div class="site-roles">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-md-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>List of system roles:</p>
            <?php
                echo GridView::widget([
                'dataProvider' => $list,                
                'columns' => [
                    'name',
                    'description',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'general/roles',                        
                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' => 
                            'js:document.location.href="'. Url::toRoute('general/roles/create').'"']),
                    ],                    
                ],
                'showFooter' => true,
                'id' => 'name',
            ]);
            ?>
        </div>
    </div>
</div>
