<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\widgets\Menu;
use yii\bootstrap\Tabs;
use nullref\datatable\DataTableAsset;

$this->title = 'Update Role';
$this->params['breadcrumbs'] = [
    'Roles',
    'General',
    $this->title,
];
DataTableAsset::register($this);
?>
<div class="site-updateuser">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-8">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to update user:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-updaterole']); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'description') ?>
                <?php
                    foreach ($permissions as $plugin => $perms) {
                        $provider = new ArrayDataProvider([
                            'allModels' => $perms,
                            'sort' => [
                                'attributes' => ['name', 'created_at', 'description'],
                                'defaultOrder' => [
                                    'created_at' => SORT_DESC,
                                ]
                            ],                            
                        ]);
                        $tabs[] = ['label' => ucfirst($plugin), 'content' => GridView::widget([
                            'dataProvider' => $provider,
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered',
                                'id' => $plugin.'grid',
                            ],
                            'columns' => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'name' => 'permissions',
                                    'checkboxOptions' =>  
                                        function ($model, $key, $this) {                           
                                            return ['value' => $key, 'checked' => $model['checked']];                                            
                                        },
                                ],
                                'name',
                                'description',
                                [
                                    'attribute' => 'created_at',
                                    'format' => ['date', 'php:Y-m-d h:i']
                                ]
                            ],
                        ])];
                        $this->registerJs('jQuery("#' . $plugin.'grid' . '").DataTable( {
                            "paging":   false,
                            "ordering": false,
                            "info":     false
                        } );');
                        $tables[] = $plugin.'grid';
                    }
                    $clearsearch = '';
                    foreach ($tables as $value)
                    {
                        $clearsearch .= 'var '. $value . ' = $(\'#'.$value . '\').DataTable(); '.$value.'.search(\'\').draw(); $(\'.dataTables_filter input\').val(\'\');';
                    }
                    $this->registerJs('$( "#form-updaterole" ).submit(function( event ) {
                            '.$clearsearch. '
                          });');
                    echo Tabs::widget([
                    'items' => $tabs]);
                    ?>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>