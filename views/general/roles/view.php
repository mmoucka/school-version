<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\widgets\Menu;
use yii\bootstrap\Tabs;

$this->title = 'View Role';
$this->params['breadcrumbs'] = [
    'Roles',
    'General',
    $this->title,
];
?>
<div class="site-updateuser">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-8">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to update user:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-viewrole']); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'description') ?>
                <?php
                    foreach ($permissions as $plugin => $perms) {
                        $provider = new ArrayDataProvider([
                            'allModels' => $perms,
                            'sort' => [
                                'attributes' => ['name', 'created_at', 'description'],
                                'defaultOrder' => [
                                    'created_at' => SORT_DESC,
                                ]
                            ],                             
                        ]);
                        $tabs[] = ['label' => ucfirst($plugin), 'content' => GridView::widget([
                            'dataProvider' => $provider,
                            'columns' => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'name' => 'permissions',
                                    'checkboxOptions' =>  
                                        function ($model, $key, $this) {
                                            return ['value' => $key, 'checked' => $model['checked'], 'disabled' => true];
                                        },
                                ],
                                'name',
                                'description',
                                [
                                    'attribute' => 'created_at',
                                    'format' => ['date', 'php:Y-m-d h:i']
                                ]
                            ],
                        ])];
                    }
                    echo Tabs::widget([
                    'items' => $tabs]);
                ?>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>