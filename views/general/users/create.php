<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;

$this->title = 'Create User';
$this->params['breadcrumbs'] = [
    'Users',
    'General',
    $this->title,
];
?>
<div class="site-createuser">
    <div class="row">
        <div class="col-lg-3">
            <?php
               echo Menu::widget($menu);
            ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to add create user:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-createuser']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <div class="form-group field-userform-role">
                <label class="control-label" for="userform-role">Role</label>
                <?= Html::dropDownList('UserForm[role]', $selected, $roles, ['id' => 'userform-role', 'class' => 'form-control']) ?>
                </div>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>
