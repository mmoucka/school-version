<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Menu;

$this->title = 'Users';
$this->params['breadcrumbs'] = [
    'General',
    $this->title,
];
?>
<div class="site-users">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-9">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <p>List of system users:</p>
            <?php
                echo GridView::widget([
                'dataProvider' => $list,
                'columns' => [
                    'username',
                    'email',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'controller' => 'general/users',                        
                        'footer' => Html::button('Add', ['class' => 'btn btn-primary', 'name' => 'add-button', 'onclick' => 
                            'js:document.location.href="'. Url::toRoute('general/users/create').'"']),
                    ],                    
                ],
                'showFooter' => true,
                'id' => 'username',
            ]);
            ?>
        </div>
    </div>
</div>
