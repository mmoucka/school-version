<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;

$this->title = 'Update User';
$this->params['breadcrumbs'] = [
    'Users',
    'General',
    $this->title,
];
?>
<div class="site-updateuser">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            
            <p>Please fill this up to update user:</p>
            <?php $form = ActiveForm::begin(['id' => 'form-updateuser']); ?>
                <?= $form->field($model, 'username', [ 'inputOptions' => ['value' => $model->username]]) ?>
                <?= $form->field($model, 'email') ?>
                <div class="form-group field-userform-role">
                <label class="control-label" for="userform-role">Role</label>
                <?= Html::dropDownList('UserForm[role]', $selected, $roles, ['id' => 'userform-role', 'class' => 'form-control', 'disabled' => $isadmin]) ?>
                </div>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>