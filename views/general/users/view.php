<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Menu;

$this->title = 'View User';
$this->params['breadcrumbs'] = [
    'Users',
    'General',
    $this->title,
];
?>
<div class="site-viewuser">
    <div class="row">
        <div class="col-lg-3">
                <?php
                echo Menu::widget($menu);
                ?> 
        </div>
        <div class="col-lg-5">
            <h1 style="margin-top: 0px"><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'form-viewuser']); ?>
                <?= $form->field($model, 'username', [ 'inputOptions' => ['value' => $model->username, 'readonly'=>true]]) ?>
                <?= $form->field($model, 'email', [ 'inputOptions' => ['value' => $model->email, 'readonly' => true]])?>
                <div class="form-group field-userform-role">
                <label class="control-label" for="userform-role">Role</label>
                <?= Html::dropDownList('UserForm[role]', $selected, $roles, ['id' => 'userform-role', 'class' => 'form-control', 'disabled' => true]) ?>
                </div>
            <?php ActiveForm::end(); ?>                       
        </div>
    </div>
</div>