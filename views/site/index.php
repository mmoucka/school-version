<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Webinterface';
?>
<div class="site-index">

    <div class="jumbotron">
        <?php echo Html::img("@web/logo.png") ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-6">
                <h2>Webinterface for Asterisk PBX <!--and MikroTik--></h2>

                <p>Provides more comfortable configuration of Asterisk <!--and MikroTik--> without need of deep knowledge how those services work.
                    Also provides some additional features which are not present in base services.<br>
                    <br>
                    Author:<br>
                    <b>Martin Moučka  - xmouck02@stud.feec.vutbr.cz</b> <br>
                    <!--<b>Lukáš Janda - </b>-->
                    
            </div>
        </div>

    </div>
</div>
