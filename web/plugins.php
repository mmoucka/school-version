<?php

/**
 * Description of xmlRepository
 *
 * @author Lukas
 */
class Plugins {

    private $xmlFile = './plugins.xml';

    private function getFile() {
        return $this->xmlFile;
    }

    public function setFile($xmlFile) {

        if (file_exists($xmlFile)) {
            $this->xmlFile = $xmlFile;
        }
    }

    /*
     * paste new plugin info into XML file
     * 
     * @param type $content array of new plugin [menuName, mainPage, pathToPluginFolder]
     * @return 0 if fails 
     */

    public function create($module, $sideMenu, $pluginName, $description, $files = array(), $permissions = array(), $db = array()) {
        if ($sideMenu == NULL || $module == NULL || $files == NULL || $pluginName == NULL) {
            return NULL;
        }
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $newItem = $xml->createElement('plugin');
            $newItem->appendChild($xml->createElement('pluginName', $pluginName));
            if ($description != NULL) {
                $newItem->appendChild($xml->createElement('description', $description));
            }
            $newItem->appendChild($xml->createElement('module', $module));
            foreach ($files as $file) {
                $newItem->appendChild($xml->createElement('pluginFile', $file));
            }
            if ($db != NULL) {
                foreach ($db as $db => $key) {
                    $database = $newItem->appendChild($xml->createElement('database'));
                    $database->appendChild($xml->createElement('databaseName', $key['database']));
                    $database->appendChild($xml->createElement('tableName', $key['table']));
                }
            }
            if ($permissions != NULL) {
                foreach ($permissions as $permission => $key) {
                    $newItem->appendChild($xml->createElement('permission', $permission));
                }
            }
            if ($sideMenu != NULL) {
                foreach ($sideMenu as $key => $sideName) {
                    $side = $newItem->appendChild($xml->createElement('sideMenu'));
                    $side->appendChild($xml->createElement('menuName', $key));
                    $side->appendChild($xml->createElement('url', $sideName['url']));
                    foreach ($sideName['permissions'] as $perm) {
                        $side->appendChild($xml->createElement('permissions', $perm));
                    }
                }
            }
            $xml->getElementsByTagName('plugins')->item(0)->appendChild($newItem);
            $error = @$xml->save($this->getFile());
            if ($error == FALSE) {
                return -1;
            }
            if ($error == 0) {
                return -2;
            }
            return 'OK';
        } return 0;
    }

    /*
     * delete plugin out of xml file by given menuName
     * 
     * @param type $plugin menuName of plugin to delete
     * @return 0 if fails 
     */

    public function delete($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('pluginName')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    $value->parentNode->removeChild($value);
                    return $xml->save($this->getFile());
                }
            }
        } return 0;
    }

    public function getFilePaths($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('pluginName')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    $desc_r = $value->getElementsByTagName('pluginFile');
                    $return = array();
                    foreach ($desc_r as $fajl) {
                        $return[] = $fajl->nodeValue;
                    }
                    return $return;
                }
            }
        } return 0;
    }

    /*
     * return array of plugins menuNames
     * 
     * @return 0 if fails 
     */

    public function getNames($module) {
        return null;
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    $return = $value->getElementsByTagName('menuName')->item(0);
                    $return = $return->nodeValue;
                    $returns[] = $return;
                }
            }
            return $returns;
        } return 0;
    }

    public function getNames2() {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $return = $value->getElementsByTagName('pluginName')->item(0);
                $return = $return->nodeValue;
                $module = $value->getElementsByTagName('module')->item(0);
                $module = $module->nodeValue;
                $returns[$return]['module'] = $module;
                $returns[$return]['name'] = $return;
            }
            return $returns;
        } return 0;
    }

    /*
     * return array of plugins mainPages
     * 
     * @return 0 if fails 
     */

    public function getMainPages($module) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $returns = array();
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    $return = $value->getElementsByTagName('mainPage')->item(0);
                    $return = $return->nodeValue;
                    $returns[] = $return;
                }
            }
            return $returns;
        } return 0;
    }

    /*
     * return array of plugins folder paths
     * 
     * @return 0 if fails 
     */

    public function getDescription($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('pluginName')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    $desc_r = $value->getElementsByTagName('description')->item(0);
                    $return = $desc_r->nodeValue;
                    return $return;
                }
            }
        } return 0;
    }

    public function getPermissions($module) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            if ($element == NULL) {
                return NULL;
            }
            $return = array();
            $i = 0;
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    $plugName = $value->getElementsByTagName('pluginName')->item(0);
                    $desc_r = $value->getElementsByTagName('permission');

                    foreach ($desc_r as $fajl) {
                        $return[$i][] = $plugName->nodeValue . "_" . $fajl->nodeValue;
                    }
                    $i++;
                }
            }
            return $return;
        } return 0;
    }

    public function getPermissionsByName($name) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            if ($element == NULL) {
                return NULL;
            }
            $return = array();
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('pluginName')->item(0);
                if ($nv->nodeValue == $name) {
                    $plugName = $value->getElementsByTagName('module')->item(0);
                    $desc_r = $value->getElementsByTagName('permission');
                    foreach ($desc_r as $fajl) {
                        $return[] = $plugName->nodeValue . "_" . $fajl->nodeValue;
                    }
                }
            }
            return $return;
        } return 0;
    }

    public function getString($module) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            if ($element == NULL) {
                return NULL;
            }
            $return = array();
            $i = 0;
            foreach ($element as $value) {
                $nv = $value->getElementsByTagName('module')->item(0);
                if ($nv->nodeValue == $module) {
                    //$plugName = $value->getElementsByTagName('pluginName')->item(0);
                    $label = $value->getElementsByTagName('sideMenu');
                    foreach ($label as $fajl) {
                        $return[$i]['label'] = $fajl->getElementsByTagName('menuName')->item(0)->nodeValue;
                        $return[$i]['url'] = $fajl->getElementsByTagName('url')->item(0)->nodeValue;
                        $perm = $fajl->getElementsByTagName('permissions');
                        foreach ($perm as $permission) {
                            $return[$i]['permissions'][] = $permission->nodeValue;
                        }
                        //$return[$i][] = $plugName->nodeValue . "_" . $fajl->nodeValue;

                        $i++;
                    }
                }
            }
            return $return;
        } return 0;
    }

    public function getDbInfo($plugin) {
        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = false;
        if (@$xml->load($this->getFile())) {
            $element = $xml->getElementsByTagName('plugin');
            foreach ($element as $value) {
                $name_r = $value->getElementsByTagName('pluginName')->item(0);
                $name_r = $name_r->nodeValue;
                if ($plugin == $name_r) {
                    //$tab = $value->getElementsByTagName('table')->item(0);
                    $db = $value->getElementsByTagName('database');
                    $return = array();
                    $i = 0;
                    foreach ($db as $key) {
                        $dbName = $key->getElementsByTagName('databaseName')->item(0)->nodeValue;
                        $tabName = $key->getElementsByTagName('tableName')->item(0)->nodeValue;
                        $return[$i]['database'] = $dbName;
                        $return[$i]['table'] = $tabName;
                        $i++;
                    }
                    return $return;
                }
            }
        }
    }

}
